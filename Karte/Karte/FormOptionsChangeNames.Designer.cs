﻿namespace Karte
{
    partial class FormOptionsChangeNames
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_optionsChangeNamesDONE = new System.Windows.Forms.Button();
            this.tb_changeNamesPlayer0 = new System.Windows.Forms.TextBox();
            this.tb_changeNamesPlayer1 = new System.Windows.Forms.TextBox();
            this.tb_changeNamesPlayer2 = new System.Windows.Forms.TextBox();
            this.tb_changeNamesPlayer3 = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.YellowGreen;
            this.panel1.Controls.Add(this.tb_changeNamesPlayer3);
            this.panel1.Controls.Add(this.tb_changeNamesPlayer2);
            this.panel1.Controls.Add(this.tb_changeNamesPlayer1);
            this.panel1.Controls.Add(this.tb_changeNamesPlayer0);
            this.panel1.Controls.Add(this.btn_optionsChangeNamesDONE);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(347, 263);
            this.panel1.TabIndex = 0;
            // 
            // btn_optionsChangeNamesDONE
            // 
            this.btn_optionsChangeNamesDONE.Location = new System.Drawing.Point(226, 106);
            this.btn_optionsChangeNamesDONE.Name = "btn_optionsChangeNamesDONE";
            this.btn_optionsChangeNamesDONE.Size = new System.Drawing.Size(101, 44);
            this.btn_optionsChangeNamesDONE.TabIndex = 5;
            this.btn_optionsChangeNamesDONE.Text = "Done";
            this.btn_optionsChangeNamesDONE.UseVisualStyleBackColor = true;
            this.btn_optionsChangeNamesDONE.Click += new System.EventHandler(this.btn_optionsChangeNamesDONE_Click);
            // 
            // tb_changeNamesPlayer0
            // 
            this.tb_changeNamesPlayer0.Location = new System.Drawing.Point(33, 38);
            this.tb_changeNamesPlayer0.Multiline = true;
            this.tb_changeNamesPlayer0.Name = "tb_changeNamesPlayer0";
            this.tb_changeNamesPlayer0.Size = new System.Drawing.Size(137, 33);
            this.tb_changeNamesPlayer0.TabIndex = 7;
            // 
            // tb_changeNamesPlayer1
            // 
            this.tb_changeNamesPlayer1.Location = new System.Drawing.Point(33, 88);
            this.tb_changeNamesPlayer1.Multiline = true;
            this.tb_changeNamesPlayer1.Name = "tb_changeNamesPlayer1";
            this.tb_changeNamesPlayer1.Size = new System.Drawing.Size(137, 33);
            this.tb_changeNamesPlayer1.TabIndex = 8;
            // 
            // tb_changeNamesPlayer2
            // 
            this.tb_changeNamesPlayer2.Location = new System.Drawing.Point(33, 139);
            this.tb_changeNamesPlayer2.Multiline = true;
            this.tb_changeNamesPlayer2.Name = "tb_changeNamesPlayer2";
            this.tb_changeNamesPlayer2.Size = new System.Drawing.Size(137, 33);
            this.tb_changeNamesPlayer2.TabIndex = 9;
            // 
            // tb_changeNamesPlayer3
            // 
            this.tb_changeNamesPlayer3.Location = new System.Drawing.Point(33, 189);
            this.tb_changeNamesPlayer3.Multiline = true;
            this.tb_changeNamesPlayer3.Name = "tb_changeNamesPlayer3";
            this.tb_changeNamesPlayer3.Size = new System.Drawing.Size(137, 33);
            this.tb_changeNamesPlayer3.TabIndex = 10;
            // 
            // FormOptionsChangeNames
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(342, 252);
            this.Controls.Add(this.panel1);
            this.Name = "FormOptionsChangeNames";
            this.Text = "Change Names";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_optionsChangeNamesDONE;
        private System.Windows.Forms.TextBox tb_changeNamesPlayer3;
        private System.Windows.Forms.TextBox tb_changeNamesPlayer2;
        private System.Windows.Forms.TextBox tb_changeNamesPlayer1;
        private System.Windows.Forms.TextBox tb_changeNamesPlayer0;
    }
}