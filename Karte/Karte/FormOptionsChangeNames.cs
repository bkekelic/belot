﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Karte
{
    public partial class FormOptionsChangeNames : Form
    {

        

        public FormOptionsChangeNames()
        {
            InitializeComponent();
            

            showNamesOnTextboxes();
        }
        public void showNamesOnTextboxes()
        {
            Form1 parent = (Form1)this.Owner;



            tb_changeNamesPlayer0.Text = parent.getYourName();
            tb_changeNamesPlayer1.Text = parent.lb_opponentRightName.Text;
            tb_changeNamesPlayer2.Text = parent.lb_yourTeammateName.Text;
            tb_changeNamesPlayer3.Text = parent.lb_opponentLeftName.Text;



        }

        private void btn_optionsChangeNamesDONE_Click(object sender, EventArgs e)
        {
            Form1 parent = (Form1)this.Owner;

            parent.lb_yourName.Text = tb_changeNamesPlayer0.Text;
            parent.lb_opponentRightName.Text = tb_changeNamesPlayer1.Text;
            parent.lb_yourTeammateName.Text = tb_changeNamesPlayer2.Text;
            parent.lb_opponentLeftName.Text = tb_changeNamesPlayer3.Text;

            parent.checkIfFilesExists();

            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(@"docs\\names.gsm.txt"))
            {
                writer.WriteLine(tb_changeNamesPlayer0.Text);
                writer.WriteLine(tb_changeNamesPlayer1.Text);
                writer.WriteLine(tb_changeNamesPlayer2.Text);
                writer.WriteLine(tb_changeNamesPlayer3.Text);
            }


            Application.ExitThread();

        }
    }
}
