﻿namespace Karte
{
    partial class FormStatistics
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.lb_statisticsPlayerPoints3 = new System.Windows.Forms.Label();
            this.lb_statisticsPlayerPoints2 = new System.Windows.Forms.Label();
            this.lb_statisticsPlayerPoints1 = new System.Windows.Forms.Label();
            this.lb_statisticsPlayerPoints0 = new System.Windows.Forms.Label();
            this.lb_statisticsAllPointsTheir = new System.Windows.Forms.Label();
            this.lb_statisticsAllPointsOur = new System.Windows.Forms.Label();
            this.lb_statisticsTotalScoreTheir = new System.Windows.Forms.Label();
            this.lb_statisticsTotalScoreOur = new System.Windows.Forms.Label();
            this.lb_formStatisticsPlayer3Name = new System.Windows.Forms.Label();
            this.lb_formStatisticsPlayer2Name = new System.Windows.Forms.Label();
            this.lb_formStatisticsPlayer1Name = new System.Windows.Forms.Label();
            this.lb_formStatisticsPlayer0Name = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(181, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "We";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(286, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Them";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.OliveDrab;
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.lb_statisticsPlayerPoints3);
            this.panel1.Controls.Add(this.lb_statisticsPlayerPoints2);
            this.panel1.Controls.Add(this.lb_statisticsPlayerPoints1);
            this.panel1.Controls.Add(this.lb_statisticsPlayerPoints0);
            this.panel1.Controls.Add(this.lb_statisticsAllPointsTheir);
            this.panel1.Controls.Add(this.lb_statisticsAllPointsOur);
            this.panel1.Controls.Add(this.lb_statisticsTotalScoreTheir);
            this.panel1.Controls.Add(this.lb_statisticsTotalScoreOur);
            this.panel1.Controls.Add(this.lb_formStatisticsPlayer3Name);
            this.panel1.Controls.Add(this.lb_formStatisticsPlayer2Name);
            this.panel1.Controls.Add(this.lb_formStatisticsPlayer1Name);
            this.panel1.Controls.Add(this.lb_formStatisticsPlayer0Name);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.panel16);
            this.panel1.Controls.Add(this.panel17);
            this.panel1.Controls.Add(this.panel13);
            this.panel1.Controls.Add(this.panel18);
            this.panel1.Controls.Add(this.panel14);
            this.panel1.Controls.Add(this.panel12);
            this.panel1.Controls.Add(this.panel15);
            this.panel1.Controls.Add(this.panel11);
            this.panel1.Controls.Add(this.panel10);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Controls.Add(this.panel9);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(1, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(368, 598);
            this.panel1.TabIndex = 2;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.SystemColors.Control;
            this.label8.Location = new System.Drawing.Point(192, 568);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(157, 17);
            this.label8.TabIndex = 45;
            this.label8.Text = "© Bernard Kekelić, v1.0";
            // 
            // lb_statisticsPlayerPoints3
            // 
            this.lb_statisticsPlayerPoints3.AutoSize = true;
            this.lb_statisticsPlayerPoints3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_statisticsPlayerPoints3.Location = new System.Drawing.Point(217, 523);
            this.lb_statisticsPlayerPoints3.Name = "lb_statisticsPlayerPoints3";
            this.lb_statisticsPlayerPoints3.Size = new System.Drawing.Size(23, 25);
            this.lb_statisticsPlayerPoints3.TabIndex = 27;
            this.lb_statisticsPlayerPoints3.Text = "0";
            // 
            // lb_statisticsPlayerPoints2
            // 
            this.lb_statisticsPlayerPoints2.AutoSize = true;
            this.lb_statisticsPlayerPoints2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_statisticsPlayerPoints2.Location = new System.Drawing.Point(217, 476);
            this.lb_statisticsPlayerPoints2.Name = "lb_statisticsPlayerPoints2";
            this.lb_statisticsPlayerPoints2.Size = new System.Drawing.Size(23, 25);
            this.lb_statisticsPlayerPoints2.TabIndex = 26;
            this.lb_statisticsPlayerPoints2.Text = "0";
            // 
            // lb_statisticsPlayerPoints1
            // 
            this.lb_statisticsPlayerPoints1.AutoSize = true;
            this.lb_statisticsPlayerPoints1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_statisticsPlayerPoints1.Location = new System.Drawing.Point(217, 427);
            this.lb_statisticsPlayerPoints1.Name = "lb_statisticsPlayerPoints1";
            this.lb_statisticsPlayerPoints1.Size = new System.Drawing.Size(23, 25);
            this.lb_statisticsPlayerPoints1.TabIndex = 25;
            this.lb_statisticsPlayerPoints1.Text = "0";
            // 
            // lb_statisticsPlayerPoints0
            // 
            this.lb_statisticsPlayerPoints0.AutoSize = true;
            this.lb_statisticsPlayerPoints0.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_statisticsPlayerPoints0.Location = new System.Drawing.Point(217, 376);
            this.lb_statisticsPlayerPoints0.Name = "lb_statisticsPlayerPoints0";
            this.lb_statisticsPlayerPoints0.Size = new System.Drawing.Size(23, 25);
            this.lb_statisticsPlayerPoints0.TabIndex = 24;
            this.lb_statisticsPlayerPoints0.Text = "0";
            // 
            // lb_statisticsAllPointsTheir
            // 
            this.lb_statisticsAllPointsTheir.AutoSize = true;
            this.lb_statisticsAllPointsTheir.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_statisticsAllPointsTheir.Location = new System.Drawing.Point(274, 221);
            this.lb_statisticsAllPointsTheir.Name = "lb_statisticsAllPointsTheir";
            this.lb_statisticsAllPointsTheir.Size = new System.Drawing.Size(23, 25);
            this.lb_statisticsAllPointsTheir.TabIndex = 23;
            this.lb_statisticsAllPointsTheir.Text = "0";
            // 
            // lb_statisticsAllPointsOur
            // 
            this.lb_statisticsAllPointsOur.AutoSize = true;
            this.lb_statisticsAllPointsOur.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_statisticsAllPointsOur.Location = new System.Drawing.Point(156, 220);
            this.lb_statisticsAllPointsOur.Name = "lb_statisticsAllPointsOur";
            this.lb_statisticsAllPointsOur.Size = new System.Drawing.Size(23, 25);
            this.lb_statisticsAllPointsOur.TabIndex = 22;
            this.lb_statisticsAllPointsOur.Text = "0";
            // 
            // lb_statisticsTotalScoreTheir
            // 
            this.lb_statisticsTotalScoreTheir.AutoSize = true;
            this.lb_statisticsTotalScoreTheir.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_statisticsTotalScoreTheir.Location = new System.Drawing.Point(274, 72);
            this.lb_statisticsTotalScoreTheir.Name = "lb_statisticsTotalScoreTheir";
            this.lb_statisticsTotalScoreTheir.Size = new System.Drawing.Size(23, 25);
            this.lb_statisticsTotalScoreTheir.TabIndex = 21;
            this.lb_statisticsTotalScoreTheir.Text = "0";
            // 
            // lb_statisticsTotalScoreOur
            // 
            this.lb_statisticsTotalScoreOur.AutoSize = true;
            this.lb_statisticsTotalScoreOur.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_statisticsTotalScoreOur.Location = new System.Drawing.Point(156, 72);
            this.lb_statisticsTotalScoreOur.Name = "lb_statisticsTotalScoreOur";
            this.lb_statisticsTotalScoreOur.Size = new System.Drawing.Size(23, 25);
            this.lb_statisticsTotalScoreOur.TabIndex = 20;
            this.lb_statisticsTotalScoreOur.Text = "0";
            // 
            // lb_formStatisticsPlayer3Name
            // 
            this.lb_formStatisticsPlayer3Name.AutoSize = true;
            this.lb_formStatisticsPlayer3Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_formStatisticsPlayer3Name.Location = new System.Drawing.Point(41, 527);
            this.lb_formStatisticsPlayer3Name.Name = "lb_formStatisticsPlayer3Name";
            this.lb_formStatisticsPlayer3Name.Size = new System.Drawing.Size(65, 20);
            this.lb_formStatisticsPlayer3Name.TabIndex = 19;
            this.lb_formStatisticsPlayer3Name.Text = "Player3";
            // 
            // lb_formStatisticsPlayer2Name
            // 
            this.lb_formStatisticsPlayer2Name.AutoSize = true;
            this.lb_formStatisticsPlayer2Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_formStatisticsPlayer2Name.Location = new System.Drawing.Point(41, 481);
            this.lb_formStatisticsPlayer2Name.Name = "lb_formStatisticsPlayer2Name";
            this.lb_formStatisticsPlayer2Name.Size = new System.Drawing.Size(65, 20);
            this.lb_formStatisticsPlayer2Name.TabIndex = 18;
            this.lb_formStatisticsPlayer2Name.Text = "Player2";
            // 
            // lb_formStatisticsPlayer1Name
            // 
            this.lb_formStatisticsPlayer1Name.AutoSize = true;
            this.lb_formStatisticsPlayer1Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_formStatisticsPlayer1Name.Location = new System.Drawing.Point(41, 431);
            this.lb_formStatisticsPlayer1Name.Name = "lb_formStatisticsPlayer1Name";
            this.lb_formStatisticsPlayer1Name.Size = new System.Drawing.Size(65, 20);
            this.lb_formStatisticsPlayer1Name.TabIndex = 17;
            this.lb_formStatisticsPlayer1Name.Text = "Player1";
            // 
            // lb_formStatisticsPlayer0Name
            // 
            this.lb_formStatisticsPlayer0Name.AutoSize = true;
            this.lb_formStatisticsPlayer0Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_formStatisticsPlayer0Name.Location = new System.Drawing.Point(41, 380);
            this.lb_formStatisticsPlayer0Name.Name = "lb_formStatisticsPlayer0Name";
            this.lb_formStatisticsPlayer0Name.Size = new System.Drawing.Size(65, 20);
            this.lb_formStatisticsPlayer0Name.TabIndex = 16;
            this.lb_formStatisticsPlayer0Name.Text = "Player0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(158, 319);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(191, 25);
            this.label7.TabIndex = 15;
            this.label7.Text = "Points won by player";
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.Olive;
            this.panel16.Location = new System.Drawing.Point(1, 454);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(360, 10);
            this.panel16.TabIndex = 14;
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.Olive;
            this.panel17.Location = new System.Drawing.Point(2, 551);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(360, 10);
            this.panel17.TabIndex = 13;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.Olive;
            this.panel13.Location = new System.Drawing.Point(2, 306);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(360, 10);
            this.panel13.TabIndex = 14;
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.Olive;
            this.panel18.Location = new System.Drawing.Point(2, 504);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(360, 10);
            this.panel18.TabIndex = 12;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.Olive;
            this.panel14.Location = new System.Drawing.Point(3, 403);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(360, 10);
            this.panel14.TabIndex = 13;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.Olive;
            this.panel12.Location = new System.Drawing.Point(140, 306);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(10, 253);
            this.panel12.TabIndex = 9;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.Olive;
            this.panel15.Location = new System.Drawing.Point(3, 356);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(360, 10);
            this.panel15.TabIndex = 12;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.Olive;
            this.panel11.Location = new System.Drawing.Point(3, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(360, 10);
            this.panel11.TabIndex = 5;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Olive;
            this.panel10.Location = new System.Drawing.Point(2, 151);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(360, 10);
            this.panel10.TabIndex = 11;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Olive;
            this.panel6.Location = new System.Drawing.Point(258, 151);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(10, 107);
            this.panel6.TabIndex = 9;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Olive;
            this.panel2.Location = new System.Drawing.Point(258, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(10, 107);
            this.panel2.TabIndex = 4;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Olive;
            this.panel7.Location = new System.Drawing.Point(3, 248);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(360, 10);
            this.panel7.TabIndex = 11;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Olive;
            this.panel5.Location = new System.Drawing.Point(3, 100);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(360, 10);
            this.panel5.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(26, 221);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 20);
            this.label4.TabIndex = 12;
            this.label4.Text = "Total score";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(24, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "Matches won";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Olive;
            this.panel8.Location = new System.Drawing.Point(3, 201);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(360, 10);
            this.panel8.TabIndex = 10;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Olive;
            this.panel9.Location = new System.Drawing.Point(140, 151);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(10, 107);
            this.panel9.TabIndex = 8;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Olive;
            this.panel4.Location = new System.Drawing.Point(3, 53);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(360, 10);
            this.panel4.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(181, 173);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 25);
            this.label5.TabIndex = 6;
            this.label5.Text = "Our";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Olive;
            this.panel3.Location = new System.Drawing.Point(140, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(10, 107);
            this.panel3.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(286, 173);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 25);
            this.label6.TabIndex = 7;
            this.label6.Text = "Their";
            // 
            // FormStatistics
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(363, 594);
            this.Controls.Add(this.panel1);
            this.Name = "FormStatistics";
            this.Text = "Statistics (Belot)";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label lb_statisticsPlayerPoints3;
        public System.Windows.Forms.Label lb_statisticsPlayerPoints2;
        public System.Windows.Forms.Label lb_statisticsPlayerPoints1;
        public System.Windows.Forms.Label lb_statisticsPlayerPoints0;
        public System.Windows.Forms.Label lb_statisticsAllPointsTheir;
        public System.Windows.Forms.Label lb_statisticsAllPointsOur;
        public System.Windows.Forms.Label lb_statisticsTotalScoreTheir;
        public System.Windows.Forms.Label lb_statisticsTotalScoreOur;
        public System.Windows.Forms.Label lb_formStatisticsPlayer3Name;
        public System.Windows.Forms.Label lb_formStatisticsPlayer2Name;
        public System.Windows.Forms.Label lb_formStatisticsPlayer1Name;
        public System.Windows.Forms.Label lb_formStatisticsPlayer0Name;
    }
}