﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Karte
{
    public partial class FormOptions : Form
    {
        public FormOptions()
        {
            InitializeComponent();
            setYourName();
        }

        private void btn_changeNames_Click(object sender, EventArgs e)
        {
            using (FormOptionsChangeNames frmChangeNames = new FormOptionsChangeNames())
            {         
                frmChangeNames.ShowDialog(this);
            }

        }

        public void setYourName()
        {
            Form1 parent = (Form1)this.Owner;
            lb_optionsYourName.Text = parent.getYourName();
        }






    }   
}
