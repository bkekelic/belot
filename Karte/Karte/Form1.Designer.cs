﻿namespace Karte
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.lb_yourName = new System.Windows.Forms.Label();
            this.lb_yourTeammateName = new System.Windows.Forms.Label();
            this.lb_opponentLeftName = new System.Windows.Forms.Label();
            this.lb_opponentRightName = new System.Windows.Forms.Label();
            this.btn_newGame = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.lb_theirPotSum = new System.Windows.Forms.Label();
            this.lb_ourPotSum = new System.Windows.Forms.Label();
            this.lb_scoreTheir = new System.Windows.Forms.Label();
            this.lb_scoreOur = new System.Windows.Forms.Label();
            this.lb_vi = new System.Windows.Forms.Label();
            this.lb_mi = new System.Windows.Forms.Label();
            this.pb_handCard0 = new System.Windows.Forms.PictureBox();
            this.pb_handCard1 = new System.Windows.Forms.PictureBox();
            this.pb_handCard2 = new System.Windows.Forms.PictureBox();
            this.pb_handCard4 = new System.Windows.Forms.PictureBox();
            this.pb_handCard5 = new System.Windows.Forms.PictureBox();
            this.pb_handCard6 = new System.Windows.Forms.PictureBox();
            this.pb_handCard3 = new System.Windows.Forms.PictureBox();
            this.pb_handCard7 = new System.Windows.Forms.PictureBox();
            this.pb_playedCardMy = new System.Windows.Forms.PictureBox();
            this.pb_playedCardRight = new System.Windows.Forms.PictureBox();
            this.pb_playedCardUpper = new System.Windows.Forms.PictureBox();
            this.pb_playedCardLeft = new System.Windows.Forms.PictureBox();
            this.pb_callZir = new System.Windows.Forms.PictureBox();
            this.pb_callList = new System.Windows.Forms.PictureBox();
            this.pb_callSrce = new System.Windows.Forms.PictureBox();
            this.pb_callBundeva = new System.Windows.Forms.PictureBox();
            this.pb_callDALJE = new System.Windows.Forms.PictureBox();
            this.pb_callBoja1 = new System.Windows.Forms.PictureBox();
            this.pb_callBoja0 = new System.Windows.Forms.PictureBox();
            this.pb_callBoja2 = new System.Windows.Forms.PictureBox();
            this.pb_callBoja3 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lb_showWhoPass = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btn_options = new System.Windows.Forms.Button();
            this.btn_getStatistics = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_quit = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lb_naPotezuSi = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_handCard0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_handCard1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_handCard2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_handCard4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_handCard5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_handCard6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_handCard3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_handCard7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_playedCardMy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_playedCardRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_playedCardUpper)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_playedCardLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_callZir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_callList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_callSrce)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_callBundeva)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_callDALJE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_callBoja1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_callBoja0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_callBoja2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_callBoja3)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel8.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // lb_yourName
            // 
            this.lb_yourName.AutoSize = true;
            this.lb_yourName.BackColor = System.Drawing.Color.Olive;
            this.lb_yourName.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lb_yourName.Location = new System.Drawing.Point(461, 783);
            this.lb_yourName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_yourName.Name = "lb_yourName";
            this.lb_yourName.Size = new System.Drawing.Size(86, 26);
            this.lb_yourName.TabIndex = 9;
            this.lb_yourName.Text = "STEVO";
            this.lb_yourName.Visible = false;
            // 
            // lb_yourTeammateName
            // 
            this.lb_yourTeammateName.AutoSize = true;
            this.lb_yourTeammateName.BackColor = System.Drawing.Color.Olive;
            this.lb_yourTeammateName.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lb_yourTeammateName.Location = new System.Drawing.Point(470, 23);
            this.lb_yourTeammateName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_yourTeammateName.Name = "lb_yourTeammateName";
            this.lb_yourTeammateName.Size = new System.Drawing.Size(93, 26);
            this.lb_yourTeammateName.TabIndex = 10;
            this.lb_yourTeammateName.Text = "MARKO";
            this.lb_yourTeammateName.Visible = false;
            // 
            // lb_opponentLeftName
            // 
            this.lb_opponentLeftName.AutoSize = true;
            this.lb_opponentLeftName.BackColor = System.Drawing.Color.Olive;
            this.lb_opponentLeftName.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lb_opponentLeftName.Location = new System.Drawing.Point(182, 330);
            this.lb_opponentLeftName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_opponentLeftName.Name = "lb_opponentLeftName";
            this.lb_opponentLeftName.Size = new System.Drawing.Size(75, 26);
            this.lb_opponentLeftName.TabIndex = 11;
            this.lb_opponentLeftName.Text = "PERO";
            this.lb_opponentLeftName.Visible = false;
            // 
            // lb_opponentRightName
            // 
            this.lb_opponentRightName.AutoSize = true;
            this.lb_opponentRightName.BackColor = System.Drawing.Color.Olive;
            this.lb_opponentRightName.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lb_opponentRightName.Location = new System.Drawing.Point(815, 301);
            this.lb_opponentRightName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_opponentRightName.Name = "lb_opponentRightName";
            this.lb_opponentRightName.Size = new System.Drawing.Size(64, 26);
            this.lb_opponentRightName.TabIndex = 12;
            this.lb_opponentRightName.Text = "IVAN";
            this.lb_opponentRightName.Visible = false;
            // 
            // btn_newGame
            // 
            this.btn_newGame.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_newGame.Location = new System.Drawing.Point(144, 74);
            this.btn_newGame.Margin = new System.Windows.Forms.Padding(4);
            this.btn_newGame.Name = "btn_newGame";
            this.btn_newGame.Size = new System.Drawing.Size(264, 58);
            this.btn_newGame.TabIndex = 17;
            this.btn_newGame.Text = "New Game";
            this.btn_newGame.UseVisualStyleBackColor = true;
            this.btn_newGame.Click += new System.EventHandler(this.btn_newGame_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkGreen;
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.lb_theirPotSum);
            this.panel1.Controls.Add(this.lb_ourPotSum);
            this.panel1.Controls.Add(this.lb_scoreTheir);
            this.panel1.Controls.Add(this.lb_scoreOur);
            this.panel1.Controls.Add(this.lb_vi);
            this.panel1.Controls.Add(this.lb_mi);
            this.panel1.Location = new System.Drawing.Point(10, 10);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(212, 156);
            this.panel1.TabIndex = 24;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.panel6.Location = new System.Drawing.Point(0, 47);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(212, 10);
            this.panel6.TabIndex = 10;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.panel5.Location = new System.Drawing.Point(98, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(10, 155);
            this.panel5.TabIndex = 9;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.panel7.Location = new System.Drawing.Point(0, 100);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(212, 10);
            this.panel7.TabIndex = 11;
            // 
            // lb_theirPotSum
            // 
            this.lb_theirPotSum.AutoSize = true;
            this.lb_theirPotSum.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lb_theirPotSum.Location = new System.Drawing.Point(135, 63);
            this.lb_theirPotSum.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_theirPotSum.Name = "lb_theirPotSum";
            this.lb_theirPotSum.Size = new System.Drawing.Size(43, 29);
            this.lb_theirPotSum.TabIndex = 8;
            this.lb_theirPotSum.Text = "(0)";
            this.lb_theirPotSum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_ourPotSum
            // 
            this.lb_ourPotSum.AutoSize = true;
            this.lb_ourPotSum.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lb_ourPotSum.Location = new System.Drawing.Point(25, 63);
            this.lb_ourPotSum.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_ourPotSum.Name = "lb_ourPotSum";
            this.lb_ourPotSum.Size = new System.Drawing.Size(43, 29);
            this.lb_ourPotSum.TabIndex = 7;
            this.lb_ourPotSum.Text = "(0)";
            this.lb_ourPotSum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_scoreTheir
            // 
            this.lb_scoreTheir.AutoSize = true;
            this.lb_scoreTheir.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lb_scoreTheir.Location = new System.Drawing.Point(140, 120);
            this.lb_scoreTheir.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_scoreTheir.Name = "lb_scoreTheir";
            this.lb_scoreTheir.Size = new System.Drawing.Size(27, 29);
            this.lb_scoreTheir.TabIndex = 3;
            this.lb_scoreTheir.Text = "0";
            this.lb_scoreTheir.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_scoreOur
            // 
            this.lb_scoreOur.AutoSize = true;
            this.lb_scoreOur.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lb_scoreOur.Location = new System.Drawing.Point(30, 120);
            this.lb_scoreOur.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_scoreOur.Name = "lb_scoreOur";
            this.lb_scoreOur.Size = new System.Drawing.Size(27, 29);
            this.lb_scoreOur.TabIndex = 2;
            this.lb_scoreOur.Text = "0";
            this.lb_scoreOur.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_vi
            // 
            this.lb_vi.AutoSize = true;
            this.lb_vi.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lb_vi.Location = new System.Drawing.Point(136, 12);
            this.lb_vi.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_vi.Name = "lb_vi";
            this.lb_vi.Size = new System.Drawing.Size(36, 29);
            this.lb_vi.TabIndex = 1;
            this.lb_vi.Text = "VI";
            // 
            // lb_mi
            // 
            this.lb_mi.AutoSize = true;
            this.lb_mi.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lb_mi.Location = new System.Drawing.Point(24, 12);
            this.lb_mi.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_mi.Name = "lb_mi";
            this.lb_mi.Size = new System.Drawing.Size(40, 29);
            this.lb_mi.TabIndex = 0;
            this.lb_mi.Text = "MI";
            // 
            // pb_handCard0
            // 
            this.pb_handCard0.Location = new System.Drawing.Point(212, 551);
            this.pb_handCard0.Name = "pb_handCard0";
            this.pb_handCard0.Size = new System.Drawing.Size(120, 190);
            this.pb_handCard0.TabIndex = 31;
            this.pb_handCard0.TabStop = false;
            this.pb_handCard0.Click += new System.EventHandler(this.pb_handCard0_Click);
            // 
            // pb_handCard1
            // 
            this.pb_handCard1.Location = new System.Drawing.Point(283, 551);
            this.pb_handCard1.Name = "pb_handCard1";
            this.pb_handCard1.Size = new System.Drawing.Size(120, 190);
            this.pb_handCard1.TabIndex = 32;
            this.pb_handCard1.TabStop = false;
            this.pb_handCard1.Click += new System.EventHandler(this.pb_handCard1_Click_1);
            // 
            // pb_handCard2
            // 
            this.pb_handCard2.Location = new System.Drawing.Point(356, 551);
            this.pb_handCard2.Name = "pb_handCard2";
            this.pb_handCard2.Size = new System.Drawing.Size(120, 190);
            this.pb_handCard2.TabIndex = 33;
            this.pb_handCard2.TabStop = false;
            this.pb_handCard2.Click += new System.EventHandler(this.pb_handCard2_Click_1);
            // 
            // pb_handCard4
            // 
            this.pb_handCard4.Location = new System.Drawing.Point(503, 551);
            this.pb_handCard4.Name = "pb_handCard4";
            this.pb_handCard4.Size = new System.Drawing.Size(120, 190);
            this.pb_handCard4.TabIndex = 35;
            this.pb_handCard4.TabStop = false;
            this.pb_handCard4.Click += new System.EventHandler(this.pb_handCard4_Click_1);
            // 
            // pb_handCard5
            // 
            this.pb_handCard5.Location = new System.Drawing.Point(575, 551);
            this.pb_handCard5.Name = "pb_handCard5";
            this.pb_handCard5.Size = new System.Drawing.Size(120, 190);
            this.pb_handCard5.TabIndex = 36;
            this.pb_handCard5.TabStop = false;
            this.pb_handCard5.Click += new System.EventHandler(this.pb_handCard5_Click_1);
            // 
            // pb_handCard6
            // 
            this.pb_handCard6.Location = new System.Drawing.Point(653, 551);
            this.pb_handCard6.Name = "pb_handCard6";
            this.pb_handCard6.Size = new System.Drawing.Size(120, 190);
            this.pb_handCard6.TabIndex = 37;
            this.pb_handCard6.TabStop = false;
            this.pb_handCard6.Click += new System.EventHandler(this.pb_handCard6_Click_1);
            // 
            // pb_handCard3
            // 
            this.pb_handCard3.Location = new System.Drawing.Point(428, 551);
            this.pb_handCard3.Name = "pb_handCard3";
            this.pb_handCard3.Size = new System.Drawing.Size(120, 190);
            this.pb_handCard3.TabIndex = 34;
            this.pb_handCard3.TabStop = false;
            this.pb_handCard3.Click += new System.EventHandler(this.pb_handCard3_Click_1);
            // 
            // pb_handCard7
            // 
            this.pb_handCard7.Location = new System.Drawing.Point(725, 551);
            this.pb_handCard7.Name = "pb_handCard7";
            this.pb_handCard7.Size = new System.Drawing.Size(120, 190);
            this.pb_handCard7.TabIndex = 38;
            this.pb_handCard7.TabStop = false;
            this.pb_handCard7.Click += new System.EventHandler(this.pb_handCard7_Click_1);
            // 
            // pb_playedCardMy
            // 
            this.pb_playedCardMy.ErrorImage = null;
            this.pb_playedCardMy.InitialImage = null;
            this.pb_playedCardMy.Location = new System.Drawing.Point(157, 192);
            this.pb_playedCardMy.Name = "pb_playedCardMy";
            this.pb_playedCardMy.Size = new System.Drawing.Size(97, 154);
            this.pb_playedCardMy.TabIndex = 39;
            this.pb_playedCardMy.TabStop = false;
            // 
            // pb_playedCardRight
            // 
            this.pb_playedCardRight.ErrorImage = null;
            this.pb_playedCardRight.InitialImage = null;
            this.pb_playedCardRight.Location = new System.Drawing.Point(260, 113);
            this.pb_playedCardRight.Name = "pb_playedCardRight";
            this.pb_playedCardRight.Size = new System.Drawing.Size(97, 154);
            this.pb_playedCardRight.TabIndex = 40;
            this.pb_playedCardRight.TabStop = false;
            // 
            // pb_playedCardUpper
            // 
            this.pb_playedCardUpper.ErrorImage = null;
            this.pb_playedCardUpper.InitialImage = null;
            this.pb_playedCardUpper.Location = new System.Drawing.Point(157, 23);
            this.pb_playedCardUpper.Name = "pb_playedCardUpper";
            this.pb_playedCardUpper.Size = new System.Drawing.Size(97, 154);
            this.pb_playedCardUpper.TabIndex = 41;
            this.pb_playedCardUpper.TabStop = false;
            // 
            // pb_playedCardLeft
            // 
            this.pb_playedCardLeft.ErrorImage = null;
            this.pb_playedCardLeft.InitialImage = null;
            this.pb_playedCardLeft.Location = new System.Drawing.Point(54, 113);
            this.pb_playedCardLeft.Name = "pb_playedCardLeft";
            this.pb_playedCardLeft.Size = new System.Drawing.Size(97, 154);
            this.pb_playedCardLeft.TabIndex = 42;
            this.pb_playedCardLeft.TabStop = false;
            // 
            // pb_callZir
            // 
            this.pb_callZir.Location = new System.Drawing.Point(70, 378);
            this.pb_callZir.Name = "pb_callZir";
            this.pb_callZir.Size = new System.Drawing.Size(76, 80);
            this.pb_callZir.TabIndex = 43;
            this.pb_callZir.TabStop = false;
            this.pb_callZir.Click += new System.EventHandler(this.pb_callZir_Click);
            // 
            // pb_callList
            // 
            this.pb_callList.Location = new System.Drawing.Point(70, 464);
            this.pb_callList.Name = "pb_callList";
            this.pb_callList.Size = new System.Drawing.Size(76, 80);
            this.pb_callList.TabIndex = 44;
            this.pb_callList.TabStop = false;
            this.pb_callList.Click += new System.EventHandler(this.pb_callList_Click);
            // 
            // pb_callSrce
            // 
            this.pb_callSrce.Location = new System.Drawing.Point(70, 550);
            this.pb_callSrce.Name = "pb_callSrce";
            this.pb_callSrce.Size = new System.Drawing.Size(76, 80);
            this.pb_callSrce.TabIndex = 45;
            this.pb_callSrce.TabStop = false;
            this.pb_callSrce.Click += new System.EventHandler(this.pb_callSrce_Click);
            // 
            // pb_callBundeva
            // 
            this.pb_callBundeva.Location = new System.Drawing.Point(70, 636);
            this.pb_callBundeva.Name = "pb_callBundeva";
            this.pb_callBundeva.Size = new System.Drawing.Size(76, 80);
            this.pb_callBundeva.TabIndex = 46;
            this.pb_callBundeva.TabStop = false;
            this.pb_callBundeva.Click += new System.EventHandler(this.pb_callBundeva_Click);
            // 
            // pb_callDALJE
            // 
            this.pb_callDALJE.Location = new System.Drawing.Point(70, 759);
            this.pb_callDALJE.Name = "pb_callDALJE";
            this.pb_callDALJE.Size = new System.Drawing.Size(76, 80);
            this.pb_callDALJE.TabIndex = 47;
            this.pb_callDALJE.TabStop = false;
            this.pb_callDALJE.Click += new System.EventHandler(this.pb_callDALJE_Click);
            // 
            // pb_callBoja1
            // 
            this.pb_callBoja1.Location = new System.Drawing.Point(801, 216);
            this.pb_callBoja1.Name = "pb_callBoja1";
            this.pb_callBoja1.Size = new System.Drawing.Size(89, 82);
            this.pb_callBoja1.TabIndex = 9;
            this.pb_callBoja1.TabStop = false;
            // 
            // pb_callBoja0
            // 
            this.pb_callBoja0.Location = new System.Drawing.Point(560, 759);
            this.pb_callBoja0.Name = "pb_callBoja0";
            this.pb_callBoja0.Size = new System.Drawing.Size(89, 82);
            this.pb_callBoja0.TabIndex = 48;
            this.pb_callBoja0.TabStop = false;
            // 
            // pb_callBoja2
            // 
            this.pb_callBoja2.Location = new System.Drawing.Point(582, 9);
            this.pb_callBoja2.Name = "pb_callBoja2";
            this.pb_callBoja2.Size = new System.Drawing.Size(89, 82);
            this.pb_callBoja2.TabIndex = 49;
            this.pb_callBoja2.TabStop = false;
            // 
            // pb_callBoja3
            // 
            this.pb_callBoja3.Location = new System.Drawing.Point(174, 245);
            this.pb_callBoja3.Name = "pb_callBoja3";
            this.pb_callBoja3.Size = new System.Drawing.Size(89, 82);
            this.pb_callBoja3.TabIndex = 50;
            this.pb_callBoja3.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Green;
            this.panel3.Controls.Add(this.lb_showWhoPass);
            this.panel3.Controls.Add(this.pb_playedCardUpper);
            this.panel3.Controls.Add(this.pb_playedCardMy);
            this.panel3.Controls.Add(this.pb_playedCardRight);
            this.panel3.Controls.Add(this.pb_playedCardLeft);
            this.panel3.Location = new System.Drawing.Point(24, 24);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(408, 366);
            this.panel3.TabIndex = 0;
            // 
            // lb_showWhoPass
            // 
            this.lb_showWhoPass.AutoSize = true;
            this.lb_showWhoPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_showWhoPass.Location = new System.Drawing.Point(24, 177);
            this.lb_showWhoPass.Name = "lb_showWhoPass";
            this.lb_showWhoPass.Size = new System.Drawing.Size(0, 29);
            this.lb_showWhoPass.TabIndex = 43;
            this.lb_showWhoPass.Visible = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.OliveDrab;
            this.panel4.Controls.Add(this.btn_options);
            this.panel4.Controls.Add(this.btn_getStatistics);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.btn_quit);
            this.panel4.Controls.Add(this.btn_newGame);
            this.panel4.Location = new System.Drawing.Point(984, 6);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(493, 841);
            this.panel4.TabIndex = 52;
            // 
            // btn_options
            // 
            this.btn_options.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_options.Location = new System.Drawing.Point(144, 210);
            this.btn_options.Margin = new System.Windows.Forms.Padding(4);
            this.btn_options.Name = "btn_options";
            this.btn_options.Size = new System.Drawing.Size(264, 58);
            this.btn_options.TabIndex = 46;
            this.btn_options.Text = "Options";
            this.btn_options.UseVisualStyleBackColor = true;
            this.btn_options.Click += new System.EventHandler(this.btn_options_Click);
            // 
            // btn_getStatistics
            // 
            this.btn_getStatistics.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_getStatistics.Location = new System.Drawing.Point(144, 140);
            this.btn_getStatistics.Margin = new System.Windows.Forms.Padding(4);
            this.btn_getStatistics.Name = "btn_getStatistics";
            this.btn_getStatistics.Size = new System.Drawing.Size(264, 58);
            this.btn_getStatistics.TabIndex = 45;
            this.btn_getStatistics.Text = "Statistics";
            this.btn_getStatistics.UseVisualStyleBackColor = true;
            this.btn_getStatistics.Click += new System.EventHandler(this.btn_getStatistics_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.Control;
            this.label4.Location = new System.Drawing.Point(200, 352);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(157, 17);
            this.label4.TabIndex = 44;
            this.label4.Text = "© Bernard Kekelić, v1.1";
            // 
            // btn_quit
            // 
            this.btn_quit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_quit.Location = new System.Drawing.Point(144, 280);
            this.btn_quit.Margin = new System.Windows.Forms.Padding(4);
            this.btn_quit.Name = "btn_quit";
            this.btn_quit.Size = new System.Drawing.Size(264, 58);
            this.btn_quit.TabIndex = 18;
            this.btn_quit.Text = "Quit";
            this.btn_quit.UseVisualStyleBackColor = true;
            this.btn_quit.Click += new System.EventHandler(this.btn_quit_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.OliveDrab;
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Location = new System.Drawing.Point(294, 97);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(456, 410);
            this.panel2.TabIndex = 51;
            // 
            // lb_naPotezuSi
            // 
            this.lb_naPotezuSi.AutoSize = true;
            this.lb_naPotezuSi.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_naPotezuSi.Location = new System.Drawing.Point(433, 518);
            this.lb_naPotezuSi.Name = "lb_naPotezuSi";
            this.lb_naPotezuSi.Size = new System.Drawing.Size(139, 26);
            this.lb_naPotezuSi.TabIndex = 44;
            this.lb_naPotezuSi.Text = "Na potezu si!";
            this.lb_naPotezuSi.Visible = false;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.panel8.Controls.Add(this.panel9);
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(278, 10);
            this.panel8.TabIndex = 11;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.panel9.Location = new System.Drawing.Point(0, 9);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(10, 155);
            this.panel9.TabIndex = 10;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.DarkOliveGreen;
            this.panel10.Location = new System.Drawing.Point(0, 10);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(10, 200);
            this.panel10.TabIndex = 10;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.ForestGreen;
            this.ClientSize = new System.Drawing.Size(1482, 853);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.lb_naPotezuSi);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pb_callBoja3);
            this.Controls.Add(this.pb_callBoja2);
            this.Controls.Add(this.pb_callBoja0);
            this.Controls.Add(this.pb_callBoja1);
            this.Controls.Add(this.pb_callDALJE);
            this.Controls.Add(this.pb_callBundeva);
            this.Controls.Add(this.pb_callSrce);
            this.Controls.Add(this.pb_callList);
            this.Controls.Add(this.pb_callZir);
            this.Controls.Add(this.pb_handCard7);
            this.Controls.Add(this.pb_handCard6);
            this.Controls.Add(this.pb_handCard5);
            this.Controls.Add(this.pb_handCard4);
            this.Controls.Add(this.pb_handCard3);
            this.Controls.Add(this.pb_handCard2);
            this.Controls.Add(this.pb_handCard1);
            this.Controls.Add(this.pb_handCard0);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lb_opponentRightName);
            this.Controls.Add(this.lb_opponentLeftName);
            this.Controls.Add(this.lb_yourTeammateName);
            this.Controls.Add(this.lb_yourName);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Belot";
            this.Load += new System.EventHandler(this.Form1_Load_1);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_handCard0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_handCard1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_handCard2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_handCard4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_handCard5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_handCard6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_handCard3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_handCard7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_playedCardMy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_playedCardRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_playedCardUpper)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_playedCardLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_callZir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_callList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_callSrce)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_callBundeva)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_callDALJE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_callBoja1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_callBoja0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_callBoja2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_callBoja3)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Button btn_newGame;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lb_scoreTheir;
        private System.Windows.Forms.Label lb_scoreOur;
        private System.Windows.Forms.Label lb_vi;
        private System.Windows.Forms.Label lb_mi;
        private System.Windows.Forms.Label lb_theirPotSum;
        private System.Windows.Forms.Label lb_ourPotSum;
        private System.Windows.Forms.PictureBox pb_handCard0;
        private System.Windows.Forms.PictureBox pb_handCard1;
        private System.Windows.Forms.PictureBox pb_handCard2;
        private System.Windows.Forms.PictureBox pb_handCard4;
        private System.Windows.Forms.PictureBox pb_handCard5;
        private System.Windows.Forms.PictureBox pb_handCard6;
        private System.Windows.Forms.PictureBox pb_handCard3;
        private System.Windows.Forms.PictureBox pb_handCard7;
        private System.Windows.Forms.PictureBox pb_playedCardMy;
        private System.Windows.Forms.PictureBox pb_playedCardRight;
        private System.Windows.Forms.PictureBox pb_playedCardUpper;
        private System.Windows.Forms.PictureBox pb_playedCardLeft;
        private System.Windows.Forms.PictureBox pb_callZir;
        private System.Windows.Forms.PictureBox pb_callList;
        private System.Windows.Forms.PictureBox pb_callSrce;
        private System.Windows.Forms.PictureBox pb_callBundeva;
        private System.Windows.Forms.PictureBox pb_callDALJE;
        private System.Windows.Forms.PictureBox pb_callBoja1;
        private System.Windows.Forms.PictureBox pb_callBoja0;
        private System.Windows.Forms.PictureBox pb_callBoja2;
        private System.Windows.Forms.PictureBox pb_callBoja3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_quit;
        private System.Windows.Forms.Label lb_showWhoPass;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lb_naPotezuSi;
        private System.Windows.Forms.Button btn_getStatistics;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button btn_options;
        public System.Windows.Forms.Label lb_yourName;
        public System.Windows.Forms.Label lb_yourTeammateName;
        public System.Windows.Forms.Label lb_opponentLeftName;
        public System.Windows.Forms.Label lb_opponentRightName;
    }
}

