﻿namespace Karte
{
    partial class FormOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_changeNames = new System.Windows.Forms.Button();
            this.lb_optionsYourName = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.OliveDrab;
            this.panel1.Controls.Add(this.lb_optionsYourName);
            this.panel1.Controls.Add(this.btn_changeNames);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(457, 362);
            this.panel1.TabIndex = 0;
            // 
            // btn_changeNames
            // 
            this.btn_changeNames.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_changeNames.Location = new System.Drawing.Point(130, 48);
            this.btn_changeNames.Margin = new System.Windows.Forms.Padding(4);
            this.btn_changeNames.Name = "btn_changeNames";
            this.btn_changeNames.Size = new System.Drawing.Size(191, 48);
            this.btn_changeNames.TabIndex = 47;
            this.btn_changeNames.Text = "Change names";
            this.btn_changeNames.UseVisualStyleBackColor = true;
            this.btn_changeNames.Click += new System.EventHandler(this.btn_changeNames_Click);
            // 
            // lb_optionsYourName
            // 
            this.lb_optionsYourName.AutoSize = true;
            this.lb_optionsYourName.Location = new System.Drawing.Point(184, 152);
            this.lb_optionsYourName.Name = "lb_optionsYourName";
            this.lb_optionsYourName.Size = new System.Drawing.Size(23, 17);
            this.lb_optionsYourName.TabIndex = 1;
            this.lb_optionsYourName.Text = "as";
            // 
            // FormOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(453, 359);
            this.Controls.Add(this.panel1);
            this.Name = "FormOptions";
            this.Text = "Options (Belot)";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Button btn_changeNames;
        private System.Windows.Forms.Label lb_optionsYourName;
    }
}