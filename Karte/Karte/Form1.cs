﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;
using System.Resources;

namespace Karte
{

    public partial class Form1 : Form
    {
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        SoundPlayer Player_cardDrop = new SoundPlayer(@"sounds\DropCardSound2.wav");
        //SoundPlayer Player_nextCaller = new SoundPlayer(@"sounds\NextCallerSound.wav");
        SoundPlayer Player_ClockTicking = new SoundPlayer(@"sounds\ClockTicking.wav");
        SoundPlayer Player_ReadyToPlay= new SoundPlayer(@"sounds\ReadyToPlaySound.wav");
        SoundPlayer Player_ShufflingCards = new SoundPlayer(@"sounds\ShufflingCardsSound.wav");

        Cards[] card = new Cards[32];

        HandCards MyPlayedCard = new HandCards();
        HandCards RightPlayerPlayedCard = new HandCards();
        HandCards UpperPlayerPlayedCard = new HandCards();
        HandCards LeftPlayerPlayedCard = new HandCards();
        HandCards mainPlayedCard = new HandCards();

        HandCards[] myCard = new HandCards[8];
        HandCards[] RightPlayerCard = new HandCards[8];
        HandCards[] UpperPlayerCard = new HandCards[8];
        HandCards[] LeftPlayerCard = new HandCards[8];

        Random rnd = new Random();
        public Timer timer_youPlayCards, timer_rightPlayerPlayed, timer_upperPlayerPlayed, timer_leftPlayerPlayed,timer_waitForYourTurnToPlay;
        public Timer timer_youCall, timer_rightCall, timer_upperCall, timer_leftCall;
        public Timer timer_animationWhoTakesThePot, timer_shuffleCards, timer_showWhoPass;

        public int ourSum, theirSum;
        public int ourRoundSum, theirRoundSum;
        public int totalScoreOur, totalScoreTheir;
        public int Mixer, possibleCaller, playerWhoPlay;
        public int numOfCardsPlayed;
        public bool youPlay;
        public bool modeYourTurnToCall;
        public bool[] playableCard = new bool[8];
        public int rightTime, upperTime, leftTime;

        public string calledColor;

        public string exitStringCro = "Zelis li izaci iz aplikacije?", exitStringEng = "Do you really want to exit application?";


        private void Form1_Load_1(object sender, EventArgs e)
        {

        }
        public Form1()
        {           

            InitializeComponent();
            InitTimers();

            checkIfFilesExists();

            for (int i = 0; i < 32; i++) card[i] = new Cards();
            for (int i = 0; i < 8; i++)
            {
                myCard[i] = new HandCards();
                RightPlayerCard[i] = new HandCards();
                UpperPlayerCard[i] = new HandCards();
                LeftPlayerCard[i] = new HandCards();
            }
            defineCards();
        }

        public void InitTimers()
        {

            timer_youPlayCards = new Timer();
            timer_youPlayCards.Tick += new EventHandler(timer_youPlayCards_Tick);
            timer_youPlayCards.Interval = 1000000;

            timer_rightPlayerPlayed = new Timer();
            timer_rightPlayerPlayed.Tick += new EventHandler(timer_rightPlayerPlayed_Tick);
            timer_rightPlayerPlayed.Interval = 2000;

            timer_upperPlayerPlayed = new Timer();
            timer_upperPlayerPlayed.Tick += new EventHandler(timer_upperPlayerPlayed_Tick);
            timer_upperPlayerPlayed.Interval = 2000;

            timer_leftPlayerPlayed = new Timer();
            timer_leftPlayerPlayed.Tick += new EventHandler(timer_leftPlayerPlayed_Tick);
            timer_leftPlayerPlayed.Interval = 2000;

            timer_waitForYourTurnToPlay = new Timer();
            timer_waitForYourTurnToPlay.Tick += new EventHandler(timer_waitForYourTurnToPlay_Tick);
            timer_waitForYourTurnToPlay.Interval = 100000;

            timer_youCall = new Timer();
            timer_youCall.Tick += new EventHandler(timer_youCall_Tick);
            timer_youCall.Interval = 100000;

            timer_rightCall = new Timer();
            timer_rightCall.Tick += new EventHandler(timer_rightCall_Tick);
            timer_rightCall.Interval = 100000;

            timer_upperCall = new Timer();
            timer_upperCall.Tick += new EventHandler(timer_upperCall_Tick);
            timer_upperCall.Interval = 100000;

            timer_leftCall = new Timer();
            timer_leftCall.Tick += new EventHandler(timer_leftCall_Tick);
            timer_leftCall.Interval = 100000;

            timer_animationWhoTakesThePot = new Timer();
            timer_animationWhoTakesThePot.Tick += new EventHandler(timer_animationWhoTakesThePot_Tick);
            timer_animationWhoTakesThePot.Interval = 2000;

            timer_shuffleCards = new Timer();
            timer_shuffleCards.Tick += new EventHandler(timer_shuffleCards_Tick);
            timer_shuffleCards.Interval = 100000;

            timer_showWhoPass = new Timer();
            timer_showWhoPass.Tick += new EventHandler(timer_showWhoPass_Tick);
            timer_showWhoPass.Interval = 5000;


        }

        public void timer_youPlayCards_Tick(object sender, EventArgs e)
        {
            resetTimerOptions();
            afterTimerYouPlayTick();
        }
        public void timer_rightPlayerPlayed_Tick(object sender, EventArgs e)
        {
            timer_rightPlayerPlayed.Stop();
            rightPlayerIsPlaying();

            if(playerWhoPlay == 1)
            {     
                mainPlayedCard.setColor(RightPlayerPlayedCard.getColor());
                mainPlayedCard.setID(RightPlayerPlayedCard.getID());
                mainPlayedCard.setName(RightPlayerPlayedCard.getName());
                mainPlayedCard.setValue(RightPlayerPlayedCard.getValue());
            }
            else if(playerWhoPlay == 2)
            {
                afterSomeoneLastPlayed();
            }

        }
        public void timer_upperPlayerPlayed_Tick(object sender, EventArgs e)
        {
            timer_upperPlayerPlayed.Stop();
            upperPlayerIsPlaying();

            if(playerWhoPlay == 2)
            {
                mainPlayedCard.setColor(UpperPlayerPlayedCard.getColor());
                mainPlayedCard.setID(UpperPlayerPlayedCard.getID());
                mainPlayedCard.setName(UpperPlayerPlayedCard.getName());
                mainPlayedCard.setValue(UpperPlayerPlayedCard.getValue());
            }
            else if(playerWhoPlay == 3)
            {
                afterSomeoneLastPlayed();
            }
        }
        public void timer_leftPlayerPlayed_Tick(object sender, EventArgs e)
        {
            timer_leftPlayerPlayed.Stop();
            leftPlayerIsPlaying();

            if (playerWhoPlay != 0)
            {
                timer_waitForYourTurnToPlay.Interval = 1;
                timer_waitForYourTurnToPlay.Start();
            }
            
            if (playerWhoPlay == 0)
            {
                afterSomeoneLastPlayed();
            }
            else if(playerWhoPlay == 3)
            {
                mainPlayedCard.setColor(LeftPlayerPlayedCard.getColor());
                mainPlayedCard.setID(LeftPlayerPlayedCard.getID());
                mainPlayedCard.setName(LeftPlayerPlayedCard.getName());
                mainPlayedCard.setValue(LeftPlayerPlayedCard.getValue());
            }
            
        }
        public void timer_waitForYourTurnToPlay_Tick(object sender, EventArgs e)
        {
            timer_waitForYourTurnToPlay.Stop();
            checkWhichCardCanPlay();
            youArePlaying();

        }

        
        public class Cards
        {
            private string color;
            private string name;
            private int value;
            private bool shared;
            private double ID,playableID;

            public Cards()
            {
                color = "N/A";
                name = "N/A";
                value = 0;
                shared = false;
                ID = 0;
                playableID = 0;
            }
            public Cards(string vColor, string vName, int vValue, bool vShared)
            {
                color = vColor;
                name = vName;
                value = vValue;
                shared = vShared;
            }

            public void setColor(string vColor)
            {
                color = vColor;
            }
            public void setName(string vName)
            {
                name = vName;
            }
            public void setValue(int vValue)
            {
                value = vValue;
            }
            public void setShared(bool vShared)
            {
                shared = vShared;
            }
            public void setID(double vID)
            {
                ID = vID;
            }
            public void setPlayableID(double vPlayableID)
            {
                playableID = vPlayableID;
            }

            public string getColor() { return color; }
            public string getName() { return name; }
            public int getValue() { return value; }
            public bool getShared() { return shared; }
            public double getPlayableID() { return playableID; }
            public double getID() { return ID; }


        }         
        public class HandCards
        {
            private string color;
            private string name;
            private int value;
            private double ID,playableID;
            private bool played;

            public HandCards()
            {
                color = "N/A";
                name = "N/A";
                value = 0;
                ID = 0;
                playableID = 0;
                played = false;
            }

            public void setColor(string vColor)
            {
                color = vColor;
            }
            public void setName(string vName)
            {
                name = vName;
            }
            public void setValue(int vValue)
            {
                value = vValue;
            }           
            public void setID(double vID)
            {
                ID = vID;
            }
            public void setPlayableID(double vplayableID)
            {
                playableID = vplayableID;
            }
            public void setPlayed(bool vPlayed)
            {
                played = vPlayed;
            }

            public string getColor() { return color; }
            public string getName() { return name; }
            public int getValue() { return value; }   
            public double getID() { return ID; }
            public double getPlayableID() { return playableID; }
            public bool getPlayed() { return played; }
        }


        public void defineCards()
        {

            for (int i = 0; i < 32; i++)
            {

                card[i].setID(i);
                card[i].setPlayableID(i);

                if (i < 8) card[i].setColor("Zir");
                else if (i < 16) card[i].setColor("List");
                else if (i < 24) card[i].setColor("Srce");
                else card[i].setColor("Bundeva");
            }
            for (int i = 0; i <= 24; i += 8)
            {
                card[i].setName("Kec");
                card[i].setValue(11);
                card[i + 1].setName("10");
                card[i + 1].setValue(10);
                card[i + 2].setName("Kralj");
                card[i + 2].setValue(4);
                card[i + 3].setName("Dama");
                card[i + 3].setValue(3);
                card[i + 4].setName("Decko");
                card[i + 4].setValue(2);
                card[i + 5].setName("9");
                card[i + 5].setValue(0);
                card[i + 6].setName("8");
                card[i + 6].setValue(0);
                card[i + 7].setName("7");
                card[i + 7].setValue(0);
            }


        }

        private void btn_newGame_Click(object sender, EventArgs e)
        {
            lb_yourName.Visible = true;
            lb_yourTeammateName.Visible = true;
            lb_opponentRightName.Visible = true;
            lb_opponentLeftName.Visible = true;

            btn_newGame.Enabled = false;
            restartMain();
            restartForCalling();

            timer_shuffleCards.Interval = getRandomNumber(2000, 5000);
            timer_shuffleCards.Start();
            Player_ShufflingCards.PlayLooping();
            //kod isteka timera se nalazi share6cards i play calling                
        }
        private void btn_quit_Click(object sender, EventArgs e)
        {
            string exitString = exitStringEng;
            DialogResult dialog = MessageBox.Show(exitString, "Exit", MessageBoxButtons.YesNo);

            if (dialog == DialogResult.Yes)
            {
                Application.Exit();
            }

        }

        public void restartMain()
        {           
            ourSum = 0;
            theirSum = 0;
            lb_scoreOur.Text = "(" + ourSum.ToString() + ")";
            lb_scoreTheir.Text = "(" + theirSum.ToString() + ")";
            ourRoundSum = 0;
            theirRoundSum = 0;
            numOfCardsPlayed = 0;
            calledColor = "";
            modeYourTurnToCall = false;

            for(int i = 0; i < 8; i++)
            {
                playableCard[i] = false;
            }

         }
        public void restartForCalling()
        {
            numOfCardsPlayed = 0;
            calledColor = "";
            modeYourTurnToCall = false;

            pb_callBoja0.Image = null;
            pb_callBoja1.Image = null;
            pb_callBoja2.Image = null;
            pb_callBoja3.Image = null;

            for (int i = 0; i < 32; i++) card[i].setShared(false);

            for (int i = 0; i <= 24; i += 8)
            {
                card[i + 4].setValue(2);
                card[i + 5].setValue(0);
            }
            


        }

        public int getRandomNumber(int dg, int gg)
        {
            int number = rnd.Next(dg, gg);
            return number;
        }        

        public void show6DownCards()
        {
            pb_handCard0.Visible = true;
            pb_handCard1.Visible = true;
            pb_handCard2.Visible = true;
            pb_handCard3.Visible = true;
            pb_handCard4.Visible = true;
            pb_handCard5.Visible = true;
           


        }
        public void show2DownCards()
        {
            pb_handCard6.Visible = true;
            pb_handCard7.Visible = true;

        }
        public void show4MiddleCards()
        {
            pb_playedCardMy.Visible = true;
            pb_playedCardRight.Visible = true;
            pb_playedCardUpper.Visible = true;
            pb_playedCardLeft.Visible = true;
        }              
        public void erase4MiddleCards()
        {
            pb_playedCardMy.Image = null;
            pb_playedCardRight.Image = null;
            pb_playedCardUpper.Image = null;
            pb_playedCardLeft.Image = null;
        }    
        public void erase8DownCards()
        {
            pb_handCard0.Image = null;
            pb_handCard1.Image = null;
            pb_handCard2.Image = null;
            pb_handCard3.Image = null;
            pb_handCard4.Image = null;
            pb_handCard5.Image = null;
            pb_handCard6.Image = null;
            pb_handCard7.Image = null;
        }   
     
        public void Play_Calling()
        {
           
            if ((ourSum == 0) && (theirSum == 0)) whoFirstMix();
            else nextMixer();
            
            whoCalls();                                           

        }
        public void PlayCards()
        {
            if(numOfCardsPlayed == 0)
            {
                int temp = Mixer + 1;
                if (temp == 4) temp = 0;
                playerWhoPlay = temp; 
            }     

            if (numOfCardsPlayed < 8)
            {                

                if (playerWhoPlay == 0)
                {
                    checkWhichCardCanPlay();
                    youArePlaying();
                }
                else if (playerWhoPlay == 1)
                {

                    timer_rightPlayerPlayed.Interval = getRandomNumber(400, 1000);
                    timer_rightPlayerPlayed.Start();

                    timer_upperPlayerPlayed.Interval = getRandomNumber(2600, 3200);
                    timer_upperPlayerPlayed.Start();

                    timer_leftPlayerPlayed.Interval = getRandomNumber(4800, 5400);
                    timer_leftPlayerPlayed.Start();

                }
                else if (playerWhoPlay == 2)
                {
                    timer_upperPlayerPlayed.Interval = getRandomNumber(400, 1000);
                    timer_upperPlayerPlayed.Start();

                    timer_leftPlayerPlayed.Interval = getRandomNumber(2600, 3200);
                    timer_leftPlayerPlayed.Start();

                }
                else if (playerWhoPlay == 3)
                {

                    timer_leftPlayerPlayed.Interval = getRandomNumber(400, 1000);
                    timer_leftPlayerPlayed.Start();

                }
               
            }
            //nakon sto se odigraju sve karte iz ruke, treba promjesati spil i podjeliti nove karte
            else if (numOfCardsPlayed == 8)
            {
                whoTakesTheRound();
                //u tom se nalazi timer koji zavrsava rundu
            }                             
        }

       
        public void afterTimerYouPlayTick()
        {
            //tu se nalazi kraj svake runde
            youPlay = false;
            timer_youPlayCards.Stop();


            if (playerWhoPlay == 0)
            {
                mainPlayedCard.setColor(MyPlayedCard.getColor());
                mainPlayedCard.setID(MyPlayedCard.getID());
                mainPlayedCard.setName(MyPlayedCard.getName());
                mainPlayedCard.setValue(MyPlayedCard.getValue());

                timer_rightPlayerPlayed.Interval = getRandomNumber(1600, 2200);
                timer_rightPlayerPlayed.Start();

                timer_upperPlayerPlayed.Interval = getRandomNumber(3800, 4400);
                timer_upperPlayerPlayed.Start();

                timer_leftPlayerPlayed.Interval = getRandomNumber(6000, 6600);
                timer_leftPlayerPlayed.Start();

            }
            else if (playerWhoPlay == 1)
            {
                afterSomeoneLastPlayed();
            }
            else if (playerWhoPlay == 2)
            {
                timer_rightPlayerPlayed.Interval = getRandomNumber(1600, 2200);
                timer_rightPlayerPlayed.Start(); 

            }
            else if (playerWhoPlay == 3)
            {

                timer_rightPlayerPlayed.Interval = getRandomNumber(1600, 2200);
                timer_rightPlayerPlayed.Start();

                timer_upperPlayerPlayed.Interval = getRandomNumber(3800, 4400);
                timer_upperPlayerPlayed.Start();

            }
            
        }
        public void afterSomeoneLastPlayed()
        {
            numOfCardsPlayed++;
            whoTakesThePot();       
        }
       

        public void checkWhichCardCanPlay()
        {
            bool adut3 = false,adut2 = false,adut1 = false;
            int maxValue = 0;
            int higherAdut = 0, lowerAdut = 0, higherColor = 0, lowerColor = 0;
            int countAduts = 0;
            double tempID = 0;
            string mainColor = mainPlayedCard.getColor();


            for (int i = 0; i < 8; i++) playableCard[i] = false;

            if (LeftPlayerPlayedCard.getColor() == calledColor)
            {
                adut3 = true;
                if (LeftPlayerPlayedCard.getName() == "Decko") LeftPlayerPlayedCard.setPlayableID(LeftPlayerPlayedCard.getPlayableID()-4.6);
                if (LeftPlayerPlayedCard.getName() == "9") LeftPlayerPlayedCard.setPlayableID(LeftPlayerPlayedCard.getPlayableID() - 5.4);

            }
            if (UpperPlayerPlayedCard.getColor() == calledColor)
            {
                adut2 = true;
                if (UpperPlayerPlayedCard.getName() == "Decko") UpperPlayerPlayedCard.setPlayableID(UpperPlayerPlayedCard.getPlayableID() - 4.6);
                if (UpperPlayerPlayedCard.getName() == "9") UpperPlayerPlayedCard.setPlayableID(UpperPlayerPlayedCard.getPlayableID() - 5.4);

            }
            if (RightPlayerPlayedCard.getColor() == calledColor)
            {
                adut1 = true;
                if (RightPlayerPlayedCard.getName() == "Decko") RightPlayerPlayedCard.setPlayableID(RightPlayerPlayedCard.getPlayableID() - 4.6);
                if (RightPlayerPlayedCard.getName() == "9") RightPlayerPlayedCard.setPlayableID(RightPlayerPlayedCard.getPlayableID() - 5.4);

            }
            for (int i = 0; i < 8; i++)
            {
                if (myCard[i].getColor() == calledColor)
                {
                    if (myCard[i].getName() == "Decko") myCard[i].setPlayableID(myCard[i].getPlayableID() - 4.6);
                    if (myCard[i].getName() == "9") myCard[i].setPlayableID(myCard[i].getPlayableID() - 5.4);
                }
                    
            }



            if (playerWhoPlay == 0)
            {
                //ja igram prvi te mogu odigrati bilo koju kartu  
                for (int i = 0; i < 8; i++)
                {
                    if (myCard[i].getPlayed() == false)
                    {
                        playableCard[i] = true;
                    }
                }

            }
            else if (playerWhoPlay == 3)
            {
                maxValue = LeftPlayerPlayedCard.getValue();

                //prosli je odigro aduta
                if (adut3)
                {

                    for (int i = 0; i < 8; i++)
                    {
                        // prosli odigro aduta i ako imam jaceg aduta, ili ako su jednake jacine onda gledamo po ID tko ima manji znaci jaca karta
                        if (myCard[i].getPlayed() == false && myCard[i].getColor() == calledColor && myCard[i].getValue() >= maxValue && myCard[i].getPlayableID() < LeftPlayerPlayedCard.getPlayableID())
                        {
                            higherAdut++;
                            playableCard[i] = true;
                        }
                        //ako imam manjeg aduta
                        if (myCard[i].getPlayed() == false && myCard[i].getColor() == calledColor && myCard[i].getValue() <= maxValue && myCard[i].getPlayableID() > LeftPlayerPlayedCard.getPlayableID())
                        {
                            lowerAdut++;
                        }

                    }
                    //prosli je odigro aduta a ja nemam jaceg aduta vec samo slabijeg
                    if (higherAdut == 0 && lowerAdut != 0)
                    {
                        for (int i = 0; i < 8; i++)
                        {
                            if (myCard[i].getPlayed() == false && myCard[i].getColor() == calledColor && myCard[i].getValue() <= maxValue && myCard[i].getPlayableID() > LeftPlayerPlayedCard.getPlayableID())
                            {
                                playableCard[i] = true;
                            }

                        }
                    }
                    // prosli je odigro aduta ja nemam aduta onda mogu odigrati bilo koju 
                    if (higherAdut == 0 && lowerAdut == 0)
                    {
                        for (int i = 0; i < 8; i++)
                        {
                            if (myCard[i].getPlayed() == false)
                            {
                                playableCard[i] = true;
                            }

                        }
                    }

                }
                //##########################################
                //prosli nije odigro aduta
                else if (adut3 == false)
                {
                    for(int i = 0; i < 8; i++)
                    {
                        //ako imam jacu kartu u toj boji
                        if(myCard[i].getPlayed() == false && myCard[i].getColor() == mainColor && myCard[i].getValue() >= maxValue && myCard[i].getPlayableID() < LeftPlayerPlayedCard.getPlayableID())
                        {
                            playableCard[i] = true;
                            higherColor++;
                        }
                        //ako nemam jacu kartu u toj boji
                        if (myCard[i].getPlayed() == false && myCard[i].getColor() == mainColor && myCard[i].getValue() <= maxValue && myCard[i].getPlayableID() > LeftPlayerPlayedCard.getPlayableID())
                        {
                            lowerColor++;
                        }
                    }


                    //nemam jacu boju pa igram slabiju
                    if (higherColor == 0 && lowerColor != 0)
                    {
                        for (int i = 0; i < 8; i++)
                        {
                            if (myCard[i].getPlayed() == false && myCard[i].getColor() == mainColor && myCard[i].getValue() <= maxValue && myCard[i].getPlayableID() > LeftPlayerPlayedCard.getPlayableID())
                            {
                                playableCard[i] = true;
                            }

                        }
                    }
                    // neamam tu boju pa moram igrat aduta
                    else if (higherColor == 0 && lowerColor == 0)
                    {
                        for (int i = 0; i < 8; i++)
                        {
                            if (myCard[i].getPlayed() == false && myCard[i].getColor() == calledColor)
                            {
                                playableCard[i] = true;
                                countAduts++;                           
                            }

                        }
                        //nemam aduta pa igram bilo sta
                        if (countAduts == 0)
                        {
                            for (int i = 0; i < 8; i++)
                            {
                                if (myCard[i].getPlayed() == false)
                                {
                                    playableCard[i] = true;
                                }

                            }
                        }
                    }
                    


                }


//********************************************************************************************************************
            }
            else if(playerWhoPlay == 2)
            {
                //gornji je odigrao aduta prvi
                if (adut2)
                {
                    //oba su odigrala aduta
                    if (adut3)
                    {
                        //nadi tko je odirao jacu kartu
                        if (UpperPlayerPlayedCard.getValue() >= LeftPlayerPlayedCard.getValue() && UpperPlayerPlayedCard.getPlayableID() < LeftPlayerPlayedCard.getPlayableID())
                        {
                            maxValue = UpperPlayerPlayedCard.getValue();
                            tempID = UpperPlayerPlayedCard.getPlayableID();
                        }
                        else
                        {
                            maxValue = LeftPlayerPlayedCard.getValue();
                            tempID = LeftPlayerPlayedCard.getPlayableID();
                        }
                    }
                    //ako prosli nije odigrao aduta jaci je logicno gornji adut
                    else
                    {
                        maxValue = UpperPlayerPlayedCard.getValue();
                        tempID = UpperPlayerPlayedCard.getPlayableID();

                    }

                    for (int i = 0; i < 8; i++)
                        {
                            //ako imam jaceg aduta
                            if(myCard[i].getPlayed() == false && myCard[i].getColor() == calledColor && myCard[i].getValue() >= maxValue && myCard[i].getPlayableID() < tempID)
                            {
                                playableCard[i] = true;
                                higherAdut++;
                            }
                            //nadi koliko imam slabijih aduta
                            if (myCard[i].getPlayed() == false && myCard[i].getColor() == calledColor && myCard[i].getValue() <= maxValue && myCard[i].getPlayableID() >= tempID)
                            {
                                lowerAdut++;
                            }

                        }

                        //ako nemam jaceg aduta igram slabijeg
                        if(higherAdut == 0 && lowerAdut != 0)
                        {
                            for (int i = 0; i < 8; i++)
                            {
                                if (myCard[i].getPlayed() == false && myCard[i].getColor() == calledColor && myCard[i].getValue() <= maxValue && myCard[i].getPlayableID() >= tempID)
                                {
                                    playableCard[i] = true;
                                }
                            }
                                
                        }
                        //ako nemam ni jaceg ni slabijeg aduta igram bilo sta
                        else if (higherAdut == 0 && lowerAdut == 0)
                        {
                            for (int i = 0; i < 8; i++)
                            {
                                if (myCard[i].getPlayed() == false)
                                {
                                    playableCard[i] = true;
                                }
                            }

                        }
                    
                }
                //*********************************************************************************************************
                //ako gornji nije odigrao aduta vec neku boju
                else if(adut2 == false)
                {
                    //lijevi nije odigrao aduta tj nije sjeko nego prati boju
                    if(adut3 == false)
                    {
                        //lijevi je odigro kartu iste boje
                        if(LeftPlayerPlayedCard.getColor() == mainColor)
                        {
                            //nadi tko je odirao jacu kartu
                            if (UpperPlayerPlayedCard.getValue() >= LeftPlayerPlayedCard.getValue() && UpperPlayerPlayedCard.getPlayableID() < LeftPlayerPlayedCard.getPlayableID())
                            {
                                maxValue = UpperPlayerPlayedCard.getValue();
                                tempID = UpperPlayerPlayedCard.getPlayableID();
                            }
                            else
                            {
                                maxValue = LeftPlayerPlayedCard.getValue();
                                tempID = LeftPlayerPlayedCard.getPlayableID();
                            }
                        }
                        //lijevi je odigro neku drugu kartu druge boje
                        else
                        {
                            maxValue = UpperPlayerPlayedCard.getValue();
                            tempID = UpperPlayerPlayedCard.getPlayableID();
                        }
                              
                        
                        for (int i = 0; i < 8; i++)
                        {
                            //ako imam jacu kartu u toj boji
                            if (myCard[i].getPlayed() == false && myCard[i].getColor() == mainColor && myCard[i].getValue() >= maxValue && myCard[i].getPlayableID() < tempID)
                            {
                                playableCard[i] = true;
                                higherColor++;
                            }
                            //nadi koliko imam slabijih karata u toj boji
                            if (myCard[i].getPlayed() == false && myCard[i].getColor() == mainColor && myCard[i].getValue() <= maxValue && myCard[i].getPlayableID() >= tempID)
                            {
                                lowerColor++;
                            }

                        }

                        //ako nemam jacu kartu u boji igram slabiju
                        if (higherColor == 0 && lowerColor != 0)
                        {
                            for (int i = 0; i < 8; i++)
                            {
                                if (myCard[i].getPlayed() == false && myCard[i].getColor() == mainColor && myCard[i].getValue() <= maxValue && myCard[i].getPlayableID() >= tempID)
                                {
                                    playableCard[i] = true;
                                }
                            }

                        }
                        //ako nemam ni jacu ni slabiju
                        else if (higherColor == 0 && lowerColor == 0)
                        {
                            
                            //igram aduta
                            for(int i = 0; i < 8; i++)
                            {
                                if(myCard[i].getPlayed() == false && myCard[i].getColor() == calledColor)
                                {
                                    playableCard[i] = true;
                                    higherAdut++;
                                }
                            }
                            //ako nemam aduta igram bilo sta
                            if (higherAdut == 0)
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (myCard[i].getPlayed() == false)
                                    {
                                        playableCard[i] = true;                                    
                                    }
                                }
                            }

                        }


                        
                    }
                    //lijevi je odigrao aduta tj sjeko je
                    else if(adut3)
                    {

                        //postavi jacu kartu
                        maxValue = LeftPlayerPlayedCard.getValue();
                        tempID = LeftPlayerPlayedCard.getPlayableID();

                        for (int i = 0; i < 8; i++)
                        {
                            //ako imam tu boju igram boju
                            if (myCard[i].getPlayed() == false && myCard[i].getColor() == mainColor)
                            {
                                playableCard[i] = true;
                                higherColor++;
                            }
                        }
                        //ako nemam boju igram jaceg aduta
                        if(higherColor == 0)
                        {
                            for (int i = 0; i < 8; i++)
                            {
                                //nadi jaceg aduta
                                if (myCard[i].getPlayed() == false && myCard[i].getColor() == calledColor && myCard[i].getValue() >= maxValue && myCard[i].getPlayableID() < tempID)
                                {
                                    playableCard[i] = true;
                                    higherAdut++;
                                }
                                //nadi koliko imam slabijih aduta
                                if (myCard[i].getPlayed() == false && myCard[i].getColor() == calledColor && myCard[i].getValue() <= maxValue && myCard[i].getPlayableID() >= tempID)
                                {
                                    lowerAdut++;
                                }

                            }

                            //ako nemam jacih aduta igram slabijih
                            if (higherAdut == 0 && lowerAdut != 0)
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (myCard[i].getPlayed() == false && myCard[i].getColor() == calledColor && myCard[i].getValue() <= maxValue && myCard[i].getPlayableID() >= tempID)
                                    {
                                        playableCard[i] = true;
                                    }
                                }

                            }
                            //ako nemam aduta igram bilo sta
                            else if(higherAdut == 0 && lowerAdut == 0)
                            {

                                for (int i = 0; i < 8; i++)
                                {
                                    if (myCard[i].getPlayed() == false)
                                    {
                                        playableCard[i] = true;
                                    }
                                }
                            }
                        }
                        

                    }
                    
                }
            }
            else if(playerWhoPlay == 1)
            {
                //************************************************************************

                //desni je odigrao aduta prvi
                if (adut1)
                {
                    //sva trojca su odigrala aduta
                    if (adut2 && adut3)
                    {
                        //nadi tko je odirao jacu kartu
                        //najjaci je desni
                        if (RightPlayerPlayedCard.getValue() >= UpperPlayerPlayedCard.getValue() && RightPlayerPlayedCard.getValue() >= LeftPlayerPlayedCard.getValue() && RightPlayerPlayedCard.getPlayableID() < UpperPlayerPlayedCard.getPlayableID() && RightPlayerPlayedCard.getPlayableID() < LeftPlayerPlayedCard.getPlayableID())
                        {
                            maxValue = RightPlayerPlayedCard.getValue();
                            tempID = RightPlayerPlayedCard.getPlayableID();
                        }
                        //najjaci nadut ima gornji
                        else if (UpperPlayerPlayedCard.getValue() >= RightPlayerPlayedCard.getValue() && UpperPlayerPlayedCard.getValue() >= LeftPlayerPlayedCard.getValue() && UpperPlayerPlayedCard.getPlayableID() < RightPlayerPlayedCard.getPlayableID() && UpperPlayerPlayedCard.getPlayableID() < LeftPlayerPlayedCard.getPlayableID())
                        {
                            maxValue = UpperPlayerPlayedCard.getValue();
                            tempID = UpperPlayerPlayedCard.getPlayableID();
                        }
                        //lijevi ima najjaci adut
                        else
                        {
                            maxValue = LeftPlayerPlayedCard.getValue();
                            tempID = LeftPlayerPlayedCard.getPlayableID();
                        }

                    }
                    //gornji je odigro aduta a lijevi nije
                    else if (adut2 && adut3 == false)
                    {
                        //nadi tko je odirao jacu kartu
                        if (RightPlayerPlayedCard.getValue() >= UpperPlayerPlayedCard.getValue() && RightPlayerPlayedCard.getPlayableID() < UpperPlayerPlayedCard.getPlayableID())
                        {
                            maxValue = RightPlayerPlayedCard.getValue();
                            tempID = RightPlayerPlayedCard.getPlayableID();
                        }
                        else
                        {
                            maxValue = UpperPlayerPlayedCard.getValue();
                            tempID = UpperPlayerPlayedCard.getPlayableID();
                        }
                    }
                    //lijevi je odigro aduta a desni nije
                    else if (adut2 == false && adut3)
                    {
                        //nadi tko je odirao jacu kartu
                        if (RightPlayerPlayedCard.getValue() >= LeftPlayerPlayedCard.getValue() && RightPlayerPlayedCard.getPlayableID() < LeftPlayerPlayedCard.getPlayableID())
                        {
                            maxValue = RightPlayerPlayedCard.getValue();
                            tempID = RightPlayerPlayedCard.getPlayableID();
                        }
                        else
                        {
                            maxValue = LeftPlayerPlayedCard.getValue();
                            tempID = LeftPlayerPlayedCard.getPlayableID();
                        }
                    }
                    //samo desni je odigrao aduta, ova dvojca nisu
                    else
                    {
                        maxValue = RightPlayerPlayedCard.getValue();
                        tempID = RightPlayerPlayedCard.getPlayableID();

                    }

                    for (int i = 0; i < 8; i++)
                    {
                        //ako imam jaceg aduta
                        if (myCard[i].getPlayed() == false && myCard[i].getColor() == calledColor && myCard[i].getValue() >= maxValue && myCard[i].getPlayableID() < tempID)
                        {
                            playableCard[i] = true;
                            higherAdut++;
                        }
                        //nadi koliko imam slabijih aduta
                        if (myCard[i].getPlayed() == false && myCard[i].getColor() == calledColor && myCard[i].getValue() <= maxValue && myCard[i].getPlayableID() >= tempID)
                        {
                            lowerAdut++;
                        }

                    }

                    //ako nemam jaceg aduta igram slabijeg
                    if (higherAdut == 0 && lowerAdut != 0)
                    {
                        for (int i = 0; i < 8; i++)
                        {
                            if (myCard[i].getPlayed() == false && myCard[i].getColor() == calledColor && myCard[i].getValue() <= maxValue && myCard[i].getPlayableID() >= tempID)
                            {
                                playableCard[i] = true;
                            }
                        }

                    }
                    //ako nemam ni jaceg ni slabijeg aduta igram bilo sta
                    else if (higherAdut == 0 && lowerAdut == 0)
                    {
                        for (int i = 0; i < 8; i++)
                        {
                            if (myCard[i].getPlayed() == false)
                            {
                                playableCard[i] = true;
                            }
                        }

                    }

                }
                //*********************************************************************************************************
                //ako gornji nije odigrao aduta vec neku boju
                else if (adut1 == false)
                {
                    //sva trojca postivaju boju
                    if (UpperPlayerPlayedCard.getColor() == mainColor && LeftPlayerPlayedCard.getColor() == mainColor)
                    {
                        //nadi tko je odirao jacu kartu
                        //najjaci je desni
                        if (RightPlayerPlayedCard.getValue() >= UpperPlayerPlayedCard.getValue() && RightPlayerPlayedCard.getValue() >= LeftPlayerPlayedCard.getValue() && RightPlayerPlayedCard.getPlayableID() < UpperPlayerPlayedCard.getPlayableID() && RightPlayerPlayedCard.getPlayableID() < LeftPlayerPlayedCard.getPlayableID())
                        {
                            maxValue = RightPlayerPlayedCard.getValue();
                            tempID = RightPlayerPlayedCard.getPlayableID();
                        }
                        //najjacu kartu ima gornji
                        else if (UpperPlayerPlayedCard.getValue() >= RightPlayerPlayedCard.getValue() && UpperPlayerPlayedCard.getValue() >= LeftPlayerPlayedCard.getValue() && UpperPlayerPlayedCard.getPlayableID() < RightPlayerPlayedCard.getPlayableID() && UpperPlayerPlayedCard.getPlayableID() < LeftPlayerPlayedCard.getPlayableID())
                        {
                            maxValue = UpperPlayerPlayedCard.getValue();
                            tempID = UpperPlayerPlayedCard.getPlayableID();
                        }
                        //lijevi ima najjacu kartu
                        else
                        {
                            maxValue = LeftPlayerPlayedCard.getValue();
                            tempID = LeftPlayerPlayedCard.getPlayableID();
                        }

                        //********************
                        for (int i = 0; i < 8; i++)
                        {
                            //ako imam jacu kartu
                            if (myCard[i].getPlayed() == false && myCard[i].getColor() == mainColor && myCard[i].getValue() >= maxValue && myCard[i].getPlayableID() < tempID)
                            {
                                playableCard[i] = true;
                                higherColor++;
                            }
                            //nadi koliko imam slabijih karata
                            if (myCard[i].getPlayed() == false && myCard[i].getColor() == mainColor && myCard[i].getValue() <= maxValue && myCard[i].getPlayableID() >= tempID)
                            {
                                lowerColor++;
                            }

                        }

                        //ako nemam jacu kartu igram slabiju
                        if (higherColor == 0 && lowerColor != 0)
                        {
                            for (int i = 0; i < 8; i++)
                            {
                                if (myCard[i].getPlayed() == false && myCard[i].getColor() == mainColor && myCard[i].getValue() <= maxValue && myCard[i].getPlayableID() >= tempID)
                                {
                                    playableCard[i] = true;
                                }
                            }

                        }
                        //ako nemam ni jacu ni slabiju kartu igram aduta
                        else if (higherColor == 0 && lowerColor == 0)
                        {
                            for (int i = 0; i < 8; i++)
                            {
                                if (myCard[i].getPlayed() == false && myCard[i].getColor() == calledColor)
                                {
                                    playableCard[i] = true;
                                    higherAdut++;
                                }
                            }
                            //ako nemam ni aduta igram bilo sta
                            if(higherAdut == 0)
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (myCard[i].getPlayed() == false )
                                    {
                                        playableCard[i] = true;
                                        
                                    }
                                }
                            }


                        }
                        //**********************





                    }
                    //gornji postiva boju, lijevi sjece adutom
                    else if(UpperPlayerPlayedCard.getColor() == mainColor && LeftPlayerPlayedCard.getColor() == calledColor)
                    {
                        for(int i = 0; i < 8; i++)
                        {
                            //jel imam ja tu boju
                            if(myCard[i].getPlayed() == false && myCard[i].getColor() == mainColor)
                            {
                                playableCard[i] = true;
                                higherColor++;
                            }
                        }
                        //nemam boju pa moram igrat aduta, ako ne onda igram bilo sta
                        if(higherColor == 0)
                        {
                            maxValue = LeftPlayerPlayedCard.getValue();
                            tempID = LeftPlayerPlayedCard.getPlayableID();

                            //nadi jaceg aduta
                            for (int i = 0; i < 8; i++)
                            {
                                //ako imam jaceg aduta
                                if (myCard[i].getPlayed() == false && myCard[i].getColor() == calledColor && myCard[i].getValue() >= maxValue && myCard[i].getPlayableID() < tempID)
                                {
                                    playableCard[i] = true;
                                    higherAdut++;
                                }
                                //nadi koliko imam slabijih aduta
                                if (myCard[i].getPlayed() == false && myCard[i].getColor() == calledColor && myCard[i].getValue() <= maxValue && myCard[i].getPlayableID() >= tempID)
                                {
                                    lowerAdut++;
                                }

                            }

                            //***
                            //ako nemam jaceg aduta igram slabijeg
                            if (higherAdut == 0 && lowerAdut != 0)
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (myCard[i].getPlayed() == false && myCard[i].getColor() == calledColor && myCard[i].getValue() <= maxValue && myCard[i].getPlayableID() >= tempID)
                                    {
                                        playableCard[i] = true;
                                    }
                                }

                            }
                            //ako nemam ni jaceg ni slabijeg aduta igram bilo sta
                            else if (higherAdut == 0 && lowerAdut == 0)
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (myCard[i].getPlayed() == false)
                                    {
                                        playableCard[i] = true;
                                    }
                                }

                            }

                        }
                    }
                    //gornji sjece adutom a ljevi igra boju
                    else if (UpperPlayerPlayedCard.getColor() == calledColor && LeftPlayerPlayedCard.getColor() == mainColor)
                    {
                        for (int i = 0; i < 8; i++)
                        {
                            //jel imam ja tu boju
                            if (myCard[i].getPlayed() == false && myCard[i].getColor() == mainColor)
                            {
                                playableCard[i] = true;
                                higherColor++;
                            }
                        }
                        //nemam boju pa moram igrat aduta, ako ne onda igram bilo sta
                        if (higherColor == 0)
                        {
                            maxValue = UpperPlayerPlayedCard.getValue();
                            tempID = UpperPlayerPlayedCard.getPlayableID();

                            //nadi jaceg aduta
                            for (int i = 0; i < 8; i++)
                            {
                                //ako imam jaceg aduta
                                if (myCard[i].getPlayed() == false && myCard[i].getColor() == calledColor && myCard[i].getValue() >= maxValue && myCard[i].getPlayableID() < tempID)
                                {
                                    playableCard[i] = true;
                                    higherAdut++;
                                }
                                //nadi koliko imam slabijih aduta
                                if (myCard[i].getPlayed() == false && myCard[i].getColor() == calledColor && myCard[i].getValue() <= maxValue && myCard[i].getPlayableID() >= tempID)
                                {
                                    lowerAdut++;
                                }

                            }

                            //***
                            //ako nemam jaceg aduta igram slabijeg
                            if (higherAdut == 0 && lowerAdut != 0)
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (myCard[i].getPlayed() == false && myCard[i].getColor() == calledColor && myCard[i].getValue() <= maxValue && myCard[i].getPlayableID() >= tempID)
                                    {
                                        playableCard[i] = true;
                                    }
                                }

                            }
                            //ako nemam ni jaceg ni slabijeg aduta igram bilo sta
                            else if (higherAdut == 0 && lowerAdut == 0)
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (myCard[i].getPlayed() == false)
                                    {
                                        playableCard[i] = true;
                                    }
                                }

                            }

                        }
                    }
                    //obojca sjeku adutom
                    else if (UpperPlayerPlayedCard.getColor() == calledColor && LeftPlayerPlayedCard.getColor() == calledColor)
                    {
                        for (int i = 0; i < 8; i++)
                        {
                            //jel imam ja tu boju
                            if (myCard[i].getPlayed() == false && myCard[i].getColor() == mainColor)
                            {
                                playableCard[i] = true;
                                higherColor++;
                            }
                        }
                        //nemam boju pa moram igrat aduta, ako ne onda igram bilo sta
                        if (higherColor == 0)
                        {
                            if(UpperPlayerPlayedCard.getValue() >= LeftPlayerPlayedCard.getValue() && UpperPlayerPlayedCard.getPlayableID() < LeftPlayerPlayedCard.getPlayableID())
                            {
                                maxValue = UpperPlayerPlayedCard.getValue();
                                tempID = UpperPlayerPlayedCard.getPlayableID();
                            }
                            else
                            {
                                maxValue = LeftPlayerPlayedCard.getValue();
                                tempID = LeftPlayerPlayedCard.getPlayableID();
                            }
                            

                            //nadi jaceg aduta
                            for (int i = 0; i < 8; i++)
                            {
                                //ako imam jaceg aduta
                                if (myCard[i].getPlayed() == false && myCard[i].getColor() == calledColor && myCard[i].getValue() >= maxValue && myCard[i].getPlayableID() < tempID)
                                {
                                    playableCard[i] = true;
                                    higherAdut++;
                                }
                                //nadi koliko imam slabijih aduta
                                if (myCard[i].getPlayed() == false && myCard[i].getColor() == calledColor && myCard[i].getValue() <= maxValue && myCard[i].getPlayableID() >= tempID)
                                {
                                    lowerAdut++;
                                }

                            }

                            //***
                            //ako nemam jaceg aduta igram slabijeg
                            if (higherAdut == 0 && lowerAdut != 0)
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (myCard[i].getPlayed() == false && myCard[i].getColor() == calledColor && myCard[i].getValue() <= maxValue && myCard[i].getPlayableID() >= tempID)
                                    {
                                        playableCard[i] = true;
                                    }
                                }

                            }
                            //ako nemam ni jaceg ni slabijeg aduta igram bilo sta
                            else if (higherAdut == 0 && lowerAdut == 0)
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (myCard[i].getPlayed() == false)
                                    {
                                        playableCard[i] = true;
                                    }
                                }

                            }

                        }
                    }
                    //obojca igraju bilo sta
                    else
                    {
                        maxValue = RightPlayerPlayedCard.getValue();
                        tempID = RightPlayerPlayedCard.getPlayableID();


                        //jel imam jacu boju
                        for (int i = 0; i < 8; i++)
                        {
                            //ako imam jacu boju
                            if (myCard[i].getPlayed() == false && myCard[i].getColor() == mainColor && myCard[i].getValue() >= maxValue && myCard[i].getPlayableID() < tempID)
                            {
                                playableCard[i] = true;
                                higherColor++;
                            }
                            //nadi koliko imam slabijih aduta
                            if (myCard[i].getPlayed() == false && myCard[i].getColor() == mainColor && myCard[i].getValue() <= maxValue && myCard[i].getPlayableID() >= tempID)
                            {
                                lowerColor++;
                            }

                        }

                        //***
                        //ako nemam jacu kartu igram slabiju
                        if (higherColor == 0 && lowerColor != 0)
                        {
                            for (int i = 0; i < 8; i++)
                            {
                                if (myCard[i].getPlayed() == false && myCard[i].getColor() == mainColor && myCard[i].getValue() <= maxValue && myCard[i].getPlayableID() >= tempID)
                                {
                                    playableCard[i] = true;
                                }
                            }

                        }
                        //ako nemam ni jacu ni slabiju kartu u toj boji igram aduta
                        else if (higherColor == 0 && lowerColor == 0)
                        {
                            for (int i = 0; i < 8; i++)
                            {
                                if (myCard[i].getPlayed() == false  && myCard[i].getColor() == calledColor)
                                {
                                    playableCard[i] = true;
                                    higherAdut++;
                                }
                            }

                            if(higherAdut == 0)
                            {
                                //ako nemam ni aduta igram bilo sta
                                for (int i = 0; i < 8; i++)
                                {
                                    if (myCard[i].getPlayed() == false)
                                    {
                                        playableCard[i] = true;
                                    }
                                }
                            }
                            

                        }


                    }


                }

            }

            //************************************************************************
            /*
            tb_playableCards.Text = "";
            for(int i = 0; i < 8; i++)
            {
               tb_playableCards.Text += playableCard[i] + Environment.NewLine;
            }
            */

        }
        
        public void whoWonTheMatch()
        {
            bool weWon = false;
            if (ourSum > 1000 && theirSum > 1000)
            {
                if (possibleCaller == 0 || possibleCaller == 2) weWon = true;

            }
            else if (ourSum > 1000) weWon = true;

            if (weWon)
            {
                MessageBox.Show("Bravooo pobjediliii stee!!!\nIgra gotova...");
                Int32.TryParse(getStatisticsByLine(0), out totalScoreOur);
                totalScoreOur++;
                writeScatisticToSpecificLine(totalScoreOur.ToString(), 0);
            }
            else
            {
                MessageBox.Show("Izgubili ste ovu.. bit ce bolja iduca!!");
                Int32.TryParse(getStatisticsByLine(1), out totalScoreTheir);
                totalScoreTheir++;
                writeScatisticToSpecificLine(totalScoreTheir.ToString(), 1);
            }

            btn_newGame.Enabled = true;
            


        }    
        public void whoTakesTheRound()
        {
            int totalOur, totalTheir;

            Int32.TryParse(getStatisticsByLine(2), out totalOur);
            Int32.TryParse(getStatisticsByLine(3), out totalTheir);


            if (lb_showWhoPass.Visible == false)lb_showWhoPass.Visible = true;

            if (ourRoundSum == 0) theirRoundSum += 90;
            else if (theirRoundSum == 0) ourRoundSum += 90;

            if(possibleCaller == 0 || possibleCaller == 2)
            {
                if (ourRoundSum > theirRoundSum)
                {
                    ourSum += ourRoundSum;
                    theirSum += theirRoundSum;

                    totalOur += ourRoundSum;
                    totalTheir += theirRoundSum;



                    lb_showWhoPass.Text = "Prosli smo! Nastavljamo dalje...";            
                }
                else
                {
                    theirSum += ourRoundSum + theirRoundSum;

                    totalTheir += ourRoundSum + theirRoundSum;

                    lb_showWhoPass.Text = "Pali smo! Nastavljamo dalje...";
                }
                
            }
            else
            {
                if (theirRoundSum > ourRoundSum)
                {
                    ourSum += ourRoundSum;
                    theirSum += theirRoundSum;

                    totalOur += ourRoundSum;
                    totalTheir += theirRoundSum;

                    lb_showWhoPass.Text = "Prosli su! Nastavljamo dalje...";
                }
                else
                {
                    ourSum += ourRoundSum + theirRoundSum;

                    totalOur += ourRoundSum + theirRoundSum;

                    lb_showWhoPass.Text = "Pali su! Nastavljamo dalje...";
                }
            }


            ourRoundSum = 0;
            theirRoundSum = 0;
            lb_scoreOur.Text = ourSum.ToString();
            lb_scoreTheir.Text = theirSum.ToString();
            lb_ourPotSum.Text = "(" + ourRoundSum.ToString() + ")";
            lb_theirPotSum.Text = "(" + theirRoundSum.ToString() + ")";

            
           
            

            writeScatisticToSpecificLine(totalOur.ToString(), 2);
            writeScatisticToSpecificLine(totalTheir.ToString(), 3);

            timer_showWhoPass.Start();
        }
        public void whoTakesThePot()
        {

            int player0Sum, player1Sum, player2Sum, player3Sum;

            Int32.TryParse(getStatisticsByLine(4), out player0Sum);
            Int32.TryParse(getStatisticsByLine(5), out player1Sum);
            Int32.TryParse(getStatisticsByLine(6), out player2Sum);
            Int32.TryParse(getStatisticsByLine(7), out player3Sum);

            int potSum = MyPlayedCard.getValue() + UpperPlayerPlayedCard.getValue() + RightPlayerPlayedCard.getValue() + LeftPlayerPlayedCard.getValue();
            int maxValue = 0;

            bool adut0 = false, adut1 = false, adut2 = false, adut3 = false;
            bool weTakeThePot = false;

            bool rightColor0 = false, rightColor1 = false, rightColor2 = false, rightColor3 = false;

            if (MyPlayedCard.getColor() == calledColor) adut0 = true;
            if (RightPlayerPlayedCard.getColor() == calledColor) adut1 = true;
            if (UpperPlayerPlayedCard.getColor() == calledColor) adut2 = true;
            if (LeftPlayerPlayedCard.getColor() == calledColor) adut3 = true;

            if (MyPlayedCard.getColor() == mainPlayedCard.getColor()) rightColor0 = true;
            if (RightPlayerPlayedCard.getColor() == mainPlayedCard.getColor()) rightColor1 = true;
            if (UpperPlayerPlayedCard.getColor() == mainPlayedCard.getColor()) rightColor2 = true;
            if (LeftPlayerPlayedCard.getColor() == mainPlayedCard.getColor()) rightColor3 = true;

            if (MyPlayedCard.getName() == "9" && adut0 == false) MyPlayedCard.setValue(-33);
            else if (MyPlayedCard.getName() == "8") MyPlayedCard.setValue(-44);
            else if (MyPlayedCard.getName() == "7") MyPlayedCard.setValue(-55);

            if (RightPlayerPlayedCard.getName() == "9" && adut1 == false) RightPlayerPlayedCard.setValue(-33);
            else if (RightPlayerPlayedCard.getName() == "8") RightPlayerPlayedCard.setValue(-44);
            else if (RightPlayerPlayedCard.getName() == "7") RightPlayerPlayedCard.setValue(-55);
 
            if (UpperPlayerPlayedCard.getName() == "9" && adut2 == false) UpperPlayerPlayedCard.setValue(-33);
            else if (UpperPlayerPlayedCard.getName() == "8") UpperPlayerPlayedCard.setValue(-44);
            else if (UpperPlayerPlayedCard.getName() == "7") UpperPlayerPlayedCard.setValue(-55);

            if (LeftPlayerPlayedCard.getName() == "9" && adut3 == false) LeftPlayerPlayedCard.setValue(-33);
            else if (LeftPlayerPlayedCard.getName() == "8") LeftPlayerPlayedCard.setValue(-44);
            else if (LeftPlayerPlayedCard.getName() == "7") LeftPlayerPlayedCard.setValue(-55);

            if (MyPlayedCard.getValue() < 0 && MyPlayedCard.getColor() == mainPlayedCard.getColor()) MyPlayedCard.setValue(MyPlayedCard.getValue() + 5);
            if (RightPlayerPlayedCard.getValue() < 0 && RightPlayerPlayedCard.getColor() == mainPlayedCard.getColor()) RightPlayerPlayedCard.setValue(RightPlayerPlayedCard.getValue() + 5);
            if (UpperPlayerPlayedCard.getValue() < 0 && UpperPlayerPlayedCard.getColor() == mainPlayedCard.getColor()) UpperPlayerPlayedCard.setValue(UpperPlayerPlayedCard.getValue() + 5);
            if (LeftPlayerPlayedCard.getValue() < 0 && LeftPlayerPlayedCard.getColor() == mainPlayedCard.getColor()) LeftPlayerPlayedCard.setValue(LeftPlayerPlayedCard.getValue() + 5);


            //s adutima
            if (adut0 || adut1 || adut2 || adut3)
            {
                if ((adut0 || adut2) && adut1 == false && adut3 == false) weTakeThePot = true;
                else if (adut0 && adut1 && adut2 == false && adut3 == false)
                {
                    if (MyPlayedCard.getValue() > RightPlayerPlayedCard.getValue()) weTakeThePot = true;
                }

                else if (adut0 && adut3 && adut2 == false && adut1 == false)
                {
                    if (MyPlayedCard.getValue() > LeftPlayerPlayedCard.getValue()) weTakeThePot = true;
                }
                else if (adut2 && adut1 && adut0 == false && adut3 == false)
                {
                    if (UpperPlayerPlayedCard.getValue() > RightPlayerPlayedCard.getValue()) weTakeThePot = true;
                }
                else if (adut2 && adut3 && adut0 == false && adut1 == false)
                {
                    if (UpperPlayerPlayedCard.getValue() > LeftPlayerPlayedCard.getValue()) weTakeThePot = true;
                }
                else if (adut0 && adut1 && adut2 && adut3 == false)
                {
                    if (MyPlayedCard.getValue() > UpperPlayerPlayedCard.getValue()) maxValue = MyPlayedCard.getValue();
                    else maxValue = UpperPlayerPlayedCard.getValue();
                    if (RightPlayerPlayedCard.getValue() < maxValue) weTakeThePot = true;
                }
                else if (adut0 && adut3 && adut2 && adut1 == false)
                {
                    if (MyPlayedCard.getValue() > UpperPlayerPlayedCard.getValue()) maxValue = MyPlayedCard.getValue();
                    else maxValue = UpperPlayerPlayedCard.getValue();
                    if (LeftPlayerPlayedCard.getValue() < maxValue) weTakeThePot = true;
                }
                else if (adut0 && adut3 && adut1 && adut2 == false)
                {
                    if (LeftPlayerPlayedCard.getValue() > RightPlayerPlayedCard.getValue()) maxValue = LeftPlayerPlayedCard.getValue();
                    else maxValue = RightPlayerPlayedCard.getValue();
                    if (MyPlayedCard.getValue() > maxValue) weTakeThePot = true;
                }
                else if (adut1 && adut2 && adut3 && adut0 == false)
                {
                    if (LeftPlayerPlayedCard.getValue() > RightPlayerPlayedCard.getValue()) maxValue = LeftPlayerPlayedCard.getValue();
                    else maxValue = RightPlayerPlayedCard.getValue();
                    if (UpperPlayerPlayedCard.getValue() > maxValue) weTakeThePot = true;
                }
                else if (adut0 && adut1 && adut2 && adut3)
                {
                    if (MyPlayedCard.getValue() > UpperPlayerPlayedCard.getValue()) maxValue = MyPlayedCard.getValue();
                    else maxValue = UpperPlayerPlayedCard.getValue();
                    if (RightPlayerPlayedCard.getValue() < maxValue && LeftPlayerPlayedCard.getValue() < maxValue) weTakeThePot = true;
                }
                else weTakeThePot = false;

            }
            //bez aduta
            else
            {
                if ((rightColor0 || rightColor2) && rightColor1 == false && rightColor3 == false) weTakeThePot = true;
                else if (rightColor0 && rightColor1 && rightColor2 == false && rightColor3 == false)
                {
                    if (MyPlayedCard.getValue() > RightPlayerPlayedCard.getValue()) weTakeThePot = true;
                }

                else if (rightColor0 && rightColor3 && rightColor2 == false && rightColor1 == false)
                {
                    if (MyPlayedCard.getValue() > LeftPlayerPlayedCard.getValue()) weTakeThePot = true;
                }
                else if (rightColor2 && rightColor1 && rightColor0 == false && rightColor3 == false)
                {
                    if (UpperPlayerPlayedCard.getValue() > RightPlayerPlayedCard.getValue()) weTakeThePot = true;
                }
                else if (rightColor2 && rightColor3 && rightColor0 == false && rightColor1 == false)
                {
                    if (UpperPlayerPlayedCard.getValue() > LeftPlayerPlayedCard.getValue()) weTakeThePot = true;
                }
                else if (rightColor0 && rightColor1 && rightColor2 && rightColor3 == false)
                {
                    if (MyPlayedCard.getValue() > UpperPlayerPlayedCard.getValue()) maxValue = MyPlayedCard.getValue();
                    else maxValue = UpperPlayerPlayedCard.getValue();
                    if (RightPlayerPlayedCard.getValue() < maxValue) weTakeThePot = true;
                }
                else if (rightColor0 && rightColor3 && rightColor2 && rightColor1 == false)
                {
                    if (MyPlayedCard.getValue() > UpperPlayerPlayedCard.getValue()) maxValue = MyPlayedCard.getValue();
                    else maxValue = UpperPlayerPlayedCard.getValue();
                    if (LeftPlayerPlayedCard.getValue() < maxValue) weTakeThePot = true;
                }
                else if (rightColor0 && rightColor3 && rightColor1 && rightColor2 == false)
                {
                    if (LeftPlayerPlayedCard.getValue() > RightPlayerPlayedCard.getValue()) maxValue = LeftPlayerPlayedCard.getValue();
                    else maxValue = RightPlayerPlayedCard.getValue();
                    if (MyPlayedCard.getValue() > maxValue) weTakeThePot = true;
                }
                else if (rightColor1 && rightColor2 && rightColor3 && rightColor0 == false)
                {
                    if (LeftPlayerPlayedCard.getValue() > RightPlayerPlayedCard.getValue()) maxValue = LeftPlayerPlayedCard.getValue();
                    else maxValue = RightPlayerPlayedCard.getValue();
                    if (UpperPlayerPlayedCard.getValue() > maxValue) weTakeThePot = true;
                }
                else if (rightColor0 && rightColor1 && rightColor2 && rightColor3)
                {
                    if (MyPlayedCard.getValue() > UpperPlayerPlayedCard.getValue()) maxValue = MyPlayedCard.getValue();
                    else maxValue = UpperPlayerPlayedCard.getValue();
                    if (RightPlayerPlayedCard.getValue() < maxValue && LeftPlayerPlayedCard.getValue() < maxValue) weTakeThePot = true;
                }
                else weTakeThePot = false;
            }

            if (numOfCardsPlayed == 8 && weTakeThePot) ourRoundSum += 10;
            else if (numOfCardsPlayed == 8 && weTakeThePot == false) theirRoundSum += 10;
            

            if (weTakeThePot) ourRoundSum += potSum;
            else theirRoundSum += potSum;
            
            lb_ourPotSum.Text = "(" + ourRoundSum.ToString() + ")";
            lb_theirPotSum.Text = "(" + theirRoundSum.ToString() + ")";

          

            if (weTakeThePot)
            {
                if (adut0 && adut2)
                {
                    if (MyPlayedCard.getValue() > UpperPlayerPlayedCard.getValue()) playerWhoPlay = 0;
                    else playerWhoPlay = 2;
                }
                else if (adut0 && adut2 == false) playerWhoPlay = 0;
                else if (adut0 == false && adut2) playerWhoPlay = 2;
                else if(rightColor0 && rightColor2)
                {
                    if (MyPlayedCard.getValue() > UpperPlayerPlayedCard.getValue()) playerWhoPlay = 0;
                    else playerWhoPlay = 2;
                }
                else if (rightColor0 && rightColor2 == false) playerWhoPlay = 0;
                else if (rightColor0 == false && rightColor2) playerWhoPlay = 2;
            }
            else
            {

                if (adut1 && adut3)
                {
                    if (RightPlayerPlayedCard.getValue() > LeftPlayerPlayedCard.getValue()) playerWhoPlay = 1;
                    else playerWhoPlay = 3;
                }
                else if (adut1 && adut3 == false) playerWhoPlay = 1;
                else if (adut1 == false && adut3) playerWhoPlay = 3;
                else if (rightColor1 && rightColor3)
                {
                    if (RightPlayerPlayedCard.getValue() > LeftPlayerPlayedCard.getValue()) playerWhoPlay = 1;
                    else playerWhoPlay = 3;
                }
                else if (rightColor1 && rightColor3 == false) playerWhoPlay = 1;
                else if (rightColor1 == false && rightColor3) playerWhoPlay = 3;
            }


            
            if(playerWhoPlay == 0)
            {
                lb_yourName.BackColor = Color.Chocolate;
                player0Sum += potSum;
            }
            else if(playerWhoPlay == 1)
            {
                lb_opponentRightName.BackColor = Color.Chocolate;
                player1Sum += potSum;
            }
            else if (playerWhoPlay == 2)
            {
                lb_yourTeammateName.BackColor = Color.Chocolate;
                player2Sum += potSum;
            }
            else if (playerWhoPlay == 3)
            {
                lb_opponentLeftName.BackColor = Color.Chocolate;
                player3Sum += potSum;
            }

            //dodaje u statistiku zadnjio stih 10 tko nosi
            if (numOfCardsPlayed == 8)
            {
                if (playerWhoPlay == 0)
                {                   
                    player0Sum += 10;
                }
                else if (playerWhoPlay == 1)
                {
                    player1Sum += 10;
                }
                else if (playerWhoPlay == 2)
                {
                    player2Sum += 10;
                }
                else if (playerWhoPlay == 3)
                {
                    player3Sum += 10;
                }
            }



            timer_animationWhoTakesThePot.Start();

            writeScatisticToSpecificLine(player0Sum.ToString(), 4);
            writeScatisticToSpecificLine(player1Sum.ToString(), 5);
            writeScatisticToSpecificLine(player2Sum.ToString(), 6);
            writeScatisticToSpecificLine(player3Sum.ToString(), 7);
        }

        public void timer_showWhoPass_Tick(object sender, EventArgs e)
        {
            timer_showWhoPass.Stop();

            lb_showWhoPass.Text = "";
            lb_showWhoPass.Visible = false;

            restartForCalling();
            erase8DownCards();

            if (ourSum > 1000 || theirSum > 1000)
            {
                whoWonTheMatch();
            }
            else
            {
                timer_shuffleCards.Interval = getRandomNumber(2000, 5000);
                timer_shuffleCards.Start();
                Player_ShufflingCards.PlayLooping();
                //kod isteka timera se nalazi share6cards i play calling
            }
        }

        public void timer_animationWhoTakesThePot_Tick(object sender, EventArgs e)
        {
            timer_animationWhoTakesThePot.Stop();

            if (playerWhoPlay == 0)
            {
                lb_yourName.BackColor = Color.Olive;
            }
            else if (playerWhoPlay == 1)
            {
                lb_opponentRightName.BackColor = Color.Olive;
            }
            else if (playerWhoPlay == 2)
            {
                lb_yourTeammateName.BackColor = Color.Olive;
            }
            else if (playerWhoPlay == 3)
            {
                lb_opponentLeftName.BackColor = Color.Olive;
            }

            //nastavlja se dalje igrati
            erase4MiddleCards();
            PlayCards();


        }

        public void youArePlaying()
        {
            youPlay = true;
            lb_naPotezuSi.Visible = true;

            timer_youPlayCards.Start(); 

        }
        public void rightPlayerIsPlaying()
        {
            //MessageBox.Show("Desni igra");
            int index  = -1;
            bool deckoAdut = false, devetkaAdut = false, kecAdut = false, desetkaAdut = false, kraljAdut = false, damaAdut = false, osmicaAdut = false, sedmicaAdut = false;
            int indDecko = -1, indDevetka = -1, indKec = -1, indDesetka = -1, indKralj = -1, indDama = -1, indOsmica = -1, indSedmica = -1;
            bool KecZir = false, kecList = false, kecSrce = false, kecBundeva = false;
            int indKecZir = -1, indKecList = -1, indKecSrce = -1, indKecBundeva = -1;
            int maxValue = -1, minValue = 100;
            int indMax = -1, indMin = -1;
            bool haveThatColor = false;
            int maxPlayedValue = -1;
            int maxTempValue = -1, minTempValue = 100;
            int indTempMax = -1, indTempMin = -1;


            int leftAduts = 0;

            for(int i = 0; i < 8; i++)
            {

                if (RightPlayerCard[i].getColor() == calledColor && RightPlayerCard[i].getPlayed() == false)
                {
                    leftAduts++;

                    if (RightPlayerCard[i].getName() == "Decko")
                    {
                        deckoAdut = true;
                        indDecko = i;
                    }
                    else if (RightPlayerCard[i].getName() == "9")
                    {
                        devetkaAdut = true;
                        indDevetka = i;
                    }
                    else if (RightPlayerCard[i].getName() == "Kec")
                    {
                        kecAdut = true;
                        indKec = i;
                    }
                    else if (RightPlayerCard[i].getName() == "10")
                    {
                        desetkaAdut = true;
                        indDesetka = i;
                    }
                    else if (RightPlayerCard[i].getName() == "Kralj")
                    {
                        kraljAdut = true;
                        indKralj = i;
                    }
                    else if (RightPlayerCard[i].getName() == "Dama")
                    {
                        damaAdut = true;
                        indDama = i;
                    }
                    else if (RightPlayerCard[i].getName() == "8")
                    {
                        osmicaAdut = true;
                        indOsmica = i;
                    }
                    else if (RightPlayerCard[i].getName() == "7")
                    {
                        sedmicaAdut = true;
                        indSedmica = i;
                    }

                }
                else
                {
                    if(RightPlayerCard[i].getName() == "Kec" && RightPlayerCard[i].getPlayed() == false)
                    {
                        if(RightPlayerCard[i].getColor() == "Zir")
                        {
                            indKecZir = i;
                            KecZir = true;
                        }
                        else if (RightPlayerCard[i].getColor() == "List")
                        {
                            indKecList = i;
                            kecList = true;
                        }
                        else if (RightPlayerCard[i].getColor() == "Srce")
                        {
                            indKecSrce = i;
                            kecSrce = true;
                        }
                        else if (RightPlayerCard[i].getColor() == "Bundeva")
                        {
                            indKecBundeva = i;
                            kecBundeva = true;
                        }
                    }
                }
               

            }
            //on prvi igra te je zvao
            if(playerWhoPlay == 1 && numOfCardsPlayed == 0)
            {
                if (leftAduts > 1 && deckoAdut)
                {
                    index = indDecko;
                }
                else if (leftAduts > 1 && (osmicaAdut || sedmicaAdut))
                {
                    if (osmicaAdut) index = indOsmica;
                    else if (sedmicaAdut) index = indSedmica;
                }
                else if (KecZir || kecList || kecSrce || kecBundeva)
                {
                    if (KecZir) index = indKecZir;
                    else if (kecList) index = indKecList;
                    else if (kecSrce) index = indKecSrce;
                    else if (kecBundeva) index = indKecBundeva;
                }
                else
                {
                    index = getRandomNumber(0, 8);
                }
            }
            else 
            {
//******************************************************************************************************************************
                //on prvi igra ali nije zvao
                if (playerWhoPlay == 1)
                {
                    if (leftAduts > 2)
                    {
                        if (deckoAdut) index = indDecko;
                        else if (kraljAdut) index = indKralj;
                        else if (damaAdut) index = indDama;
                        else if (sedmicaAdut) index = indSedmica;
                        else if (osmicaAdut) index = indOsmica;
                    }
                    else if (KecZir || kecList || kecBundeva)
                    {
                        if (KecZir) index = indKecZir;
                        else if (kecList) index = indKecList;
                        else if (kecSrce) index = indKecSrce;
                        else if (kecBundeva) index = indKecBundeva;
                    }
                    else
                    {
                        do
                        {
                            index = getRandomNumber(0, 8);
                        } while (RightPlayerCard[index].getPlayed() == true);
                    }

                }
//************************************************************************************************************************
                //prosli igra prvi
                else if (playerWhoPlay == 0)
                {


                    //prosli je odigrao aduta
                    if (mainPlayedCard.getColor() == calledColor)
                    {



                        for (int i = 0; i < 8; i++)
                        {
                            if (RightPlayerCard[i].getColor() == calledColor && RightPlayerCard[i].getValue() > maxValue && RightPlayerCard[i].getPlayed() == false)
                            {
                                maxValue = RightPlayerCard[i].getValue();
                                indMax = i;
                            }
                            if (RightPlayerCard[i].getColor() == calledColor && RightPlayerCard[i].getValue() < minValue && RightPlayerCard[i].getPlayed() == false)
                            {
                                minValue = RightPlayerCard[i].getValue();
                                indMin = i;
                            }

                        }
                        if (leftAduts == 0)
                        {
                            for(int i = 0; i < 8; i++)
                            {
                                if(RightPlayerCard[i].getValue() < minTempValue && RightPlayerCard[i].getPlayed() == false)
                                {
                                    minTempValue = RightPlayerCard[i].getValue();
                                    index = i;
                                }
                            }

                        }
                        else if (mainPlayedCard.getValue() < maxValue) index = indMax;
                        else index = indMin;

                    }
//#########################################################
                    //prosli igra, prosli je odigro ne aduta
                    else if (mainPlayedCard.getColor() != calledColor)
                    {
                            
                        for (int i = 0; i < 8; i++)
                        {
                            if (RightPlayerCard[i].getColor() != calledColor && mainPlayedCard.getColor() == RightPlayerCard[i].getColor() && RightPlayerCard[i].getPlayed() == false && RightPlayerCard[i].getValue() > maxValue)
                            {
                                maxValue = RightPlayerCard[i].getValue();
                                indMax = i;
                                haveThatColor = true;
                            }
                            if (RightPlayerCard[i].getColor() != calledColor && mainPlayedCard.getColor() == RightPlayerCard[i].getColor() && RightPlayerCard[i].getPlayed() == false && RightPlayerCard[i].getValue() < minValue)
                            {
                                minValue = RightPlayerCard[i].getValue();
                                indMin = i;
                                haveThatColor = true;
                            }

                        }
                            
                        if (haveThatColor == false)
                        {
                            if(leftAduts == 0)
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (RightPlayerCard[i].getValue() < minTempValue && RightPlayerCard[i].getPlayed() == false)
                                    {
                                        minTempValue = RightPlayerCard[i].getValue();
                                        index = i;
                                    }
                                }
                            }
                            else
                            {
                                for(int i = 0; i < 8; i++)
                                {
                                    if(RightPlayerCard[i].getColor() == calledColor && minTempValue > RightPlayerCard[i].getValue() && RightPlayerCard[i].getPlayed() == false)
                                    {
                                        minTempValue = RightPlayerCard[i].getValue();
                                        index = i;
                                    }
                                }
                            }
                        }
                        else if (mainPlayedCard.getValue() < maxValue) index = indMax;
                        else index = indMin;

                        
                    }




                }
//*****************************************************************************************************************************
                //prvi igra pret-prosli igrac
                else if (playerWhoPlay == 3)
                {


                    //pret-prosli je odigrao aduta
                    if (mainPlayedCard.getColor() == calledColor)
                    {



                        for (int i = 0; i < 8; i++)
                        {
                            if (RightPlayerCard[i].getColor() == calledColor && RightPlayerCard[i].getValue() > maxValue && RightPlayerCard[i].getPlayed() == false)
                            {
                                maxValue = RightPlayerCard[i].getValue();
                                indMax = i;
                            }
                            if (RightPlayerCard[i].getColor() == calledColor && RightPlayerCard[i].getValue() < minValue && RightPlayerCard[i].getPlayed() == false)
                            {
                                minValue = RightPlayerCard[i].getValue();
                                indMin = i;
                            }

                        }
                        //pret-prosli zvao, obojca igraju aduta
                        if(mainPlayedCard.getColor() == calledColor && MyPlayedCard.getColor() == calledColor)
                        {
                            if (mainPlayedCard.getValue() > MyPlayedCard.getValue()) maxPlayedValue = mainPlayedCard.getValue();
                            else maxPlayedValue = MyPlayedCard.getValue();
                        }
                        //donji ne igra aduta
                        else
                        {
                            maxPlayedValue = mainPlayedCard.getValue();
                        }
                       


                        if (leftAduts == 0)
                        {
                            for (int i = 0; i < 8; i++)
                            {
                                if (RightPlayerCard[i].getValue() < minTempValue && RightPlayerCard[i].getPlayed() == false)
                                {
                                    minTempValue = RightPlayerCard[i].getValue();
                                    index = i;
                                }
                            }
                        }
                        else if (maxPlayedValue < maxValue) index = indMax;
                        else index = indMin;

                    }
//####################################################
                    //pret-prosli je odigro ne aduta
                    else if (mainPlayedCard.getColor() != calledColor)
                    {

                        for (int i = 0; i < 8; i++)
                        {
                            if (RightPlayerCard[i].getColor() != calledColor && mainPlayedCard.getColor() == RightPlayerCard[i].getColor() && RightPlayerCard[i].getValue() > maxValue && RightPlayerCard[i].getPlayed() == false)
                            {
                                maxValue = RightPlayerCard[i].getValue();
                                indMax = i;
                                haveThatColor = true;
                            }
                            if (RightPlayerCard[i].getColor() != calledColor && mainPlayedCard.getColor() == RightPlayerCard[i].getColor() && RightPlayerCard[i].getValue() < minValue && RightPlayerCard[i].getPlayed() == false)
                            {
                                minValue = RightPlayerCard[i].getValue();
                                indMin = i;
                                haveThatColor = true;
                            }

                        }
                        //obojca su odigrala ne adute
                        if (mainPlayedCard.getColor() != calledColor && MyPlayedCard.getColor() != calledColor)
                        {
                            if (mainPlayedCard.getValue() > MyPlayedCard.getValue()) maxPlayedValue = mainPlayedCard.getValue();
                            else maxPlayedValue = MyPlayedCard.getValue();

                            if (haveThatColor)
                            {
                                if (maxValue > maxPlayedValue) index = indMax;
                                else index = indMin;
                            }
                            else if (leftAduts > 0)
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (RightPlayerCard[i].getColor() == calledColor && RightPlayerCard[i].getValue() > maxTempValue && RightPlayerCard[i].getPlayed() == false)
                                    {
                                        maxTempValue = RightPlayerCard[i].getValue();
                                        indTempMax = i;
                                    }

                                }
                                index = indTempMax;
                            }
                            else
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (RightPlayerCard[i].getValue() < minTempValue && RightPlayerCard[i].getPlayed() == false)
                                    {
                                        minTempValue = RightPlayerCard[i].getValue();
                                        index = i;
                                    }
                                }
                            }

                        }
                        //prosli je odigrao aduta
                        else
                        {
                            if(haveThatColor) index = indMin;
                            else if(leftAduts>0)
                            {
                                for(int i = 0; i < 8; i++)
                                {
                                    if(RightPlayerCard[i].getColor() == calledColor && RightPlayerCard[i].getValue() > maxTempValue && RightPlayerCard[i].getPlayed() == false)
                                    {
                                        maxTempValue = RightPlayerCard[i].getValue();
                                        indTempMax = i;
                                    }
                                    if (RightPlayerCard[i].getColor() == calledColor && RightPlayerCard[i].getValue() < minTempValue && RightPlayerCard[i].getPlayed() == false)
                                    {
                                        minTempValue = RightPlayerCard[i].getValue();
                                        indTempMin = i;
                                    }
                                }
                                if (maxTempValue > MyPlayedCard.getValue()) index = indTempMax;
                                else index = indTempMin;
                            }
                            else
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (RightPlayerCard[i].getValue() < minTempValue && RightPlayerCard[i].getPlayed() == false)
                                    {
                                        minTempValue = RightPlayerCard[i].getValue();
                                        index = i;
                                    }
                                }
                            }

                        }

                        
                    }

                }
//*********************************************************************************************************************
                //prvi igra pret-pret-prosli igrac
                else if(playerWhoPlay == 2)
                {


                    //pret-pret-prosli je odigrao aduta
                    if (mainPlayedCard.getColor() == calledColor)
                    {



                        for (int i = 0; i < 8; i++)
                        {
                            if (RightPlayerCard[i].getColor() == calledColor && RightPlayerCard[i].getValue() > maxValue && RightPlayerCard[i].getPlayed() == false)
                            {
                                maxValue = RightPlayerCard[i].getValue();
                                indMax = i;
                            }
                            if (RightPlayerCard[i].getColor() == calledColor && RightPlayerCard[i].getValue() < minValue && RightPlayerCard[i].getPlayed() == false)
                            {
                                minValue = RightPlayerCard[i].getValue();
                                indMin = i;
                            }

                        }
                        //svi igraju adute
                        if (mainPlayedCard.getColor() == calledColor && LeftPlayerPlayedCard.getColor() == calledColor && MyPlayedCard.getColor() == calledColor)
                        {
                            if ((mainPlayedCard.getValue() > LeftPlayerPlayedCard.getValue()) && (mainPlayedCard.getValue() > MyPlayedCard.getValue())) maxPlayedValue = mainPlayedCard.getValue();
                            else if ((LeftPlayerPlayedCard.getValue() > mainPlayedCard.getValue()) && (LeftPlayerPlayedCard.getValue() > MyPlayedCard.getValue())) maxPlayedValue = LeftPlayerPlayedCard.getValue();
                            else maxPlayedValue = MyPlayedCard.getValue();

                            if (leftAduts == 0)
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (RightPlayerCard[i].getValue() < minTempValue && RightPlayerCard[i].getPlayed() == false)
                                    {
                                        minTempValue = RightPlayerCard[i].getValue();
                                        index = i;
                                    }
                                }
                            }
                            else if (maxPlayedValue < maxValue) index = indMax;
                            else index = indMin;
                            

                       
                        }
                        //lijevi ne odigra aduta
                        else if(mainPlayedCard.getColor() == calledColor && LeftPlayerPlayedCard.getColor() != calledColor && MyPlayedCard.getColor() == calledColor)
                        {
                            if (mainPlayedCard.getValue() > MyPlayedCard.getValue()) maxPlayedValue = mainPlayedCard.getValue();                           
                            else maxPlayedValue = MyPlayedCard.getValue();

                            if (leftAduts == 0)
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (RightPlayerCard[i].getValue() < minTempValue && RightPlayerCard[i].getPlayed() == false)
                                    {
                                        minTempValue = RightPlayerCard[i].getValue();
                                        index = i;
                                    }
                                }
                            }
                            else if (maxPlayedValue < maxValue) index = indMax;
                            else index = indMin;
                        }
                        //donji ne odigra aduta
                        else if (mainPlayedCard.getColor() == calledColor && LeftPlayerPlayedCard.getColor() == calledColor && MyPlayedCard.getColor() != calledColor)
                        {
                            if (mainPlayedCard.getValue() > LeftPlayerPlayedCard.getValue()) maxPlayedValue = mainPlayedCard.getValue();
                            else maxPlayedValue = LeftPlayerPlayedCard.getValue();

                            if (leftAduts == 0)
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (RightPlayerCard[i].getValue() < minTempValue && RightPlayerCard[i].getPlayed() == false)
                                    {
                                        minTempValue = RightPlayerCard[i].getValue();
                                        index = i;
                                    }
                                }
                            }
                            else if (maxPlayedValue < maxValue) index = indMax;
                            else index = indMin;
                        }
                        //lijevi i donji ne odigraju aduta
                        else
                        {
                            maxPlayedValue = mainPlayedCard.getValue();
                            if (leftAduts == 0)
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (RightPlayerCard[i].getValue() < minTempValue && RightPlayerCard[i].getPlayed() == false)
                                    {
                                        minTempValue = RightPlayerCard[i].getValue();
                                        index = i;
                                    }
                                }
                            }
                            else if (maxPlayedValue < maxValue) index = indMax;
                            else index = indMin;

                        }


                    }
//###########################################
                    //pret-pret-prosli odigra ne aduta
                    else
                    {
                        if (mainPlayedCard.getColor() != calledColor)
                        {
                            //max i min moja karta u toj boji
                            for (int i = 0; i < 8; i++)
                            {
                                if (RightPlayerCard[i].getColor() != calledColor && mainPlayedCard.getColor() == RightPlayerCard[i].getColor() && RightPlayerCard[i].getValue() > maxValue && RightPlayerCard[i].getPlayed() == false)
                                {
                                    maxValue = RightPlayerCard[i].getValue();
                                    indMax = i;
                                    haveThatColor = true;
                                }
                                if (RightPlayerCard[i].getColor() != calledColor && mainPlayedCard.getColor() == RightPlayerCard[i].getColor() && RightPlayerCard[i].getValue() < minValue && RightPlayerCard[i].getPlayed() == false)
                                {
                                    minValue = RightPlayerCard[i].getValue();
                                    indMin = i;
                                    haveThatColor = true;
                                }

                            }
                            //sva trojca su odigrala ne adute
                            if (mainPlayedCard.getColor() != calledColor && LeftPlayerPlayedCard.getColor()!=calledColor && MyPlayedCard.getColor() != calledColor)
                            {
                                if ((mainPlayedCard.getValue() > LeftPlayerPlayedCard.getValue()) && (mainPlayedCard.getValue() > MyPlayedCard.getValue())) maxPlayedValue = mainPlayedCard.getValue();
                                else if ((LeftPlayerPlayedCard.getValue() > mainPlayedCard.getValue()) && (LeftPlayerPlayedCard.getValue() > MyPlayedCard.getValue())) maxPlayedValue = LeftPlayerPlayedCard.getValue();
                                else maxPlayedValue = MyPlayedCard.getValue();

                                if (haveThatColor) {
                                    if (maxValue > maxPlayedValue) index = indMax;
                                    else index = indMin;
                                } 
                                else if (leftAduts > 0)
                                {
                                    for (int i = 0; i < 8; i++)
                                    {
                                        if (RightPlayerCard[i].getColor() == calledColor && RightPlayerCard[i].getValue() > maxTempValue && RightPlayerCard[i].getPlayed() == false)
                                        {
                                            maxTempValue = RightPlayerCard[i].getValue();
                                            indTempMax = i;
                                        }
                                    }
                                    index = indTempMax;
                                }
                                else
                                {
                                    for (int i = 0; i < 8; i++)
                                    {
                                        if (RightPlayerCard[i].getValue() < minTempValue && RightPlayerCard[i].getPlayed() == false)
                                        {
                                            minTempValue = RightPlayerCard[i].getValue();
                                            index = i;
                                        }
                                    }
                                }

                            }
                            //prosli je odigrao aduta, pret-prosli ne aduta
                            else if(LeftPlayerPlayedCard.getColor() != calledColor && MyPlayedCard.getColor() == calledColor)
                            {
                                if (haveThatColor) index = indMin;
                                else if (leftAduts > 0)
                                {
                                    for (int i = 0; i < 8; i++)
                                    {
                                        if (RightPlayerCard[i].getColor() == calledColor && RightPlayerCard[i].getValue() > maxTempValue && RightPlayerCard[i].getPlayed() == false)
                                        {
                                            maxTempValue = RightPlayerCard[i].getValue();
                                            indTempMax = i;
                                        }
                                        if (RightPlayerCard[i].getColor() == calledColor && RightPlayerCard[i].getValue() < minTempValue && RightPlayerCard[i].getPlayed() == false)
                                        {
                                            minTempValue = RightPlayerCard[i].getValue();
                                            indTempMin = i;
                                        }
                                    }
                                    if (maxTempValue > MyPlayedCard.getValue()) index = indTempMax;
                                    else index = indTempMin;
                                }
                                else
                                {
                                    for (int i = 0; i < 8; i++)
                                    {
                                        if (RightPlayerCard[i].getValue() < minTempValue && RightPlayerCard[i].getPlayed() == false)
                                        {
                                            minTempValue = RightPlayerCard[i].getValue();
                                            index = i;
                                        }
                                    }
                                }


                            }
                            //prosli je odigrao ne aduta, pret-prosli aduta
                            else if (LeftPlayerPlayedCard.getColor() == calledColor && MyPlayedCard.getColor() != calledColor)
                            {
                                if (haveThatColor) index = indMin;
                                else if (leftAduts > 0)
                                {
                                    for (int i = 0; i < 8; i++)
                                    {
                                        if (RightPlayerCard[i].getColor() == calledColor && RightPlayerCard[i].getValue() > maxTempValue && RightPlayerCard[i].getPlayed() == false)
                                        {
                                            maxTempValue = RightPlayerCard[i].getValue();
                                            indTempMax = i;
                                        }
                                        if (RightPlayerCard[i].getColor() == calledColor && RightPlayerCard[i].getValue() < minTempValue && RightPlayerCard[i].getPlayed() == false)
                                        {
                                            minTempValue = RightPlayerCard[i].getValue();
                                            indTempMin = i;
                                        }

                                    }
                                    if (maxTempValue > LeftPlayerPlayedCard.getValue()) index = indTempMax;
                                    else index = indTempMin;

                                }
                                else
                                {
                                    for (int i = 0; i < 8; i++)
                                    {
                                        if (RightPlayerCard[i].getValue() < minTempValue && RightPlayerCard[i].getPlayed() == false)
                                        {
                                            minTempValue = RightPlayerCard[i].getValue();
                                            index = i;
                                        }
                                    }
                                }


                            }
                            //prosli i pret-prosli su odigrali adute
                            else
                            {
                                if (haveThatColor) index = indMin;
                                else if (leftAduts > 0)
                                {
                                    for (int i = 0; i < 8; i++)
                                    {
                                        if (RightPlayerCard[i].getColor() == calledColor && RightPlayerCard[i].getValue() > maxTempValue && RightPlayerCard[i].getPlayed() == false)
                                        {
                                            maxTempValue = RightPlayerCard[i].getValue();
                                            indTempMax = i;
                                        }
                                        if (RightPlayerCard[i].getColor() == calledColor && RightPlayerCard[i].getValue() < minTempValue && RightPlayerCard[i].getPlayed() == false)
                                        {
                                            minTempValue = RightPlayerCard[i].getValue();
                                            indTempMin = i;
                                        }
                                    }
                                    if (maxTempValue>LeftPlayerPlayedCard.getValue() && maxTempValue > MyPlayedCard.getValue()) index = indTempMax;
                                    else index = indTempMin;
                                }
                                else
                                {
                                    for (int i = 0; i < 8; i++)
                                    {
                                        if (RightPlayerCard[i].getValue() < minTempValue && RightPlayerCard[i].getPlayed() == false)
                                        {
                                            minTempValue = RightPlayerCard[i].getValue();
                                            index = i;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            //
            }



            RightPlayerPlayedCard.setColor(RightPlayerCard[index].getColor());
            RightPlayerPlayedCard.setValue(RightPlayerCard[index].getValue());
            RightPlayerPlayedCard.setID(RightPlayerCard[index].getID());
            RightPlayerPlayedCard.setPlayableID(RightPlayerCard[index].getPlayableID());
            RightPlayerPlayedCard.setName(RightPlayerCard[index].getName());
            RightPlayerCard[index].setPlayed(true);
            findRightPlayedCard();
            //printTheirCards(8);

            Player_cardDrop.Play();

        }
        public void upperPlayerIsPlaying()
        {

            //MessageBox.Show("Gornji igra");
            int index = -1;
            bool deckoAdut = false, devetkaAdut = false, kecAdut = false, desetkaAdut = false, kraljAdut = false, damaAdut = false, osmicaAdut = false, sedmicaAdut = false;
            int indDecko = -1, indDevetka = -1, indKec = -1, indDesetka = -1, indKralj = -1, indDama = -1, indOsmica = -1, indSedmica = -1;
            bool KecZir = false, kecList = false, kecSrce = false, kecBundeva = false;
            int indKecZir = -1, indKecList = -1, indKecSrce = -1, indKecBundeva = -1;
            int maxValue = -1, minValue = 100;
            int indMax = -1, indMin = -1;
            bool haveThatColor = false;
            int maxPlayedValue = -1;
            int maxTempValue = -1, minTempValue = 100;
            int indTempMax = -1, indTempMin = -1;


            int leftAduts = 0;

            for (int i = 0; i < 8; i++)
            {

                if (UpperPlayerCard[i].getColor() == calledColor && UpperPlayerCard[i].getPlayed() == false)
                {
                    leftAduts++;

                    if (UpperPlayerCard[i].getName() == "Decko")
                    {
                        deckoAdut = true;
                        indDecko = i;
                    }
                    else if (UpperPlayerCard[i].getName() == "9")
                    {
                        devetkaAdut = true;
                        indDevetka = i;
                    }
                    else if (UpperPlayerCard[i].getName() == "Kec")
                    {
                        kecAdut = true;
                        indKec = i;
                    }
                    else if (UpperPlayerCard[i].getName() == "10")
                    {
                        desetkaAdut = true;
                        indDesetka = i;
                    }
                    else if (UpperPlayerCard[i].getName() == "Kralj")
                    {
                        kraljAdut = true;
                        indKralj = i;
                    }
                    else if (UpperPlayerCard[i].getName() == "Dama")
                    {
                        damaAdut = true;
                        indDama = i;
                    }
                    else if (UpperPlayerCard[i].getName() == "8")
                    {
                        osmicaAdut = true;
                        indOsmica = i;
                    }
                    else if (UpperPlayerCard[i].getName() == "7")
                    {
                        sedmicaAdut = true;
                        indSedmica = i;
                    }

                }
                else
                {
                    if (UpperPlayerCard[i].getName() == "Kec" && UpperPlayerCard[i].getPlayed() == false)
                    {
                        if (UpperPlayerCard[i].getColor() == "Zir")
                        {
                            indKecZir = i;
                            KecZir = true;
                        }
                        else if (UpperPlayerCard[i].getColor() == "List")
                        {
                            indKecList = i;
                            kecList = true;
                        }
                        else if (UpperPlayerCard[i].getColor() == "Srce")
                        {
                            indKecSrce = i;
                            kecSrce = true;
                        }
                        else if (UpperPlayerCard[i].getColor() == "Bundeva")
                        {
                            indKecBundeva = i;
                            kecBundeva = true;
                        }
                    }
                }


            }
            //on prvi igra te je zvao
            if (playerWhoPlay == 2 && numOfCardsPlayed == 0)
            {
                if (leftAduts > 1 && deckoAdut)
                {
                    index = indDecko;
                }
                else if (leftAduts > 1 && (osmicaAdut || sedmicaAdut))
                {
                    if (osmicaAdut) index = indOsmica;
                    else if (sedmicaAdut) index = indSedmica;
                }
                else if (KecZir || kecList || kecSrce || kecBundeva)
                {
                    if (KecZir) index = indKecZir;
                    else if (kecList) index = indKecList;
                    else if (kecSrce) index = indKecSrce;
                    else if (kecBundeva) index = indKecBundeva;
                }
                else
                {
                    index = getRandomNumber(0, 8);
                }
            }
            else
            {
                //******************************************************************************************************************************
                //on prvi igra ali nije zvao
                if (playerWhoPlay == 2)
                {
                    if (leftAduts > 2)
                    {
                        if (deckoAdut) index = indDecko;
                        else if (kraljAdut) index = indKralj;
                        else if (damaAdut) index = indDama;
                        else if (sedmicaAdut) index = indSedmica;
                        else if (osmicaAdut) index = indOsmica;
                    }
                    else if (KecZir || kecList || kecBundeva)
                    {
                        if (KecZir) index = indKecZir;
                        else if (kecList) index = indKecList;
                        else if (kecSrce) index = indKecSrce;
                        else if (kecBundeva) index = indKecBundeva;
                    }
                    else
                    {
                        do
                        {
                            index = getRandomNumber(0, 8);
                        } while (UpperPlayerCard[index].getPlayed() == true);
                    }

                }
                //************************************************************************************************************************
                //prosli igra prvi
                else if (playerWhoPlay == 1)
                {


                    //prosli je odigrao aduta
                    if (mainPlayedCard.getColor() == calledColor)
                    {



                        for (int i = 0; i < 8; i++)
                        {
                            if (UpperPlayerCard[i].getColor() == calledColor && UpperPlayerCard[i].getValue() > maxValue && UpperPlayerCard[i].getPlayed() == false)
                            {
                                maxValue = UpperPlayerCard[i].getValue();
                                indMax = i;
                            }
                            if (UpperPlayerCard[i].getColor() == calledColor && UpperPlayerCard[i].getValue() < minValue && UpperPlayerCard[i].getPlayed() == false)
                            {
                                minValue = UpperPlayerCard[i].getValue();
                                indMin = i;
                            }

                        }
                        if (leftAduts == 0)
                        {
                            for (int i = 0; i < 8; i++)
                            {
                                if (UpperPlayerCard[i].getValue() < minTempValue && UpperPlayerCard[i].getPlayed() == false)
                                {
                                    minTempValue = UpperPlayerCard[i].getValue();
                                    index = i;
                                }
                            }

                        }
                        else if (mainPlayedCard.getValue() < maxValue) index = indMax;
                        else index = indMin;

                    }
                    //#########################################################
                    //prosli igra, prosli je odigro ne aduta
                    else if (mainPlayedCard.getColor() != calledColor)
                    {

                        for (int i = 0; i < 8; i++)
                        {
                            if (UpperPlayerCard[i].getColor() != calledColor && mainPlayedCard.getColor() == UpperPlayerCard[i].getColor() && UpperPlayerCard[i].getPlayed() == false && UpperPlayerCard[i].getValue() > maxValue)
                            {
                                maxValue = UpperPlayerCard[i].getValue();
                                indMax = i;
                                haveThatColor = true;
                            }
                            if (UpperPlayerCard[i].getColor() != calledColor && mainPlayedCard.getColor() == UpperPlayerCard[i].getColor() && UpperPlayerCard[i].getPlayed() == false && UpperPlayerCard[i].getValue() < minValue)
                            {
                                minValue = UpperPlayerCard[i].getValue();
                                indMin = i;
                                haveThatColor = true;
                            }

                        }

                        if (haveThatColor == false)
                        {
                            if (leftAduts == 0)
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (UpperPlayerCard[i].getValue() < minTempValue && UpperPlayerCard[i].getPlayed() == false)
                                    {
                                        minTempValue = UpperPlayerCard[i].getValue();
                                        index = i;
                                    }
                                }
                            }
                            else
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (UpperPlayerCard[i].getColor() == calledColor && minTempValue > UpperPlayerCard[i].getValue() && UpperPlayerCard[i].getPlayed() == false)
                                    {
                                        minTempValue = UpperPlayerCard[i].getValue();
                                        index = i;
                                    }
                                }
                            }
                        }
                        else if (mainPlayedCard.getValue() < maxValue) index = indMax;
                        else index = indMin;


                    }




                }
                //*****************************************************************************************************************************
                //prvi igra pret-prosli igrac
                else if (playerWhoPlay == 0)
                {


                    //pret-prosli je odigrao aduta
                    if (mainPlayedCard.getColor() == calledColor)
                    {



                        for (int i = 0; i < 8; i++)
                        {
                            if (UpperPlayerCard[i].getColor() == calledColor && UpperPlayerCard[i].getValue() > maxValue && UpperPlayerCard[i].getPlayed() == false)
                            {
                                maxValue = UpperPlayerCard[i].getValue();
                                indMax = i;
                            }
                            if (UpperPlayerCard[i].getColor() == calledColor && UpperPlayerCard[i].getValue() < minValue && UpperPlayerCard[i].getPlayed() == false)
                            {
                                minValue = UpperPlayerCard[i].getValue();
                                indMin = i;
                            }

                        }
                        //pret-prosli zvao, obojca igraju aduta
                        if (mainPlayedCard.getColor() == calledColor && RightPlayerPlayedCard.getColor() == calledColor)
                        {
                            if (mainPlayedCard.getValue() > RightPlayerPlayedCard.getValue()) maxPlayedValue = mainPlayedCard.getValue();
                            else maxPlayedValue = RightPlayerPlayedCard.getValue();
                        }
                        //donji ne igra aduta
                        else
                        {
                            maxPlayedValue = mainPlayedCard.getValue();
                        }



                        if (leftAduts == 0)
                        {
                            for (int i = 0; i < 8; i++)
                            {
                                if (UpperPlayerCard[i].getValue() < minTempValue && UpperPlayerCard[i].getPlayed() == false)
                                {
                                    minTempValue = UpperPlayerCard[i].getValue();
                                    index = i;
                                }
                            }
                        }
                        else if (maxPlayedValue < maxValue) index = indMax;
                        else index = indMin;

                    }
                    //####################################################
                    //pret-prosli je odigro ne aduta
                    else if (mainPlayedCard.getColor() != calledColor)
                    {

                        for (int i = 0; i < 8; i++)
                        {
                            if (UpperPlayerCard[i].getColor() != calledColor && mainPlayedCard.getColor() == UpperPlayerCard[i].getColor() && UpperPlayerCard[i].getValue() > maxValue && UpperPlayerCard[i].getPlayed() == false)
                            {
                                maxValue = UpperPlayerCard[i].getValue();
                                indMax = i;
                                haveThatColor = true;
                            }
                            if (UpperPlayerCard[i].getColor() != calledColor && mainPlayedCard.getColor() == UpperPlayerCard[i].getColor() && UpperPlayerCard[i].getValue() < minValue && UpperPlayerCard[i].getPlayed() == false)
                            {
                                minValue = UpperPlayerCard[i].getValue();
                                indMin = i;
                                haveThatColor = true;
                            }

                        }
                        //obojca su odigrala ne adute
                        if (mainPlayedCard.getColor() != calledColor && RightPlayerPlayedCard.getColor() != calledColor)
                        {
                            if (mainPlayedCard.getValue() > RightPlayerPlayedCard.getValue()) maxPlayedValue = mainPlayedCard.getValue();
                            else maxPlayedValue = RightPlayerPlayedCard.getValue();

                            if (haveThatColor)
                            {
                                if (maxValue > maxPlayedValue) index = indMax;
                                else index = indMin;
                            }
                            else if (leftAduts > 0)
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (UpperPlayerCard[i].getColor() == calledColor && UpperPlayerCard[i].getValue() > maxTempValue && UpperPlayerCard[i].getPlayed() == false)
                                    {
                                        maxTempValue = UpperPlayerCard[i].getValue();
                                        indTempMax = i;
                                    }

                                }
                                index = indTempMax;
                            }
                            else
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (UpperPlayerCard[i].getValue() < minTempValue && UpperPlayerCard[i].getPlayed() == false)
                                    {
                                        minTempValue = UpperPlayerCard[i].getValue();
                                        index = i;
                                    }
                                }
                            }

                        }
                        //prosli je odigrao aduta
                        else
                        {
                            if (haveThatColor) index = indMin;
                            else if (leftAduts > 0)
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (UpperPlayerCard[i].getColor() == calledColor && UpperPlayerCard[i].getValue() > maxTempValue && UpperPlayerCard[i].getPlayed() == false)
                                    {
                                        maxTempValue = UpperPlayerCard[i].getValue();
                                        indTempMax = i;
                                    }
                                    if (UpperPlayerCard[i].getColor() == calledColor && UpperPlayerCard[i].getValue() < minTempValue && UpperPlayerCard[i].getPlayed() == false)
                                    {
                                        minTempValue = UpperPlayerCard[i].getValue();
                                        indTempMin = i;
                                    }
                                }
                                if (maxTempValue > RightPlayerPlayedCard.getValue()) index = indTempMax;
                                else index = indTempMin;
                            }
                            else
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (UpperPlayerCard[i].getValue() < minTempValue && UpperPlayerCard[i].getPlayed() == false)
                                    {
                                        minTempValue = UpperPlayerCard[i].getValue();
                                        index = i;
                                    }
                                }
                            }

                        }


                    }

                }
                //*********************************************************************************************************************
                //prvi igra pret-pret-prosli igrac
                else if (playerWhoPlay == 3)
                {


                    //pret-pret-prosli je odigrao aduta
                    if (mainPlayedCard.getColor() == calledColor)
                    {



                        for (int i = 0; i < 8; i++)
                        {
                            if (UpperPlayerCard[i].getColor() == calledColor && UpperPlayerCard[i].getValue() > maxValue && UpperPlayerCard[i].getPlayed() == false)
                            {
                                maxValue = UpperPlayerCard[i].getValue();
                                indMax = i;
                            }
                            if (UpperPlayerCard[i].getColor() == calledColor && UpperPlayerCard[i].getValue() < minValue && UpperPlayerCard[i].getPlayed() == false)
                            {
                                minValue = UpperPlayerCard[i].getValue();
                                indMin = i;
                            }

                        }
                        //svi igraju adute
                        if (mainPlayedCard.getColor() == calledColor && MyPlayedCard.getColor() == calledColor && RightPlayerPlayedCard.getColor() == calledColor)
                        {
                            if ((mainPlayedCard.getValue() > MyPlayedCard.getValue()) && (mainPlayedCard.getValue() > RightPlayerPlayedCard.getValue())) maxPlayedValue = mainPlayedCard.getValue();
                            else if ((MyPlayedCard.getValue() > mainPlayedCard.getValue()) && (MyPlayedCard.getValue() > RightPlayerPlayedCard.getValue())) maxPlayedValue = MyPlayedCard.getValue();
                            else maxPlayedValue = RightPlayerPlayedCard.getValue();

                            if (leftAduts == 0)
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (UpperPlayerCard[i].getValue() < minTempValue && UpperPlayerCard[i].getPlayed() == false)
                                    {
                                        minTempValue = UpperPlayerCard[i].getValue();
                                        index = i;
                                    }
                                }
                            }
                            else if (maxPlayedValue < maxValue) index = indMax;
                            else index = indMin;



                        }
                        //pret-prosli ne odigra aduta
                        else if (mainPlayedCard.getColor() == calledColor && MyPlayedCard.getColor() != calledColor && RightPlayerPlayedCard.getColor() == calledColor)
                        {
                            if (mainPlayedCard.getValue() > RightPlayerPlayedCard.getValue()) maxPlayedValue = mainPlayedCard.getValue();
                            else maxPlayedValue = RightPlayerPlayedCard.getValue();

                            if (leftAduts == 0)
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (UpperPlayerCard[i].getValue() < minTempValue && UpperPlayerCard[i].getPlayed() == false)
                                    {
                                        minTempValue = UpperPlayerCard[i].getValue();
                                        index = i;
                                    }
                                }
                            }
                            else if (maxPlayedValue < maxValue) index = indMax;
                            else index = indMin;
                        }
                        //prosli ne odigra aduta
                        else if (mainPlayedCard.getColor() == calledColor && MyPlayedCard.getColor() == calledColor && RightPlayerPlayedCard.getColor() != calledColor)
                        {
                            if (mainPlayedCard.getValue() > MyPlayedCard.getValue()) maxPlayedValue = mainPlayedCard.getValue();
                            else maxPlayedValue = MyPlayedCard.getValue();

                            if (leftAduts == 0)
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (UpperPlayerCard[i].getValue() < minTempValue && UpperPlayerCard[i].getPlayed() == false)
                                    {
                                        minTempValue = UpperPlayerCard[i].getValue();
                                        index = i;
                                    }
                                }
                            }
                            else if (maxPlayedValue < maxValue) index = indMax;
                            else index = indMin;
                        }
                        //lijevi i donji ne odigraju aduta
                        else
                        {
                            maxPlayedValue = mainPlayedCard.getValue();
                            if (leftAduts == 0)
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (UpperPlayerCard[i].getValue() < minTempValue && UpperPlayerCard[i].getPlayed() == false)
                                    {
                                        minTempValue = UpperPlayerCard[i].getValue();
                                        index = i;
                                    }
                                }
                            }
                            else if (maxPlayedValue < maxValue) index = indMax;
                            else index = indMin;

                        }


                    }
                    //###########################################
                    //pret-pret-prosli odigra ne aduta
                    else
                    {
                        if (mainPlayedCard.getColor() != calledColor)
                        {
                            //max i min moja karta u toj boji
                            for (int i = 0; i < 8; i++)
                            {
                                if (UpperPlayerCard[i].getColor() != calledColor && mainPlayedCard.getColor() == UpperPlayerCard[i].getColor() && UpperPlayerCard[i].getValue() > maxValue && UpperPlayerCard[i].getPlayed() == false)
                                {
                                    maxValue = UpperPlayerCard[i].getValue();
                                    indMax = i;
                                    haveThatColor = true;
                                }
                                if (UpperPlayerCard[i].getColor() != calledColor && mainPlayedCard.getColor() == UpperPlayerCard[i].getColor() && UpperPlayerCard[i].getValue() < minValue && UpperPlayerCard[i].getPlayed() == false)
                                {
                                    minValue = UpperPlayerCard[i].getValue();
                                    indMin = i;
                                    haveThatColor = true;
                                }

                            }
                            //sva trojca su odigrala ne adute
                            if (mainPlayedCard.getColor() != calledColor && MyPlayedCard.getColor() != calledColor && RightPlayerPlayedCard.getColor() != calledColor)
                            {
                                if ((mainPlayedCard.getValue() > MyPlayedCard.getValue()) && (mainPlayedCard.getValue() > RightPlayerPlayedCard.getValue())) maxPlayedValue = mainPlayedCard.getValue();
                                else if ((MyPlayedCard.getValue() > mainPlayedCard.getValue()) && (MyPlayedCard.getValue() > RightPlayerPlayedCard.getValue())) maxPlayedValue = MyPlayedCard.getValue();
                                else maxPlayedValue = RightPlayerPlayedCard.getValue();

                                if (haveThatColor)
                                {
                                    if (maxValue > maxPlayedValue) index = indMax;
                                    else index = indMin;
                                }
                                else if (leftAduts > 0)
                                {
                                    for (int i = 0; i < 8; i++)
                                    {
                                        if (UpperPlayerCard[i].getColor() == calledColor && UpperPlayerCard[i].getValue() > maxTempValue && UpperPlayerCard[i].getPlayed() == false)
                                        {
                                            maxTempValue = UpperPlayerCard[i].getValue();
                                            indTempMax = i;
                                        }
                                    }
                                    index = indTempMax;
                                }
                                else
                                {
                                    for (int i = 0; i < 8; i++)
                                    {
                                        if (UpperPlayerCard[i].getValue() < minTempValue && UpperPlayerCard[i].getPlayed() == false)
                                        {
                                            minTempValue = UpperPlayerCard[i].getValue();
                                            index = i;
                                        }
                                    }
                                }

                            }
                            //prosli je odigrao aduta, pret-prosli ne aduta
                            else if (RightPlayerPlayedCard.getColor() == calledColor && MyPlayedCard.getColor() != calledColor)
                            {
                                if (haveThatColor) index = indMin;
                                else if (leftAduts > 0)
                                {
                                    for (int i = 0; i < 8; i++)
                                    {
                                        if (UpperPlayerCard[i].getColor() == calledColor && UpperPlayerCard[i].getValue() > maxTempValue && UpperPlayerCard[i].getPlayed() == false)
                                        {
                                            maxTempValue = UpperPlayerCard[i].getValue();
                                            indTempMax = i;
                                        }
                                        if (UpperPlayerCard[i].getColor() == calledColor && UpperPlayerCard[i].getValue() < minTempValue && UpperPlayerCard[i].getPlayed() == false)
                                        {
                                            minTempValue = UpperPlayerCard[i].getValue();
                                            indTempMin = i;
                                        }
                                    }
                                    if (maxTempValue > RightPlayerPlayedCard.getValue()) index = indTempMax;
                                    else index = indTempMin;
                                }
                                else
                                {
                                    for (int i = 0; i < 8; i++)
                                    {
                                        if (UpperPlayerCard[i].getValue() < minTempValue && UpperPlayerCard[i].getPlayed() == false)
                                        {
                                            minTempValue = UpperPlayerCard[i].getValue();
                                            index = i;
                                        }
                                    }
                                }


                            }
                            //prosli je odigrao ne aduta, pret-prosli aduta
                            else if (RightPlayerPlayedCard.getColor() != calledColor && MyPlayedCard.getColor() == calledColor)
                            {
                                if (haveThatColor) index = indMin;
                                else if (leftAduts > 0)
                                {
                                    for (int i = 0; i < 8; i++)
                                    {
                                        if (UpperPlayerCard[i].getColor() == calledColor && UpperPlayerCard[i].getValue() > maxTempValue && UpperPlayerCard[i].getPlayed() == false)
                                        {
                                            maxTempValue = UpperPlayerCard[i].getValue();
                                            indTempMax = i;
                                        }
                                        if (UpperPlayerCard[i].getColor() == calledColor && UpperPlayerCard[i].getValue() < minTempValue && UpperPlayerCard[i].getPlayed() == false)
                                        {
                                            minTempValue = UpperPlayerCard[i].getValue();
                                            indTempMin = i;
                                        }

                                    }
                                    if (maxTempValue > MyPlayedCard.getValue()) index = indTempMax;
                                    else index = indTempMin;

                                }
                                else
                                {
                                    for (int i = 0; i < 8; i++)
                                    {
                                        if (UpperPlayerCard[i].getValue() < minTempValue && UpperPlayerCard[i].getPlayed() == false)
                                        {
                                            minTempValue = UpperPlayerCard[i].getValue();
                                            index = i;
                                        }
                                    }
                                }


                            }
                            //prosli i pret-prosli su odigrali adute
                            else
                            {
                                if (haveThatColor) index = indMin;
                                else if (leftAduts > 0)
                                {
                                    for (int i = 0; i < 8; i++)
                                    {
                                        if (UpperPlayerCard[i].getColor() == calledColor && UpperPlayerCard[i].getValue() > maxTempValue && UpperPlayerCard[i].getPlayed() == false)
                                        {
                                            maxTempValue = UpperPlayerCard[i].getValue();
                                            indTempMax = i;
                                        }
                                        if (UpperPlayerCard[i].getColor() == calledColor && UpperPlayerCard[i].getValue() < minTempValue && UpperPlayerCard[i].getPlayed() == false)
                                        {
                                            minTempValue = UpperPlayerCard[i].getValue();
                                            indTempMin = i;
                                        }
                                    }
                                    if (maxTempValue > MyPlayedCard.getValue() && maxTempValue > RightPlayerPlayedCard.getValue()) index = indTempMax;
                                    else index = indTempMin;
                                }
                                else
                                {
                                    for (int i = 0; i < 8; i++)
                                    {
                                        if (UpperPlayerCard[i].getValue() < minTempValue && UpperPlayerCard[i].getPlayed() == false)
                                        {
                                            minTempValue = UpperPlayerCard[i].getValue();
                                            index = i;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //
            }


            UpperPlayerPlayedCard.setColor(UpperPlayerCard[index].getColor());
            UpperPlayerPlayedCard.setValue(UpperPlayerCard[index].getValue());
            UpperPlayerPlayedCard.setID(UpperPlayerCard[index].getID());
            UpperPlayerPlayedCard.setPlayableID(UpperPlayerCard[index].getPlayableID());
            UpperPlayerPlayedCard.setName(UpperPlayerCard[index].getName());
            UpperPlayerCard[index].setPlayed(true);
            findUpperPlayedCard();
           // printTheirCards(8);

            Player_cardDrop.Play();
        }
        public void leftPlayerIsPlaying()
        {

            //MessageBox.Show("Lijevi igra");
            int index = -1;
            bool deckoAdut = false, devetkaAdut = false, kecAdut = false, desetkaAdut = false, kraljAdut = false, damaAdut = false, osmicaAdut = false, sedmicaAdut = false;
            int indDecko = -1, indDevetka = -1, indKec = -1, indDesetka = -1, indKralj = -1, indDama = -1, indOsmica = -1, indSedmica = -1;
            bool KecZir = false, kecList = false, kecSrce = false, kecBundeva = false;
            int indKecZir = -1, indKecList = -1, indKecSrce = -1, indKecBundeva = -1;
            int maxValue = -1, minValue = 100;
            int indMax = -1, indMin = -1;
            bool haveThatColor = false;
            int maxPlayedValue = -1;
            int maxTempValue = -1, minTempValue = 100;
            int indTempMax = -1, indTempMin = -1;


            int leftAduts = 0;

            for (int i = 0; i < 8; i++)
            {

                if (LeftPlayerCard[i].getColor() == calledColor && LeftPlayerCard[i].getPlayed() == false)
                {
                    leftAduts++;

                    if (LeftPlayerCard[i].getName() == "Decko")
                    {
                        deckoAdut = true;
                        indDecko = i;
                    }
                    else if (LeftPlayerCard[i].getName() == "9")
                    {
                        devetkaAdut = true;
                        indDevetka = i;
                    }
                    else if (LeftPlayerCard[i].getName() == "Kec")
                    {
                        kecAdut = true;
                        indKec = i;
                    }
                    else if (LeftPlayerCard[i].getName() == "10")
                    {
                        desetkaAdut = true;
                        indDesetka = i;
                    }
                    else if (LeftPlayerCard[i].getName() == "Kralj")
                    {
                        kraljAdut = true;
                        indKralj = i;
                    }
                    else if (LeftPlayerCard[i].getName() == "Dama")
                    {
                        damaAdut = true;
                        indDama = i;
                    }
                    else if (LeftPlayerCard[i].getName() == "8")
                    {
                        osmicaAdut = true;
                        indOsmica = i;
                    }
                    else if (LeftPlayerCard[i].getName() == "7")
                    {
                        sedmicaAdut = true;
                        indSedmica = i;
                    }

                }
                else
                {
                    if (LeftPlayerCard[i].getName() == "Kec" && LeftPlayerCard[i].getPlayed() == false)
                    {
                        if (LeftPlayerCard[i].getColor() == "Zir")
                        {
                            indKecZir = i;
                            KecZir = true;
                        }
                        else if (LeftPlayerCard[i].getColor() == "List")
                        {
                            indKecList = i;
                            kecList = true;
                        }
                        else if (LeftPlayerCard[i].getColor() == "Srce")
                        {
                            indKecSrce = i;
                            kecSrce = true;
                        }
                        else if (LeftPlayerCard[i].getColor() == "Bundeva")
                        {
                            indKecBundeva = i;
                            kecBundeva = true;
                        }
                    }
                }


            }
            //on prvi igra te je zvao
            if (playerWhoPlay == 3 && numOfCardsPlayed == 0)
            {
                if (leftAduts > 1 && deckoAdut)
                {
                    index = indDecko;
                }
                else if (leftAduts > 1 && (osmicaAdut || sedmicaAdut))
                {
                    if (osmicaAdut) index = indOsmica;
                    else if (sedmicaAdut) index = indSedmica;
                }
                else if (KecZir || kecList || kecSrce || kecBundeva)
                {
                    if (KecZir) index = indKecZir;
                    else if (kecList) index = indKecList;
                    else if (kecSrce) index = indKecSrce;
                    else if (kecBundeva) index = indKecBundeva;
                }
                else
                {
                    index = getRandomNumber(0, 8);
                }
            }
            else
            {
                //******************************************************************************************************************************
                //on prvi igra ali nije zvao
                if (playerWhoPlay == 3)
                {
                    if (leftAduts > 2)
                    {
                        if (deckoAdut) index = indDecko;
                        else if (kraljAdut) index = indKralj;
                        else if (damaAdut) index = indDama;
                        else if (sedmicaAdut) index = indSedmica;
                        else if (osmicaAdut) index = indOsmica;
                    }
                    else if (KecZir || kecList || kecBundeva)
                    {
                        if (KecZir) index = indKecZir;
                        else if (kecList) index = indKecList;
                        else if (kecSrce) index = indKecSrce;
                        else if (kecBundeva) index = indKecBundeva;
                    }
                    else
                    {
                        do
                        {
                            index = getRandomNumber(0, 8);
                        } while (LeftPlayerCard[index].getPlayed() == true);
                    }

                }
                //************************************************************************************************************************
                //prosli igra prvi
                else if (playerWhoPlay == 2)
                {


                    //prosli je odigrao aduta
                    if (mainPlayedCard.getColor() == calledColor)
                    {



                        for (int i = 0; i < 8; i++)
                        {
                            if (LeftPlayerCard[i].getColor() == calledColor && LeftPlayerCard[i].getValue() > maxValue && LeftPlayerCard[i].getPlayed() == false)
                            {
                                maxValue = LeftPlayerCard[i].getValue();
                                indMax = i;
                            }
                            if (LeftPlayerCard[i].getColor() == calledColor && LeftPlayerCard[i].getValue() < minValue && LeftPlayerCard[i].getPlayed() == false)
                            {
                                minValue = LeftPlayerCard[i].getValue();
                                indMin = i;
                            }

                        }
                        if (leftAduts == 0)
                        {
                            for (int i = 0; i < 8; i++)
                            {
                                if (LeftPlayerCard[i].getValue() < minTempValue && LeftPlayerCard[i].getPlayed() == false)
                                {
                                    minTempValue = LeftPlayerCard[i].getValue();
                                    index = i;
                                }
                            }

                        }
                        else if (mainPlayedCard.getValue() < maxValue) index = indMax;
                        else index = indMin;

                    }
                    //#########################################################
                    //prosli igra, prosli je odigro ne aduta
                    else if (mainPlayedCard.getColor() != calledColor)
                    {

                        for (int i = 0; i < 8; i++)
                        {
                            if (LeftPlayerCard[i].getColor() != calledColor && mainPlayedCard.getColor() == LeftPlayerCard[i].getColor() && LeftPlayerCard[i].getPlayed() == false && LeftPlayerCard[i].getValue() > maxValue)
                            {
                                maxValue = LeftPlayerCard[i].getValue();
                                indMax = i;
                                haveThatColor = true;
                            }
                            if (LeftPlayerCard[i].getColor() != calledColor && mainPlayedCard.getColor() == LeftPlayerCard[i].getColor() && LeftPlayerCard[i].getPlayed() == false && LeftPlayerCard[i].getValue() < minValue)
                            {
                                minValue = LeftPlayerCard[i].getValue();
                                indMin = i;
                                haveThatColor = true;
                            }

                        }

                        if (haveThatColor == false)
                        {
                            if (leftAduts == 0)
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (LeftPlayerCard[i].getValue() < minTempValue && LeftPlayerCard[i].getPlayed() == false)
                                    {
                                        minTempValue = LeftPlayerCard[i].getValue();
                                        index = i;
                                    }
                                }
                            }
                            else
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (LeftPlayerCard[i].getColor() == calledColor && minTempValue > LeftPlayerCard[i].getValue() && LeftPlayerCard[i].getPlayed() == false)
                                    {
                                        minTempValue = LeftPlayerCard[i].getValue();
                                        index = i;
                                    }
                                }
                            }
                        }
                        else if (mainPlayedCard.getValue() < maxValue) index = indMax;
                        else  index = indMin;


                    }




                }
                //*****************************************************************************************************************************
                //prvi igra pret-prosli igrac
                else if (playerWhoPlay == 1)
                {


                    //pret-prosli je odigrao aduta
                    if (mainPlayedCard.getColor() == calledColor)
                    {



                        for (int i = 0; i < 8; i++)
                        {
                            if (LeftPlayerCard[i].getColor() == calledColor && LeftPlayerCard[i].getValue() > maxValue && LeftPlayerCard[i].getPlayed() == false)
                            {
                                maxValue = LeftPlayerCard[i].getValue();
                                indMax = i;
                            }
                            if (LeftPlayerCard[i].getColor() == calledColor && LeftPlayerCard[i].getValue() < minValue && LeftPlayerCard[i].getPlayed() == false)
                            {
                                minValue = LeftPlayerCard[i].getValue();
                                indMin = i;
                            }

                        }
                        //pret-prosli zvao, obojca igraju aduta
                        if (mainPlayedCard.getColor() == calledColor && UpperPlayerPlayedCard.getColor() == calledColor)
                        {
                            if (mainPlayedCard.getValue() > UpperPlayerPlayedCard.getValue()) maxPlayedValue = mainPlayedCard.getValue();
                            else maxPlayedValue = UpperPlayerPlayedCard.getValue();
                        }
                        //donji ne igra aduta
                        else
                        {
                            maxPlayedValue = mainPlayedCard.getValue();
                        }



                        if (leftAduts == 0)
                        {
                            for (int i = 0; i < 8; i++)
                            {
                                if (LeftPlayerCard[i].getValue() < minTempValue && LeftPlayerCard[i].getPlayed() == false)
                                {
                                    minTempValue = LeftPlayerCard[i].getValue();
                                    index = i;
                                }
                            }
                        }
                        else if (maxPlayedValue < maxValue) index = indMax;
                        else index = indMin;

                    }
                    //####################################################
                    //pret-prosli je odigro ne aduta
                    else if (mainPlayedCard.getColor() != calledColor)
                    {

                        for (int i = 0; i < 8; i++)
                        {
                            if (LeftPlayerCard[i].getColor() != calledColor && mainPlayedCard.getColor() == LeftPlayerCard[i].getColor() && LeftPlayerCard[i].getValue() > maxValue && LeftPlayerCard[i].getPlayed() == false)
                            {
                                maxValue = LeftPlayerCard[i].getValue();
                                indMax = i;
                                haveThatColor = true;
                            }
                            if (LeftPlayerCard[i].getColor() != calledColor && mainPlayedCard.getColor() == LeftPlayerCard[i].getColor() && LeftPlayerCard[i].getValue() < minValue && LeftPlayerCard[i].getPlayed() == false)
                            {
                                minValue = LeftPlayerCard[i].getValue();
                                indMin = i;
                                haveThatColor = true;
                            }

                        }
                        //obojca su odigrala ne adute
                        if (mainPlayedCard.getColor() != calledColor && UpperPlayerPlayedCard.getColor() != calledColor)
                        {
                            if (mainPlayedCard.getValue() > UpperPlayerPlayedCard.getValue()) maxPlayedValue = mainPlayedCard.getValue();
                            else maxPlayedValue = UpperPlayerPlayedCard.getValue();

                            if (haveThatColor)
                            {
                                if (maxValue > maxPlayedValue) index = indMax;
                                else index = indMin;
                            }
                            else if (leftAduts > 0)
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (LeftPlayerCard[i].getColor() == calledColor && LeftPlayerCard[i].getValue() > maxTempValue && LeftPlayerCard[i].getPlayed() == false)
                                    {
                                        maxTempValue = LeftPlayerCard[i].getValue();
                                        indTempMax = i;
                                    }

                                }
                                index = indTempMax;
                            }
                            else
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (LeftPlayerCard[i].getValue() < minTempValue && LeftPlayerCard[i].getPlayed() == false)
                                    {
                                        minTempValue = LeftPlayerCard[i].getValue();
                                        index = i;
                                    }
                                }
                            }

                        }
                        //prosli je odigrao aduta
                        else
                        {
                            if (haveThatColor) index = indMin;
                            else if (leftAduts > 0)
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (LeftPlayerCard[i].getColor() == calledColor && LeftPlayerCard[i].getValue() > maxTempValue && LeftPlayerCard[i].getPlayed() == false)
                                    {
                                        maxTempValue = LeftPlayerCard[i].getValue();
                                        indTempMax = i;
                                    }
                                    if (LeftPlayerCard[i].getColor() == calledColor && LeftPlayerCard[i].getValue() < minTempValue && LeftPlayerCard[i].getPlayed() == false)
                                    {
                                        minTempValue = LeftPlayerCard[i].getValue();
                                        indTempMin = i;
                                    }
                                }
                                if (maxTempValue > UpperPlayerPlayedCard.getValue()) index = indTempMax;
                                else index = indTempMin;
                            }
                            else
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (LeftPlayerCard[i].getValue() < minTempValue && LeftPlayerCard[i].getPlayed() == false)
                                    {
                                        minTempValue = LeftPlayerCard[i].getValue();
                                        index = i;
                                    }
                                }
                            }

                        }


                    }

                }
                //*********************************************************************************************************************
                //prvi igra pret-pret-prosli igrac
                else if (playerWhoPlay == 0)
                {


                    //pret-pret-prosli je odigrao aduta
                    if (mainPlayedCard.getColor() == calledColor)
                    {



                        for (int i = 0; i < 8; i++)
                        {
                            if (LeftPlayerCard[i].getColor() == calledColor && LeftPlayerCard[i].getValue() > maxValue && LeftPlayerCard[i].getPlayed() == false)
                            {
                                maxValue = LeftPlayerCard[i].getValue();
                                indMax = i;
                            }
                            if (LeftPlayerCard[i].getColor() == calledColor && LeftPlayerCard[i].getValue() < minValue && LeftPlayerCard[i].getPlayed() == false)
                            {
                                minValue = LeftPlayerCard[i].getValue();
                                indMin = i;
                            }

                        }
                        //svi igraju adute
                        if (mainPlayedCard.getColor() == calledColor && RightPlayerPlayedCard.getColor() == calledColor && UpperPlayerPlayedCard.getColor() == calledColor)
                        {
                            if ((mainPlayedCard.getValue() > RightPlayerPlayedCard.getValue()) && (mainPlayedCard.getValue() > UpperPlayerPlayedCard.getValue())) maxPlayedValue = mainPlayedCard.getValue();
                            else if ((RightPlayerPlayedCard.getValue() > mainPlayedCard.getValue()) && (RightPlayerPlayedCard.getValue() > UpperPlayerPlayedCard.getValue())) maxPlayedValue = RightPlayerPlayedCard.getValue();
                            else maxPlayedValue = UpperPlayerPlayedCard.getValue();

                            if (leftAduts == 0)
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (LeftPlayerCard[i].getValue() < minTempValue && LeftPlayerCard[i].getPlayed() == false)
                                    {
                                        minTempValue = LeftPlayerCard[i].getValue();
                                        index = i;
                                    }
                                }
                            }
                            else if (maxPlayedValue < maxValue) index = indMax;
                            else index = indMin;



                        }
                        //pret-prosli ne odigra aduta
                        else if (mainPlayedCard.getColor() == calledColor && RightPlayerPlayedCard.getColor() != calledColor && UpperPlayerPlayedCard.getColor() == calledColor)
                        {
                            if (mainPlayedCard.getValue() > UpperPlayerPlayedCard.getValue()) maxPlayedValue = mainPlayedCard.getValue();
                            else maxPlayedValue = UpperPlayerPlayedCard.getValue();

                            if (leftAduts == 0)
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (LeftPlayerCard[i].getValue() < minTempValue && LeftPlayerCard[i].getPlayed() == false)
                                    {
                                        minTempValue = LeftPlayerCard[i].getValue();
                                        index = i;
                                    }
                                }
                            }
                            else if (maxPlayedValue < maxValue) index = indMax;
                            else index = indMin;
                        }
                        //prosli ne odigra aduta
                        else if (mainPlayedCard.getColor() == calledColor && RightPlayerPlayedCard.getColor() == calledColor && UpperPlayerPlayedCard.getColor() != calledColor)
                        {
                            if (mainPlayedCard.getValue() > RightPlayerPlayedCard.getValue()) maxPlayedValue = mainPlayedCard.getValue();
                            else maxPlayedValue = RightPlayerPlayedCard.getValue();

                            if (leftAduts == 0)
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (LeftPlayerCard[i].getValue() < minTempValue && LeftPlayerCard[i].getPlayed() == false)
                                    {
                                        minTempValue = LeftPlayerCard[i].getValue();
                                        index = i;
                                    }
                                }
                            }
                            else if (maxPlayedValue < maxValue) index = indMax;
                            else index = indMin;
                        }
                        //lijevi i donji ne odigraju aduta
                        else
                        {
                            maxPlayedValue = mainPlayedCard.getValue();
                            if (leftAduts == 0)
                            {
                                for (int i = 0; i < 8; i++)
                                {
                                    if (LeftPlayerCard[i].getValue() < minTempValue && LeftPlayerCard[i].getPlayed() == false)
                                    {
                                        minTempValue = LeftPlayerCard[i].getValue();
                                        index = i;
                                    }
                                }
                            }
                            else if (maxPlayedValue < maxValue) index = indMax;
                            else index = indMin;

                        }


                    }
                    //###########################################
                    //pret-pret-prosli odigra ne aduta
                    else
                    {
                        if (mainPlayedCard.getColor() != calledColor)
                        {
                            //max i min moja karta u toj boji
                            for (int i = 0; i < 8; i++)
                            {
                                if (LeftPlayerCard[i].getColor() != calledColor && mainPlayedCard.getColor() == LeftPlayerCard[i].getColor() && LeftPlayerCard[i].getValue() > maxValue && LeftPlayerCard[i].getPlayed() == false)
                                {
                                    maxValue = LeftPlayerCard[i].getValue();
                                    indMax = i;
                                    haveThatColor = true;
                                }
                                if (LeftPlayerCard[i].getColor() != calledColor && mainPlayedCard.getColor() == LeftPlayerCard[i].getColor() && LeftPlayerCard[i].getValue() < minValue && LeftPlayerCard[i].getPlayed() == false)
                                {
                                    minValue = LeftPlayerCard[i].getValue();
                                    indMin = i;
                                    haveThatColor = true;
                                }

                            }
                            //sva trojca su odigrala ne adute
                            if (mainPlayedCard.getColor() != calledColor && RightPlayerPlayedCard.getColor() != calledColor && UpperPlayerPlayedCard.getColor() != calledColor)
                            {
                                if ((mainPlayedCard.getValue() > RightPlayerPlayedCard.getValue()) && (mainPlayedCard.getValue() > UpperPlayerPlayedCard.getValue())) maxPlayedValue = mainPlayedCard.getValue();
                                else if ((RightPlayerPlayedCard.getValue() > mainPlayedCard.getValue()) && (RightPlayerPlayedCard.getValue() > UpperPlayerPlayedCard.getValue())) maxPlayedValue = RightPlayerPlayedCard.getValue();
                                else maxPlayedValue = UpperPlayerPlayedCard.getValue();

                                if (haveThatColor)
                                {
                                    if (maxValue > maxPlayedValue) index = indMax;
                                    else index = indMin;
                                }
                                else if (leftAduts > 0)
                                {
                                    for (int i = 0; i < 8; i++)
                                    {
                                        if (LeftPlayerCard[i].getColor() == calledColor && LeftPlayerCard[i].getValue() > maxTempValue && LeftPlayerCard[i].getPlayed() == false)
                                        {
                                            maxTempValue = LeftPlayerCard[i].getValue();
                                            indTempMax = i;
                                        }
                                    }
                                    index = indTempMax;
                                }
                                else
                                {
                                    for (int i = 0; i < 8; i++)
                                    {
                                        if (LeftPlayerCard[i].getValue() < minTempValue && LeftPlayerCard[i].getPlayed() == false)
                                        {
                                            minTempValue = LeftPlayerCard[i].getValue();
                                            index = i;
                                        }
                                    }
                                }

                            }
                            //prosli je odigrao aduta, pret-prosli ne aduta
                            else if (UpperPlayerPlayedCard.getColor() == calledColor && RightPlayerPlayedCard.getColor() != calledColor)
                            {
                                if (haveThatColor) index = indMin;
                                else if (leftAduts > 0)
                                {
                                    for (int i = 0; i < 8; i++)
                                    {
                                        if (LeftPlayerCard[i].getColor() == calledColor && LeftPlayerCard[i].getValue() > maxTempValue && LeftPlayerCard[i].getPlayed() == false)
                                        {
                                            maxTempValue = LeftPlayerCard[i].getValue();
                                            indTempMax = i;
                                        }
                                        if (LeftPlayerCard[i].getColor() == calledColor && LeftPlayerCard[i].getValue() < minTempValue && LeftPlayerCard[i].getPlayed() == false)
                                        {
                                            minTempValue = LeftPlayerCard[i].getValue();
                                            indTempMin = i;
                                        }
                                    }
                                    if (maxTempValue > UpperPlayerPlayedCard.getValue()) index = indTempMax;
                                    else index = indTempMin;
                                }
                                else
                                {
                                    for (int i = 0; i < 8; i++)
                                    {
                                        if (LeftPlayerCard[i].getValue() < minTempValue && LeftPlayerCard[i].getPlayed() == false)
                                        {
                                            minTempValue = LeftPlayerCard[i].getValue();
                                            index = i;
                                        }
                                    }
                                }


                            }
                            //prosli je odigrao ne aduta, pret-prosli aduta
                            else if (UpperPlayerPlayedCard.getColor() != calledColor && RightPlayerPlayedCard.getColor() == calledColor)
                            {
                                if (haveThatColor) index = indMin;
                                else if (leftAduts > 0)
                                {
                                    for (int i = 0; i < 8; i++)
                                    {
                                        if (LeftPlayerCard[i].getColor() == calledColor && LeftPlayerCard[i].getValue() > maxTempValue && LeftPlayerCard[i].getPlayed() == false)
                                        {
                                            maxTempValue = LeftPlayerCard[i].getValue();
                                            indTempMax = i;
                                        }
                                        if (LeftPlayerCard[i].getColor() == calledColor && LeftPlayerCard[i].getValue() < minTempValue && LeftPlayerCard[i].getPlayed() == false)
                                        {
                                            minTempValue = LeftPlayerCard[i].getValue();
                                            indTempMin = i;
                                        }

                                    }
                                    if (maxTempValue > RightPlayerPlayedCard.getValue()) index = indTempMax;
                                    else index = indTempMin;

                                }
                                else
                                {
                                    for (int i = 0; i < 8; i++)
                                    {
                                        if (LeftPlayerCard[i].getValue() < minTempValue && LeftPlayerCard[i].getPlayed() == false)
                                        {
                                            minTempValue = LeftPlayerCard[i].getValue();
                                            index = i;
                                        }
                                    }
                                }


                            }
                            //prosli i pret-prosli su odigrali adute
                            else
                            {
                                if (haveThatColor) index = indMin;
                                else if (leftAduts > 0)
                                {
                                    for (int i = 0; i < 8; i++)
                                    {
                                        if (LeftPlayerCard[i].getColor() == calledColor && LeftPlayerCard[i].getValue() > maxTempValue && LeftPlayerCard[i].getPlayed() == false)
                                        {
                                            maxTempValue = LeftPlayerCard[i].getValue();
                                            indTempMax = i;
                                        }
                                        if (LeftPlayerCard[i].getColor() == calledColor && LeftPlayerCard[i].getValue() < minTempValue && LeftPlayerCard[i].getPlayed() == false)
                                        {
                                            minTempValue = LeftPlayerCard[i].getValue();
                                            indTempMin = i;
                                        }
                                    }
                                    if (maxTempValue > RightPlayerPlayedCard.getValue() && maxTempValue > UpperPlayerPlayedCard.getValue()) index = indTempMax;
                                    else index = indTempMin;
                                }
                                else
                                {
                                    for (int i = 0; i < 8; i++)
                                    {
                                        if (LeftPlayerCard[i].getValue() < minTempValue && LeftPlayerCard[i].getPlayed() == false)
                                        {
                                            minTempValue = LeftPlayerCard[i].getValue();
                                            index = i;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //
            }


            LeftPlayerPlayedCard.setColor(LeftPlayerCard[index].getColor());
            LeftPlayerPlayedCard.setValue(LeftPlayerCard[index].getValue());
            LeftPlayerPlayedCard.setID(LeftPlayerCard[index].getID());
            LeftPlayerPlayedCard.setPlayableID(LeftPlayerCard[index].getPlayableID());
            LeftPlayerPlayedCard.setName(LeftPlayerCard[index].getName());
            LeftPlayerCard[index].setPlayed(true);
            findLeftPlayedCard();
           // printTheirCards(8);

            Player_cardDrop.Play();
        }

        public void timer_shuffleCards_Tick(object sender, EventArgs e)
        {
            timer_shuffleCards.Stop();
            Player_ShufflingCards.Stop();
            share6Cards();
            Play_Calling();
        }

        public void share6Cards()
        {
            show6DownCards();

            set6MyCards();
            set6RightPlayerCards();
            set6UpperPlayerCards();
            set6LeftPlayerCards();

            sortRightPlayerCards(6);
            sortUpperPlayerCards(6);
            sortLeftPlayerCards(6);
            //printTheirCards(6);
        }               
        public void set6MyCards()
        {

            int rnd;
            for (int i = 0; i < 6; i++)
            {
                do
                {
                    rnd = getRandomNumber(0, 32);
                } while (card[rnd].getShared() == true);

                myCard[i].setColor(card[rnd].getColor());
                myCard[i].setValue(card[rnd].getValue());
                myCard[i].setName(card[rnd].getName());
                myCard[i].setID(card[rnd].getID());
                myCard[i].setPlayableID(card[rnd].getPlayableID());
                myCard[i].setPlayed(false);

                card[rnd].setShared(true);
            }

            sortMyCards(6);
            loadImagesOnMyCards(6);
            
     
        }
        public void set6RightPlayerCards()
        {
            int rnd;
            for (int i = 0; i < 6; i++)
            {
                do
                {
                    rnd = getRandomNumber(0, 31);
                } while (card[rnd].getShared() == true);

                RightPlayerCard[i].setColor(card[rnd].getColor());
                RightPlayerCard[i].setValue(card[rnd].getValue());
                RightPlayerCard[i].setName(card[rnd].getName());
                RightPlayerCard[i].setID(card[rnd].getID());
                RightPlayerCard[i].setPlayableID(card[rnd].getPlayableID());
                RightPlayerCard[i].setPlayed(false);
                card[rnd].setShared(true);
            }
        
        }
        public void set6UpperPlayerCards()
        {
            int rnd;
            for (int i = 0; i < 6; i++)
            {
                do
                {
                    rnd = getRandomNumber(0, 31);
                } while (card[rnd].getShared() == true);

                UpperPlayerCard[i].setColor(card[rnd].getColor());
                UpperPlayerCard[i].setValue(card[rnd].getValue());
                UpperPlayerCard[i].setName(card[rnd].getName());
                UpperPlayerCard[i].setID(card[rnd].getID());
                UpperPlayerCard[i].setPlayableID(card[rnd].getPlayableID());
                UpperPlayerCard[i].setPlayed(false);
                card[rnd].setShared(true);
            }

        }
        public void set6LeftPlayerCards()
        {
            
            int rnd;
            for (int i = 0; i < 6; i++)
            {
                do
                {
                    rnd = getRandomNumber(0, 31);
                } while (card[rnd].getShared() == true);

                LeftPlayerCard[i].setColor(card[rnd].getColor());
                LeftPlayerCard[i].setValue(card[rnd].getValue());
                LeftPlayerCard[i].setName(card[rnd].getName());
                LeftPlayerCard[i].setID(card[rnd].getID());
                LeftPlayerCard[i].setPlayableID(card[rnd].getPlayableID());
                LeftPlayerCard[i].setPlayed(false);

                card[rnd].setShared(true);
            }
        }

        public void share2Cards()
        {
            show2DownCards();
            set2MyCards();
            set2RightPlayerCards();
            set2UpperPlayerCards();
            set2LeftPlayerCards();

            sortRightPlayerCards(8);
            sortUpperPlayerCards(8);
            sortLeftPlayerCards(8);

            //printTheirCards(8);
        }
        public void set2MyCards()
        {

            int rnd;
            for (int i = 6; i < 8; i++)
            {
                do
                {
                    rnd = getRandomNumber(0, 32);
                } while (card[rnd].getShared() == true);

                myCard[i].setColor(card[rnd].getColor());
                myCard[i].setValue(card[rnd].getValue());
                myCard[i].setName(card[rnd].getName());
                myCard[i].setID(card[rnd].getID());
                myCard[i].setPlayableID(card[rnd].getPlayableID());
                myCard[i].setPlayed(false);
                card[rnd].setShared(true);
            }

            sortMyCards(8);

            loadImagesOnMyCards(8);

        }
        public void set2RightPlayerCards()
        {
            int rnd;
            for (int i = 6; i < 8; i++)
            {
                do
                {
                    rnd = getRandomNumber(0, 32);
                } while (card[rnd].getShared() == true);

                RightPlayerCard[i].setColor(card[rnd].getColor());
                RightPlayerCard[i].setValue(card[rnd].getValue());
                RightPlayerCard[i].setName(card[rnd].getName());
                RightPlayerCard[i].setID(card[rnd].getID());
                RightPlayerCard[i].setPlayableID(card[rnd].getPlayableID());
                RightPlayerCard[i].setPlayed(false);
                card[rnd].setShared(true);
            }

        }
        public void set2UpperPlayerCards()
        {
            int rnd;
            for (int i = 6; i < 8; i++)
            {
                do
                {
                    rnd = getRandomNumber(0, 32);
                } while (card[rnd].getShared() == true);

                UpperPlayerCard[i].setColor(card[rnd].getColor());
                UpperPlayerCard[i].setValue(card[rnd].getValue());
                UpperPlayerCard[i].setName(card[rnd].getName());
                UpperPlayerCard[i].setID(card[rnd].getID());
                UpperPlayerCard[i].setPlayableID(card[rnd].getPlayableID());
                UpperPlayerCard[i].setPlayed(false);
                card[rnd].setShared(true);
            }

        }
        public void set2LeftPlayerCards()
        {
            
            int rnd;
            for (int i = 6; i < 8; i++)
            {
                do
                {
                    rnd = getRandomNumber(0, 32);
                } while (card[rnd].getShared() == true);

                LeftPlayerCard[i].setColor(card[rnd].getColor());
                LeftPlayerCard[i].setValue(card[rnd].getValue());
                LeftPlayerCard[i].setName(card[rnd].getName());
                LeftPlayerCard[i].setID(card[rnd].getID());
                LeftPlayerCard[i].setPlayableID(card[rnd].getPlayableID());
                LeftPlayerCard[i].setPlayed(false);
                card[rnd].setShared(true);
            }
        }


        public void sortMyCards(int n)
        {
            HandCards temp = new HandCards(); 

            if(n == 8)
            {
                for(int i = 0; i < n; i++)
                {
                    if(myCard[i].getColor() == calledColor && myCard[i].getName() == "Decko")
                    {
                        myCard[i].setID(myCard[i].getID() - 4.2);
                    }
                    if (myCard[i].getColor() == calledColor && myCard[i].getName() == "9")
                    {
                        myCard[i].setID(myCard[i].getID() - 5.1);
                    }

                }
            }

            for(int i = 0; i < n; i++)
            {
                for(int j = 0; j < n-1; j++)
                {
                    if (myCard[j].getID() > myCard[j+1].getID())
                    {
                        temp.setColor(myCard[j + 1].getColor());
                        temp.setID(myCard[j + 1].getID());
                        temp.setPlayableID(myCard[j + 1].getPlayableID());
                        temp.setName(myCard[j + 1].getName());
                        temp.setValue(myCard[j + 1].getValue());

                        myCard[j + 1].setColor(myCard[j].getColor());
                        myCard[j + 1].setID(myCard[j].getID());
                        myCard[j + 1].setPlayableID(myCard[j].getPlayableID());
                        myCard[j + 1].setName(myCard[j].getName());
                        myCard[j + 1].setValue(myCard[j].getValue());

                        myCard[j].setColor(temp.getColor());
                        myCard[j].setID(temp.getID());
                        myCard[j].setPlayableID(temp.getPlayableID());
                        myCard[j].setName(temp.getName());
                        myCard[j].setValue(temp.getValue());
                    }
                }
            }
            
        }
        public void sortRightPlayerCards(int n)
        {
            HandCards temp = new HandCards();
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n - 1; j++)
                {
                    if (RightPlayerCard[j].getID() > RightPlayerCard[j + 1].getID())
                    {
                        temp.setColor(RightPlayerCard[j + 1].getColor());
                        temp.setID(RightPlayerCard[j + 1].getID());
                        temp.setPlayableID(RightPlayerCard[j + 1].getPlayableID());
                        temp.setName(RightPlayerCard[j + 1].getName());
                        temp.setValue(RightPlayerCard[j + 1].getValue());

                        RightPlayerCard[j + 1].setColor(RightPlayerCard[j].getColor());
                        RightPlayerCard[j + 1].setID(RightPlayerCard[j].getID());
                        RightPlayerCard[j + 1].setPlayableID(RightPlayerCard[j].getPlayableID());
                        RightPlayerCard[j + 1].setName(RightPlayerCard[j].getName());
                        RightPlayerCard[j + 1].setValue(RightPlayerCard[j].getValue());

                        RightPlayerCard[j].setColor(temp.getColor());
                        RightPlayerCard[j].setID(temp.getID());
                        RightPlayerCard[j].setPlayableID(temp.getPlayableID());
                        RightPlayerCard[j].setName(temp.getName());
                        RightPlayerCard[j].setValue(temp.getValue());
                    }
                }
            }

        }
        public void sortUpperPlayerCards(int n)
        {
            HandCards temp = new HandCards();
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n - 1; j++)
                {
                    if (UpperPlayerCard[j].getID() > UpperPlayerCard[j + 1].getID())
                    {
                        temp.setColor(UpperPlayerCard[j + 1].getColor());
                        temp.setID(UpperPlayerCard[j + 1].getID());
                        temp.setPlayableID(UpperPlayerCard[j + 1].getPlayableID());
                        temp.setName(UpperPlayerCard[j + 1].getName());
                        temp.setValue(UpperPlayerCard[j + 1].getValue());

                        UpperPlayerCard[j + 1].setColor(UpperPlayerCard[j].getColor());
                        UpperPlayerCard[j + 1].setID(UpperPlayerCard[j].getID());
                        UpperPlayerCard[j + 1].setPlayableID(UpperPlayerCard[j].getPlayableID());
                        UpperPlayerCard[j + 1].setName(UpperPlayerCard[j].getName());
                        UpperPlayerCard[j + 1].setValue(UpperPlayerCard[j].getValue());

                        UpperPlayerCard[j].setColor(temp.getColor());
                        UpperPlayerCard[j].setID(temp.getID());
                        UpperPlayerCard[j].setPlayableID(temp.getPlayableID());
                        UpperPlayerCard[j].setName(temp.getName());
                        UpperPlayerCard[j].setValue(temp.getValue());
                    }
                }
            }

        }
        public void sortLeftPlayerCards(int n)
        {
            HandCards temp = new HandCards();
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n - 1; j++)
                {
                    if (LeftPlayerCard[j].getID() > LeftPlayerCard[j + 1].getID())
                    {
                        temp.setColor(LeftPlayerCard[j + 1].getColor());
                        temp.setID(LeftPlayerCard[j + 1].getID());
                        temp.setPlayableID(LeftPlayerCard[j + 1].getPlayableID());
                        temp.setName(LeftPlayerCard[j + 1].getName());
                        temp.setValue(LeftPlayerCard[j + 1].getValue());

                        LeftPlayerCard[j + 1].setColor(LeftPlayerCard[j].getColor());
                        LeftPlayerCard[j + 1].setID(LeftPlayerCard[j].getID());
                        LeftPlayerCard[j + 1].setPlayableID(LeftPlayerCard[j].getPlayableID());
                        LeftPlayerCard[j + 1].setName(LeftPlayerCard[j].getName());
                        LeftPlayerCard[j + 1].setValue(LeftPlayerCard[j].getValue());

                        LeftPlayerCard[j].setColor(temp.getColor());
                        LeftPlayerCard[j].setID(temp.getID());
                        LeftPlayerCard[j].setPlayableID(temp.getPlayableID());
                        LeftPlayerCard[j].setName(temp.getName());
                        LeftPlayerCard[j].setValue(temp.getValue());
                    }
                }
            }

        }

        public void loadImagesOnMyCards(int n)
        {
            if (n == 6) pb_handCard6.Visible = false;
            else if (n == 8) pb_handCard6.Visible = true;

            string[] location = new string[8];

            for (int i = 0; i < n; i++)
            {
                if (myCard[i].getColor() == "Zir" && myCard[i].getName() == "Kec") location[i] = "images\\ZirKec.png";
                else if (myCard[i].getColor() == "Zir" && myCard[i].getName() == "10") location[i] = "images\\Zir10.png";
                else if (myCard[i].getColor() == "Zir" && myCard[i].getName() == "Kralj") location[i] = "images\\ZirKralj.png";
                else if (myCard[i].getColor() == "Zir" && myCard[i].getName() == "Dama") location[i] = "images\\ZirDama.png";
                else if (myCard[i].getColor() == "Zir" && myCard[i].getName() == "Decko") location[i] = "images\\ZirDecko.png";
                else if (myCard[i].getColor() == "Zir" && myCard[i].getName() == "9") location[i] = "images\\Zir9.png";
                else if (myCard[i].getColor() == "Zir" && myCard[i].getName() == "8") location[i] = "images\\Zir8.png";
                else if (myCard[i].getColor() == "Zir" && myCard[i].getName() == "7") location[i] = "images\\Zir7.png";
                else if (myCard[i].getColor() == "List" && myCard[i].getName() == "Kec") location[i] = "images\\ListKec.png";
                else if (myCard[i].getColor() == "List" && myCard[i].getName() == "10") location[i] = "images\\List10.png";
                else if (myCard[i].getColor() == "List" && myCard[i].getName() == "Kralj") location[i] = "images\\ListKralj.png";
                else if (myCard[i].getColor() == "List" && myCard[i].getName() == "Dama") location[i] = "images\\ListDama.png";
                else if (myCard[i].getColor() == "List" && myCard[i].getName() == "Decko") location[i] = "images\\ListDecko.png";
                else if (myCard[i].getColor() == "List" && myCard[i].getName() == "9") location[i] = "images\\List9.png";
                else if (myCard[i].getColor() == "List" && myCard[i].getName() == "8") location[i] = "images\\List8.png";
                else if (myCard[i].getColor() == "List" && myCard[i].getName() == "7") location[i] = "images\\List7.png";
                else if (myCard[i].getColor() == "Srce" && myCard[i].getName() == "Kec") location[i] = "images\\SrceKec.png";
                else if (myCard[i].getColor() == "Srce" && myCard[i].getName() == "10") location[i] = "images\\Srce10.png";
                else if (myCard[i].getColor() == "Srce" && myCard[i].getName() == "Kralj") location[i] = "images\\SrceKralj.png";
                else if (myCard[i].getColor() == "Srce" && myCard[i].getName() == "Dama") location[i] = "images\\SrceDama.png";
                else if (myCard[i].getColor() == "Srce" && myCard[i].getName() == "Decko") location[i] = "images\\SrceDecko.png";
                else if (myCard[i].getColor() == "Srce" && myCard[i].getName() == "9") location[i] = "images\\Srce9.png";
                else if (myCard[i].getColor() == "Srce" && myCard[i].getName() == "8") location[i] = "images\\Srce8.png";
                else if (myCard[i].getColor() == "Srce" && myCard[i].getName() == "7") location[i] = "images\\Srce7.png";
                else if (myCard[i].getColor() == "Bundeva" && myCard[i].getName() == "Kec") location[i] = "images\\BundevaKec.png";
                else if (myCard[i].getColor() == "Bundeva" && myCard[i].getName() == "10") location[i] = "images\\Bundeva10.png";
                else if (myCard[i].getColor() == "Bundeva" && myCard[i].getName() == "Kralj") location[i] = "images\\BundevaKralj.png";
                else if (myCard[i].getColor() == "Bundeva" && myCard[i].getName() == "Dama") location[i] = "images\\BundevaDama.png";
                else if (myCard[i].getColor() == "Bundeva" && myCard[i].getName() == "Decko") location[i] = "images\\BundevaDecko.png";
                else if (myCard[i].getColor() == "Bundeva" && myCard[i].getName() == "9") location[i] = "images\\Bundeva9.png";
                else if (myCard[i].getColor() == "Bundeva" && myCard[i].getName() == "8") location[i] = "images\\Bundeva8.png";
                else if (myCard[i].getColor() == "Bundeva" && myCard[i].getName() == "7") location[i] = "images\\Bundeva7.png";

            }

            pb_handCard0.ImageLocation = location[0];
            pb_handCard1.ImageLocation = location[1];
            pb_handCard2.ImageLocation = location[2];
            pb_handCard3.ImageLocation = location[3];
            pb_handCard4.ImageLocation = location[4];
            pb_handCard5.ImageLocation = location[5];
           

            pb_handCard0.SizeMode = PictureBoxSizeMode.StretchImage;
            pb_handCard1.SizeMode = PictureBoxSizeMode.StretchImage;
            pb_handCard2.SizeMode = PictureBoxSizeMode.StretchImage;
            pb_handCard3.SizeMode = PictureBoxSizeMode.StretchImage;
            pb_handCard4.SizeMode = PictureBoxSizeMode.StretchImage;
            pb_handCard5.SizeMode = PictureBoxSizeMode.StretchImage;
           

            if (n == 8)
            {
                pb_handCard6.ImageLocation = location[6];
                pb_handCard7.ImageLocation = location[7];

                pb_handCard6.SizeMode = PictureBoxSizeMode.StretchImage;
                pb_handCard7.SizeMode = PictureBoxSizeMode.StretchImage;
            }




        }

        /*public void printTheirCards(int n)
        {
            tb_RightPlayersCards.Text = "";
            tb_UpperPlayersCards.Text = "";
            tb_LeftPlayersCards.Text = "";
            for (int i = 0; i < n; i++)
            {
                if(RightPlayerCard[i].getPlayed() == false) tb_RightPlayersCards.Text += RightPlayerCard[i].getColor() + " " + RightPlayerCard[i].getName() + " " + RightPlayerCard[i].getValue() + Environment.NewLine;
                if(UpperPlayerCard[i].getPlayed() == false) tb_UpperPlayersCards.Text += UpperPlayerCard[i].getColor() + " " + UpperPlayerCard[i].getName() + " " + UpperPlayerCard[i].getValue() + Environment.NewLine;
                if(LeftPlayerCard[i].getPlayed() == false) tb_LeftPlayersCards.Text += LeftPlayerCard[i].getColor() + " " + LeftPlayerCard[i].getName() + " " + LeftPlayerCard[i].getValue() + Environment.NewLine;
            }


        }*/


        public void whoFirstMix()
        {
            Mixer = getRandomNumber(0, 4);
            
        }
        public void nextMixer()
        {
            Mixer++;
            if (Mixer == 4) Mixer = 0;            
        }
        public void whoCalls()
        {
            if (Mixer == 3)
            {              
                possibleCaller = 0;                
                doYouCall();
            }
            else if (Mixer == 0)
            {
                possibleCaller = 1;
                rightPlayerIsThinkingToCall();                
            }
            else if (Mixer == 1)
            {
                possibleCaller = 2;
                upperPlayerIsThinkingToCall();
            }
            else if (Mixer == 2)
            {
                possibleCaller = 3;
                leftPlayerIsThinkingToCall();
            }
        }
        public void nextCaller()
        {
            //Player_nextCaller.Play();

            possibleCaller++;
            if (possibleCaller == 4) possibleCaller = 0;

            if (possibleCaller == 0)
            {
                doYouCall();
            }
            else if (possibleCaller == 1)
            {
                rightPlayerIsThinkingToCall();              
            }
            
            else if (possibleCaller == 2)
            {
                upperPlayerIsThinkingToCall();               
            } 
            else if (possibleCaller == 3)
            {
                leftPlayerIsThinkingToCall();                
            }

        }

        public void doYouCall()
        {
            Player_ClockTicking.PlayLooping();
            pb_callBoja0.ImageLocation = "images\\upitnik.png";
            pb_callBoja0.SizeMode = PictureBoxSizeMode.StretchImage;

            modeYourTurnToCall = true;  
            setCallOptions();
         
        }
        public void doRightPlayerCall()
        {
            double maxPoints = 0, pointsZir = 0, pointsList = 0, pointsSrce = 0, pointsBundeva = 0;
            string possibleAdut = "";
            double countZir = 0, countList = 0, countSrce = 0, countBundeva = 0, countZeros = 0;
            bool deckoZir = false, deckoList = false, deckoSrce = false, deckoBundeva = false;
            bool devetkaZir = false, devetkaList = false, devetkaSrce = false, devetkaBundeva = false;
            bool kecZir = false, kecList = false, kecSrce = false, kecBundeva = false;
            bool damaZir = false, damaList = false, damaSrce = false, damaBundeva = false;
            bool kraljZir = false, kraljList = false, kraljSrce = false, kraljBundeva = false;
            bool iCall = false;
            int rndNum = 0;
          

            //ako ima dobre karte onda on zove, ako ne onda ide next caller          
            //MessageBox.Show("Desni zove");
            sortRightPlayerCards(6);


            for (int i = 0; i < 6; i++)
            {
                if (RightPlayerCard[i].getColor() == "Zir")
                {
                    countZir++;
                    if (RightPlayerCard[i].getName() == "Decko") deckoZir = true;
                    else if (RightPlayerCard[i].getName() == "9") devetkaZir = true;
                    else if (RightPlayerCard[i].getName() == "Kec") kecZir = true;
                    else if (RightPlayerCard[i].getName() == "Dama") damaZir = true;
                    else if (RightPlayerCard[i].getName() == "Kralj") kraljZir = true;

                }
                else if (RightPlayerCard[i].getColor() == "List")
                {
                    countList++;
                    if (RightPlayerCard[i].getName() == "Decko") deckoList = true;
                    else if (RightPlayerCard[i].getName() == "9") devetkaList = true;
                    else if (RightPlayerCard[i].getName() == "Kec") kecList = true;
                    else if (RightPlayerCard[i].getName() == "Dama") damaList = true;
                    else if (RightPlayerCard[i].getName() == "Kralj") kraljList = true;
                }
                else if (RightPlayerCard[i].getColor() == "Srce")
                {
                    countSrce++;
                    if (RightPlayerCard[i].getName() == "Decko") deckoSrce = true;
                    else if (RightPlayerCard[i].getName() == "9") devetkaSrce = true;
                    else if (RightPlayerCard[i].getName() == "Kec") kecSrce = true;
                    else if (RightPlayerCard[i].getName() == "Dama") damaSrce = true;
                    else if (RightPlayerCard[i].getName() == "Kralj") kraljSrce = true;

                }
                else if (RightPlayerCard[i].getColor() == "Bundeva")
                {
                    countBundeva++;
                    if (RightPlayerCard[i].getName() == "Decko") deckoBundeva = true;
                    else if (RightPlayerCard[i].getName() == "9") devetkaBundeva = true;
                    else if (RightPlayerCard[i].getName() == "Kec") kecBundeva = true;
                    else if (RightPlayerCard[i].getName() == "Dama") damaBundeva = true;
                    else if (RightPlayerCard[i].getName() == "Kralj") kraljBundeva = true;
                }              

            }

            for(int i = 0; i < 6; i++)
            {
                if (RightPlayerCard[i].getValue() == 0) countZeros++;
            }


            pointsZir = countZir * 1.5;
            pointsList = countList * 1.5;
            pointsSrce = countSrce * 1.5;
            pointsBundeva = countBundeva * 1.5;

            if (deckoZir) pointsZir += 2;
            if (deckoList) pointsList += 2;
            if (deckoSrce) pointsSrce += 2;
            if (deckoBundeva) pointsBundeva += 2;

            if (devetkaZir) pointsZir += 1;
            if (devetkaList) pointsList += 1;
            if (devetkaSrce) pointsSrce += 1;
            if (devetkaBundeva) pointsBundeva += 1;

            if (deckoZir && devetkaZir && kecZir) pointsZir += 1;
            if (deckoList && devetkaList && kecList) pointsList += 1;
            if (deckoSrce && devetkaSrce && kecSrce) pointsSrce += 1;
            if (deckoBundeva && devetkaBundeva && kecBundeva) pointsBundeva += 1;

            if (damaZir && kraljZir) pointsZir += 1;
            if (damaList && kraljList) pointsList += 1;
            if (damaSrce && kraljSrce) pointsSrce += 1;
            if (damaBundeva && kraljBundeva) pointsBundeva += 1;

            if (devetkaZir && kecZir) pointsZir += 1;
            if (devetkaList && kecList) pointsList += 1;
            if (devetkaSrce && kecSrce) pointsSrce += 1;
            if (devetkaBundeva && kecBundeva) pointsBundeva += 1;


            if (kecZir)
            {
                pointsList += 1;
                pointsSrce += 1;
                pointsBundeva += 1;
            }
            if (kecList)
            {
                pointsZir += 1;
                pointsSrce += 1;
                pointsBundeva += 1;
            }
            if (kecSrce)
            {
                pointsZir += 1;
                pointsList += 1;
                pointsBundeva += 1;
            }
            if (kecBundeva)
            {
                pointsZir += 1;
                pointsSrce += 1;
                pointsList += 1;
            }

            if (pointsZir > maxPoints)
            {
                possibleAdut = "Zir";
                maxPoints = pointsZir;
            }
            if (pointsList > maxPoints)
            {
                possibleAdut = "List";
                maxPoints = pointsList;
            }
            if (pointsSrce > maxPoints)
            {
                possibleAdut = "Srce";
                maxPoints = pointsSrce;
            }
            if (pointsBundeva > maxPoints)
            {
                possibleAdut = "Bundeva";
                maxPoints = pointsBundeva;
            }   

            maxPoints -= (countZeros * 0.5);
        
            rndNum = getRandomNumber(0, 10);    

            Player_ClockTicking.Stop();

            if (rndNum <= maxPoints)
            {
                iCall = true;
            }           
           
            if (Mixer == 1)
            {
                iCall = true;            
            }
            if (iCall)
            {
                if (possibleAdut == "Zir") calledZir();
                else if (possibleAdut == "List") calledList();
                else if (possibleAdut == "Srce") calledSrce();
                else if (possibleAdut == "Bundeva") calledBundeva();
                else calledZir();
            }
            else
            {
                pb_callBoja1.Image = null;
                nextCaller();
            }

        }
        public void doUpperPlayerCall()
        {
            double maxPoints = 0, pointsZir = 0, pointsList = 0, pointsSrce = 0, pointsBundeva = 0;
            string possibleAdut = "";
            double countZir = 0, countList = 0, countSrce = 0, countBundeva = 0, countZeros = 0;
            bool deckoZir = false, deckoList = false, deckoSrce = false, deckoBundeva = false;
            bool devetkaZir = false, devetkaList = false, devetkaSrce = false, devetkaBundeva = false;
            bool kecZir = false, kecList = false, kecSrce = false, kecBundeva = false;
            bool damaZir = false, damaList = false, damaSrce = false, damaBundeva = false;
            bool kraljZir = false, kraljList = false, kraljSrce = false, kraljBundeva = false;
            bool iCall = false;
            int rndNum = 0;

           

            //ako ima dobre karte onda on zove, ako ne onda ide next caller           
            //MessageBox.Show("Gornji zove");
            sortUpperPlayerCards(6);


            for (int i = 0; i < 6; i++)
            {
                if (UpperPlayerCard[i].getColor() == "Zir")
                {
                    countZir++;
                    if (UpperPlayerCard[i].getName() == "Decko") deckoZir = true;
                    else if (UpperPlayerCard[i].getName() == "9") devetkaZir = true;
                    else if (UpperPlayerCard[i].getName() == "Kec") kecZir = true;
                    else if (UpperPlayerCard[i].getName() == "Dama") damaZir = true;
                    else if (UpperPlayerCard[i].getName() == "Kralj") kraljZir = true;

                }
                else if (UpperPlayerCard[i].getColor() == "List")
                {
                    countList++;
                    if (UpperPlayerCard[i].getName() == "Decko") deckoList = true;
                    else if (UpperPlayerCard[i].getName() == "9") devetkaList = true;
                    else if (UpperPlayerCard[i].getName() == "Kec") kecList = true;
                    else if (UpperPlayerCard[i].getName() == "Dama") damaList = true;
                    else if (UpperPlayerCard[i].getName() == "Kralj") kraljList = true;
                }
                else if (UpperPlayerCard[i].getColor() == "Srce")
                {
                    countSrce++;
                    if (UpperPlayerCard[i].getName() == "Decko") deckoSrce = true;
                    else if (UpperPlayerCard[i].getName() == "9") devetkaSrce = true;
                    else if (UpperPlayerCard[i].getName() == "Kec") kecSrce = true;
                    else if (UpperPlayerCard[i].getName() == "Dama") damaSrce = true;
                    else if (UpperPlayerCard[i].getName() == "Kralj") kraljSrce = true;

                }
                else if (UpperPlayerCard[i].getColor() == "Bundeva")
                {
                    countBundeva++;
                    if (UpperPlayerCard[i].getName() == "Decko") deckoBundeva = true;
                    else if (UpperPlayerCard[i].getName() == "9") devetkaBundeva = true;
                    else if (UpperPlayerCard[i].getName() == "Kec") kecBundeva = true;
                    else if (UpperPlayerCard[i].getName() == "Dama") damaBundeva = true;
                    else if (UpperPlayerCard[i].getName() == "Kralj") kraljBundeva = true;
                }

            }

            for (int i = 0; i < 6; i++)
            {
                if (UpperPlayerCard[i].getValue() == 0) countZeros++;
            }


            pointsZir = countZir * 1.5;
            pointsList = countList * 1.5;
            pointsSrce = countSrce * 1.5;
            pointsBundeva = countBundeva * 1.5;

            if (deckoZir) pointsZir += 2;
            if (deckoList) pointsList += 2;
            if (deckoSrce) pointsSrce += 2;
            if (deckoBundeva) pointsBundeva += 2;

            if (devetkaZir) pointsZir += 1;
            if (devetkaList) pointsList += 1;
            if (devetkaSrce) pointsSrce += 1;
            if (devetkaBundeva) pointsBundeva += 1;

            if (deckoZir && devetkaZir && kecZir) pointsZir += 1;
            if (deckoList && devetkaList && kecList) pointsList += 1;
            if (deckoSrce && devetkaSrce && kecSrce) pointsSrce += 1;
            if (deckoBundeva && devetkaBundeva && kecBundeva) pointsBundeva += 1;

            if (damaZir && kraljZir) pointsZir += 1;
            if (damaList && kraljList) pointsList += 1;
            if (damaSrce && kraljSrce) pointsSrce += 1;
            if (damaBundeva && kraljBundeva) pointsBundeva += 1;

            if (devetkaZir && kecZir) pointsZir += 1;
            if (devetkaList && kecList) pointsList += 1;
            if (devetkaSrce && kecSrce) pointsSrce += 1;
            if (devetkaBundeva && kecBundeva) pointsBundeva += 1;


            if (kecZir)
            {
                pointsList += 1;
                pointsSrce += 1;
                pointsBundeva += 1;
            }
            if (kecList)
            {
                pointsZir += 1;
                pointsSrce += 1;
                pointsBundeva += 1;
            }
            if (kecSrce)
            {
                pointsZir += 1;
                pointsList += 1;
                pointsBundeva += 1;
            }
            if (kecBundeva)
            {
                pointsZir += 1;
                pointsSrce += 1;
                pointsList += 1;
            }

            if (pointsZir > maxPoints)
            {
                possibleAdut = "Zir";
                maxPoints = pointsZir;
            }
            if (pointsList > maxPoints)
            {
                possibleAdut = "List";
                maxPoints = pointsList;
            }
            if (pointsSrce > maxPoints)
            {
                possibleAdut = "Srce";
                maxPoints = pointsSrce;
            }
            if (pointsBundeva > maxPoints)
            {
                possibleAdut = "Bundeva";
                maxPoints = pointsBundeva;
            }

            maxPoints -= (countZeros * 0.5);

            rndNum = getRandomNumber(0, 10);
          

            Player_ClockTicking.Stop();

            if (rndNum <= maxPoints)
            {
                iCall = true;
            }

            if (Mixer == 2)
            {
                iCall = true;
            }
            if (iCall)
            {
                if (possibleAdut == "Zir") calledZir();
                else if (possibleAdut == "List") calledList();
                else if (possibleAdut == "Srce") calledSrce();
                else if (possibleAdut == "Bundeva") calledBundeva();
                else calledSrce();
            }
            else
            {
                pb_callBoja2.Image = null;
                nextCaller();
            }

        }
        public void doLeftPlayerCall()
        {
            double maxPoints = 0, pointsZir = 0, pointsList = 0, pointsSrce = 0, pointsBundeva = 0;
            string possibleAdut = "";
            double countZir = 0, countList = 0, countSrce = 0, countBundeva = 0, countZeros = 0;
            bool deckoZir = false, deckoList = false, deckoSrce = false, deckoBundeva = false;
            bool devetkaZir = false, devetkaList = false, devetkaSrce = false, devetkaBundeva = false;
            bool kecZir = false, kecList = false, kecSrce = false, kecBundeva = false;
            bool damaZir = false, damaList = false, damaSrce = false, damaBundeva = false;
            bool kraljZir = false, kraljList = false, kraljSrce = false, kraljBundeva = false;
            bool iCall = false;
            int rndNum = 0;

            

            //ako ima dobre karte onda on zove, ako ne onda ide next caller           
           // MessageBox.Show("Lijevi zove");
            sortLeftPlayerCards(6);


            for (int i = 0; i < 6; i++)
            {
                if (LeftPlayerCard[i].getColor() == "Zir")
                {
                    countZir++;
                    if (LeftPlayerCard[i].getName() == "Decko") deckoZir = true;
                    else if (LeftPlayerCard[i].getName() == "9") devetkaZir = true;
                    else if (LeftPlayerCard[i].getName() == "Kec") kecZir = true;
                    else if (LeftPlayerCard[i].getName() == "Dama") damaZir = true;
                    else if (LeftPlayerCard[i].getName() == "Kralj") kraljZir = true;

                }
                else if (LeftPlayerCard[i].getColor() == "List")
                {
                    countList++;
                    if (LeftPlayerCard[i].getName() == "Decko") deckoList = true;
                    else if (LeftPlayerCard[i].getName() == "9") devetkaList = true;
                    else if (LeftPlayerCard[i].getName() == "Kec") kecList = true;
                    else if (LeftPlayerCard[i].getName() == "Dama") damaList = true;
                    else if (LeftPlayerCard[i].getName() == "Kralj") kraljList = true;
                }
                else if (LeftPlayerCard[i].getColor() == "Srce")
                {
                    countSrce++;
                    if (LeftPlayerCard[i].getName() == "Decko") deckoSrce = true;
                    else if (LeftPlayerCard[i].getName() == "9") devetkaSrce = true;
                    else if (LeftPlayerCard[i].getName() == "Kec") kecSrce = true;
                    else if (LeftPlayerCard[i].getName() == "Dama") damaSrce = true;
                    else if (LeftPlayerCard[i].getName() == "Kralj") kraljSrce = true;

                }
                else if (LeftPlayerCard[i].getColor() == "Bundeva")
                {
                    countBundeva++;
                    if (LeftPlayerCard[i].getName() == "Decko") deckoBundeva = true;
                    else if (LeftPlayerCard[i].getName() == "9") devetkaBundeva = true;
                    else if (LeftPlayerCard[i].getName() == "Kec") kecBundeva = true;
                    else if (LeftPlayerCard[i].getName() == "Dama") damaBundeva = true;
                    else if (LeftPlayerCard[i].getName() == "Kralj") kraljBundeva = true;
                }

            }

            for (int i = 0; i < 6; i++)
            {
                if (LeftPlayerCard[i].getValue() == 0) countZeros++;
            }


            pointsZir = countZir * 1.5;
            pointsList = countList * 1.5;
            pointsSrce = countSrce * 1.5;
            pointsBundeva = countBundeva * 1.5;

            if (deckoZir) pointsZir += 2;
            if (deckoList) pointsList += 2;
            if (deckoSrce) pointsSrce += 2;
            if (deckoBundeva) pointsBundeva += 2;

            if (devetkaZir) pointsZir += 1;
            if (devetkaList) pointsList += 1;
            if (devetkaSrce) pointsSrce += 1;
            if (devetkaBundeva) pointsBundeva += 1;

            if (deckoZir && devetkaZir && kecZir) pointsZir += 1;
            if (deckoList && devetkaList && kecList) pointsList += 1;
            if (deckoSrce && devetkaSrce && kecSrce) pointsSrce += 1;
            if (deckoBundeva && devetkaBundeva && kecBundeva) pointsBundeva += 1;

            if (damaZir && kraljZir) pointsZir += 1;
            if (damaList && kraljList) pointsList += 1;
            if (damaSrce && kraljSrce) pointsSrce += 1;
            if (damaBundeva && kraljBundeva) pointsBundeva += 1;

            if (devetkaZir && kecZir) pointsZir += 1;
            if (devetkaList && kecList) pointsList += 1;
            if (devetkaSrce && kecSrce) pointsSrce += 1;
            if (devetkaBundeva && kecBundeva) pointsBundeva += 1;


            if (kecZir)
            {
                pointsList += 1;
                pointsSrce += 1;
                pointsBundeva += 1;
            }
            if (kecList)
            {
                pointsZir += 1;
                pointsSrce += 1;
                pointsBundeva += 1;
            }
            if (kecSrce)
            {
                pointsZir += 1;
                pointsList += 1;
                pointsBundeva += 1;
            }
            if (kecBundeva)
            {
                pointsZir += 1;
                pointsSrce += 1;
                pointsList += 1;
            }

            if (pointsZir > maxPoints)
            {
                possibleAdut = "Zir";
                maxPoints = pointsZir;
            }
            if (pointsList > maxPoints)
            {
                possibleAdut = "List";
                maxPoints = pointsList;
            }
            if (pointsSrce > maxPoints)
            {
                possibleAdut = "Srce";
                maxPoints = pointsSrce;
            }
            if (pointsBundeva > maxPoints)
            {
                possibleAdut = "Bundeva";
                maxPoints = pointsBundeva;
            }

            maxPoints -= (countZeros * 0.5);

            rndNum = getRandomNumber(0, 10);
            

            Player_ClockTicking.Stop();

            if (rndNum <= maxPoints)
            {
                iCall = true;
            }

            if (Mixer == 3)
            {
                iCall = true;
            }
            if (iCall)
            {
                if (possibleAdut == "Zir") calledZir();
                else if (possibleAdut == "List") calledList();
                else if (possibleAdut == "Srce") calledSrce();
                else if (possibleAdut == "Bundeva") calledBundeva();
                else calledBundeva();
            }
            else
            {
                pb_callBoja3.Image = null;
                nextCaller();
            }

        }

        public void rightPlayerIsThinkingToCall()
        {
            Player_ClockTicking.PlayLooping();

            timer_rightCall.Interval = getRandomNumber(1000, 5000);
            timer_rightCall.Start();

            pb_callBoja1.ImageLocation = "images\\upitnik.png";
            pb_callBoja1.SizeMode = PictureBoxSizeMode.StretchImage;
        }
        public void upperPlayerIsThinkingToCall()
        {
            Player_ClockTicking.PlayLooping();


            timer_upperCall.Interval = getRandomNumber(1000, 5000);
            timer_upperCall.Start();

            pb_callBoja2.ImageLocation = "images\\upitnik.png";
            pb_callBoja2.SizeMode = PictureBoxSizeMode.StretchImage;
        }
        public void leftPlayerIsThinkingToCall()
        {
            Player_ClockTicking.PlayLooping();

            timer_leftCall.Interval = getRandomNumber(1000, 5000);
            timer_leftCall.Start();

            pb_callBoja3.ImageLocation = "images\\upitnik.png";
            pb_callBoja3.SizeMode = PictureBoxSizeMode.StretchImage;
        }
       
        public void timer_youCall_Tick(object sender, EventArgs e)
        {
            
        }
        public void timer_rightCall_Tick(object sender, EventArgs e)
        {
            timer_rightCall.Stop();
            doRightPlayerCall();
        }
        public void timer_upperCall_Tick(object sender, EventArgs e)
        {
            timer_upperCall.Stop();
            doUpperPlayerCall();
        }
        public void timer_leftCall_Tick(object sender, EventArgs e)
        {
            timer_leftCall.Stop();
            doLeftPlayerCall();
        }


        public void setTimerOptions()
        {
            timer_youPlayCards.Stop();
            timer_youPlayCards.Interval = 200;
            timer_youPlayCards.Start();
        }
        public void resetTimerOptions()
        {
            timer_youPlayCards.Stop();
            timer_youPlayCards.Interval = 1000000;
        }

        public void findMyPlayedCard()
        {
            string[] location = new string[1];
            int i = 0;

            if (MyPlayedCard.getColor() == "Zir" && MyPlayedCard.getName() == "Kec") location[i] = "images\\ZirKec.png";
            else if (MyPlayedCard.getColor() == "Zir" && MyPlayedCard.getName() == "10") location[i] = "images\\Zir10.png";
            else if (MyPlayedCard.getColor() == "Zir" && MyPlayedCard.getName() == "Kralj") location[i] = "images\\ZirKralj.png";
            else if (MyPlayedCard.getColor() == "Zir" && MyPlayedCard.getName() == "Dama") location[i] = "images\\ZirDama.png";
            else if (MyPlayedCard.getColor() == "Zir" && MyPlayedCard.getName() == "Decko") location[i] = "images\\ZirDecko.png";
            else if (MyPlayedCard.getColor() == "Zir" && MyPlayedCard.getName() == "9") location[i] = "images\\Zir9.png";
            else if (MyPlayedCard.getColor() == "Zir" && MyPlayedCard.getName() == "8") location[i] = "images\\Zir8.png";
            else if (MyPlayedCard.getColor() == "Zir" && MyPlayedCard.getName() == "7") location[i] = "images\\Zir7.png";
            else if (MyPlayedCard.getColor() == "List" && MyPlayedCard.getName() == "Kec") location[i] = "images\\ListKec.png";
            else if (MyPlayedCard.getColor() == "List" && MyPlayedCard.getName() == "10") location[i] = "images\\List10.png";
            else if (MyPlayedCard.getColor() == "List" && MyPlayedCard.getName() == "Kralj") location[i] = "images\\ListKralj.png";
            else if (MyPlayedCard.getColor() == "List" && MyPlayedCard.getName() == "Dama") location[i] = "images\\ListDama.png";
            else if (MyPlayedCard.getColor() == "List" && MyPlayedCard.getName() == "Decko") location[i] = "images\\ListDecko.png";
            else if (MyPlayedCard.getColor() == "List" && MyPlayedCard.getName() == "9") location[i] = "images\\List9.png";
            else if (MyPlayedCard.getColor() == "List" && MyPlayedCard.getName() == "8") location[i] = "images\\List8.png";
            else if (MyPlayedCard.getColor() == "List" && MyPlayedCard.getName() == "7") location[i] = "images\\List7.png";
            else if (MyPlayedCard.getColor() == "Srce" && MyPlayedCard.getName() == "Kec") location[i] = "images\\SrceKec.png";
            else if (MyPlayedCard.getColor() == "Srce" && MyPlayedCard.getName() == "10") location[i] = "images\\Srce10.png";
            else if (MyPlayedCard.getColor() == "Srce" && MyPlayedCard.getName() == "Kralj") location[i] = "images\\SrceKralj.png";
            else if (MyPlayedCard.getColor() == "Srce" && MyPlayedCard.getName() == "Dama") location[i] = "images\\SrceDama.png";
            else if (MyPlayedCard.getColor() == "Srce" && MyPlayedCard.getName() == "Decko") location[i] = "images\\SrceDecko.png";
            else if (MyPlayedCard.getColor() == "Srce" && MyPlayedCard.getName() == "9") location[i] = "images\\Srce9.png";
            else if (MyPlayedCard.getColor() == "Srce" && MyPlayedCard.getName() == "8") location[i] = "images\\Srce8.png";
            else if (MyPlayedCard.getColor() == "Srce" && MyPlayedCard.getName() == "7") location[i] = "images\\Srce7.png";
            else if (MyPlayedCard.getColor() == "Bundeva" && MyPlayedCard.getName() == "Kec") location[i] = "images\\BundevaKec.png";
            else if (MyPlayedCard.getColor() == "Bundeva" && MyPlayedCard.getName() == "10") location[i] = "images\\Bundeva10.png";
            else if (MyPlayedCard.getColor() == "Bundeva" && MyPlayedCard.getName() == "Kralj") location[i] = "images\\BundevaKralj.png";
            else if (MyPlayedCard.getColor() == "Bundeva" && MyPlayedCard.getName() == "Dama") location[i] = "images\\BundevaDama.png";
            else if (MyPlayedCard.getColor() == "Bundeva" && MyPlayedCard.getName() == "Decko") location[i] = "images\\BundevaDecko.png";
            else if (MyPlayedCard.getColor() == "Bundeva" && MyPlayedCard.getName() == "9") location[i] = "images\\Bundeva9.png";
            else if (MyPlayedCard.getColor() == "Bundeva" && MyPlayedCard.getName() == "8") location[i] = "images\\Bundeva8.png";
            else if (MyPlayedCard.getColor() == "Bundeva" && MyPlayedCard.getName() == "7") location[i] = "images\\Bundeva7.png";

            
            pb_playedCardMy.ImageLocation = location[0];
            pb_playedCardMy.SizeMode = PictureBoxSizeMode.StretchImage;

        }
        public void findRightPlayedCard()
        {
            string[] location = new string[1];
            int i = 0;

            if (RightPlayerPlayedCard.getColor() == "Zir" && RightPlayerPlayedCard.getName() == "Kec") location[i] = "images\\ZirKec.png";
            else if (RightPlayerPlayedCard.getColor() == "Zir" && RightPlayerPlayedCard.getName() == "10") location[i] = "images\\Zir10.png";
            else if (RightPlayerPlayedCard.getColor() == "Zir" && RightPlayerPlayedCard.getName() == "Kralj") location[i] = "images\\ZirKralj.png";
            else if (RightPlayerPlayedCard.getColor() == "Zir" && RightPlayerPlayedCard.getName() == "Dama") location[i] = "images\\ZirDama.png";
            else if (RightPlayerPlayedCard.getColor() == "Zir" && RightPlayerPlayedCard.getName() == "Decko") location[i] = "images\\ZirDecko.png";
            else if (RightPlayerPlayedCard.getColor() == "Zir" && RightPlayerPlayedCard.getName() == "9") location[i] = "images\\Zir9.png";
            else if (RightPlayerPlayedCard.getColor() == "Zir" && RightPlayerPlayedCard.getName() == "8") location[i] = "images\\Zir8.png";
            else if (RightPlayerPlayedCard.getColor() == "Zir" && RightPlayerPlayedCard.getName() == "7") location[i] = "images\\Zir7.png";
            else if (RightPlayerPlayedCard.getColor() == "List" && RightPlayerPlayedCard.getName() == "Kec") location[i] = "images\\ListKec.png";
            else if (RightPlayerPlayedCard.getColor() == "List" && RightPlayerPlayedCard.getName() == "10") location[i] = "images\\List10.png";
            else if (RightPlayerPlayedCard.getColor() == "List" && RightPlayerPlayedCard.getName() == "Kralj") location[i] = "images\\ListKralj.png";
            else if (RightPlayerPlayedCard.getColor() == "List" && RightPlayerPlayedCard.getName() == "Dama") location[i] = "images\\ListDama.png";
            else if (RightPlayerPlayedCard.getColor() == "List" && RightPlayerPlayedCard.getName() == "Decko") location[i] = "images\\ListDecko.png";
            else if (RightPlayerPlayedCard.getColor() == "List" && RightPlayerPlayedCard.getName() == "9") location[i] = "images\\List9.png";
            else if (RightPlayerPlayedCard.getColor() == "List" && RightPlayerPlayedCard.getName() == "8") location[i] = "images\\List8.png";
            else if (RightPlayerPlayedCard.getColor() == "List" && RightPlayerPlayedCard.getName() == "7") location[i] = "images\\List7.png";
            else if (RightPlayerPlayedCard.getColor() == "Srce" && RightPlayerPlayedCard.getName() == "Kec") location[i] = "images\\SrceKec.png";
            else if (RightPlayerPlayedCard.getColor() == "Srce" && RightPlayerPlayedCard.getName() == "10") location[i] = "images\\Srce10.png";
            else if (RightPlayerPlayedCard.getColor() == "Srce" && RightPlayerPlayedCard.getName() == "Kralj") location[i] = "images\\SrceKralj.png";
            else if (RightPlayerPlayedCard.getColor() == "Srce" && RightPlayerPlayedCard.getName() == "Dama") location[i] = "images\\SrceDama.png";
            else if (RightPlayerPlayedCard.getColor() == "Srce" && RightPlayerPlayedCard.getName() == "Decko") location[i] = "images\\SrceDecko.png";
            else if (RightPlayerPlayedCard.getColor() == "Srce" && RightPlayerPlayedCard.getName() == "9") location[i] = "images\\Srce9.png";
            else if (RightPlayerPlayedCard.getColor() == "Srce" && RightPlayerPlayedCard.getName() == "8") location[i] = "images\\Srce8.png";
            else if (RightPlayerPlayedCard.getColor() == "Srce" && RightPlayerPlayedCard.getName() == "7") location[i] = "images\\Srce7.png";
            else if (RightPlayerPlayedCard.getColor() == "Bundeva" && RightPlayerPlayedCard.getName() == "Kec") location[i] = "images\\BundevaKec.png";
            else if (RightPlayerPlayedCard.getColor() == "Bundeva" && RightPlayerPlayedCard.getName() == "10") location[i] = "images\\Bundeva10.png";
            else if (RightPlayerPlayedCard.getColor() == "Bundeva" && RightPlayerPlayedCard.getName() == "Kralj") location[i] = "images\\BundevaKralj.png";
            else if (RightPlayerPlayedCard.getColor() == "Bundeva" && RightPlayerPlayedCard.getName() == "Dama") location[i] = "images\\BundevaDama.png";
            else if (RightPlayerPlayedCard.getColor() == "Bundeva" && RightPlayerPlayedCard.getName() == "Decko") location[i] = "images\\BundevaDecko.png";
            else if (RightPlayerPlayedCard.getColor() == "Bundeva" && RightPlayerPlayedCard.getName() == "9") location[i] = "images\\Bundeva9.png";
            else if (RightPlayerPlayedCard.getColor() == "Bundeva" && RightPlayerPlayedCard.getName() == "8") location[i] = "images\\Bundeva8.png";
            else if (RightPlayerPlayedCard.getColor() == "Bundeva" && RightPlayerPlayedCard.getName() == "7") location[i] = "images\\Bundeva7.png";

            pb_playedCardRight.ImageLocation = location[0];
            pb_playedCardRight.SizeMode = PictureBoxSizeMode.StretchImage;

        }
        public void findUpperPlayedCard()
        {
            string[] location = new string[1];
            int i = 0;

            if (UpperPlayerPlayedCard.getColor() == "Zir" && UpperPlayerPlayedCard.getName() == "Kec") location[i] = "images\\ZirKec.png";
            else if (UpperPlayerPlayedCard.getColor() == "Zir" && UpperPlayerPlayedCard.getName() == "10") location[i] = "images\\Zir10.png";
            else if (UpperPlayerPlayedCard.getColor() == "Zir" && UpperPlayerPlayedCard.getName() == "Kralj") location[i] = "images\\ZirKralj.png";
            else if (UpperPlayerPlayedCard.getColor() == "Zir" && UpperPlayerPlayedCard.getName() == "Dama") location[i] = "images\\ZirDama.png";
            else if (UpperPlayerPlayedCard.getColor() == "Zir" && UpperPlayerPlayedCard.getName() == "Decko") location[i] = "images\\ZirDecko.png";
            else if (UpperPlayerPlayedCard.getColor() == "Zir" && UpperPlayerPlayedCard.getName() == "9") location[i] = "images\\Zir9.png";
            else if (UpperPlayerPlayedCard.getColor() == "Zir" && UpperPlayerPlayedCard.getName() == "8") location[i] = "images\\Zir8.png";
            else if (UpperPlayerPlayedCard.getColor() == "Zir" && UpperPlayerPlayedCard.getName() == "7") location[i] = "images\\Zir7.png";
            else if (UpperPlayerPlayedCard.getColor() == "List" && UpperPlayerPlayedCard.getName() == "Kec") location[i] = "images\\ListKec.png";
            else if (UpperPlayerPlayedCard.getColor() == "List" && UpperPlayerPlayedCard.getName() == "10") location[i] = "images\\List10.png";
            else if (UpperPlayerPlayedCard.getColor() == "List" && UpperPlayerPlayedCard.getName() == "Kralj") location[i] = "images\\ListKralj.png";
            else if (UpperPlayerPlayedCard.getColor() == "List" && UpperPlayerPlayedCard.getName() == "Dama") location[i] = "images\\ListDama.png";
            else if (UpperPlayerPlayedCard.getColor() == "List" && UpperPlayerPlayedCard.getName() == "Decko") location[i] = "images\\ListDecko.png";
            else if (UpperPlayerPlayedCard.getColor() == "List" && UpperPlayerPlayedCard.getName() == "9") location[i] = "images\\List9.png";
            else if (UpperPlayerPlayedCard.getColor() == "List" && UpperPlayerPlayedCard.getName() == "8") location[i] = "images\\List8.png";
            else if (UpperPlayerPlayedCard.getColor() == "List" && UpperPlayerPlayedCard.getName() == "7") location[i] = "images\\List7.png";
            else if (UpperPlayerPlayedCard.getColor() == "Srce" && UpperPlayerPlayedCard.getName() == "Kec") location[i] = "images\\SrceKec.png";
            else if (UpperPlayerPlayedCard.getColor() == "Srce" && UpperPlayerPlayedCard.getName() == "10") location[i] = "images\\Srce10.png";
            else if (UpperPlayerPlayedCard.getColor() == "Srce" && UpperPlayerPlayedCard.getName() == "Kralj") location[i] = "images\\SrceKralj.png";
            else if (UpperPlayerPlayedCard.getColor() == "Srce" && UpperPlayerPlayedCard.getName() == "Dama") location[i] = "images\\SrceDama.png";
            else if (UpperPlayerPlayedCard.getColor() == "Srce" && UpperPlayerPlayedCard.getName() == "Decko") location[i] = "images\\SrceDecko.png";
            else if (UpperPlayerPlayedCard.getColor() == "Srce" && UpperPlayerPlayedCard.getName() == "9") location[i] = "images\\Srce9.png";
            else if (UpperPlayerPlayedCard.getColor() == "Srce" && UpperPlayerPlayedCard.getName() == "8") location[i] = "images\\Srce8.png";
            else if (UpperPlayerPlayedCard.getColor() == "Srce" && UpperPlayerPlayedCard.getName() == "7") location[i] = "images\\Srce7.png";
            else if (UpperPlayerPlayedCard.getColor() == "Bundeva" && UpperPlayerPlayedCard.getName() == "Kec") location[i] = "images\\BundevaKec.png";
            else if (UpperPlayerPlayedCard.getColor() == "Bundeva" && UpperPlayerPlayedCard.getName() == "10") location[i] = "images\\Bundeva10.png";
            else if (UpperPlayerPlayedCard.getColor() == "Bundeva" && UpperPlayerPlayedCard.getName() == "Kralj") location[i] = "images\\BundevaKralj.png";
            else if (UpperPlayerPlayedCard.getColor() == "Bundeva" && UpperPlayerPlayedCard.getName() == "Dama") location[i] = "images\\BundevaDama.png";
            else if (UpperPlayerPlayedCard.getColor() == "Bundeva" && UpperPlayerPlayedCard.getName() == "Decko") location[i] = "images\\BundevaDecko.png";
            else if (UpperPlayerPlayedCard.getColor() == "Bundeva" && UpperPlayerPlayedCard.getName() == "9") location[i] = "images\\Bundeva9.png";
            else if (UpperPlayerPlayedCard.getColor() == "Bundeva" && UpperPlayerPlayedCard.getName() == "8") location[i] = "images\\Bundeva8.png";
            else if (UpperPlayerPlayedCard.getColor() == "Bundeva" && UpperPlayerPlayedCard.getName() == "7") location[i] = "images\\Bundeva7.png";

            pb_playedCardUpper.ImageLocation = location[0];
            pb_playedCardUpper.SizeMode = PictureBoxSizeMode.StretchImage;

        }
        public void findLeftPlayedCard()
        {
            string[] location = new string[1];
            int i = 0;

            if (LeftPlayerPlayedCard.getColor() == "Zir" && LeftPlayerPlayedCard.getName() == "Kec") location[i] = "images\\ZirKec.png";
            else if (LeftPlayerPlayedCard.getColor() == "Zir" && LeftPlayerPlayedCard.getName() == "10") location[i] = "images\\Zir10.png";
            else if (LeftPlayerPlayedCard.getColor() == "Zir" && LeftPlayerPlayedCard.getName() == "Kralj") location[i] = "images\\ZirKralj.png";
            else if (LeftPlayerPlayedCard.getColor() == "Zir" && LeftPlayerPlayedCard.getName() == "Dama") location[i] = "images\\ZirDama.png";
            else if (LeftPlayerPlayedCard.getColor() == "Zir" && LeftPlayerPlayedCard.getName() == "Decko") location[i] = "images\\ZirDecko.png";
            else if (LeftPlayerPlayedCard.getColor() == "Zir" && LeftPlayerPlayedCard.getName() == "9") location[i] = "images\\Zir9.png";
            else if (LeftPlayerPlayedCard.getColor() == "Zir" && LeftPlayerPlayedCard.getName() == "8") location[i] = "images\\Zir8.png";
            else if (LeftPlayerPlayedCard.getColor() == "Zir" && LeftPlayerPlayedCard.getName() == "7") location[i] = "images\\Zir7.png";
            else if (LeftPlayerPlayedCard.getColor() == "List" && LeftPlayerPlayedCard.getName() == "Kec") location[i] = "images\\ListKec.png";
            else if (LeftPlayerPlayedCard.getColor() == "List" && LeftPlayerPlayedCard.getName() == "10") location[i] = "images\\List10.png";
            else if (LeftPlayerPlayedCard.getColor() == "List" && LeftPlayerPlayedCard.getName() == "Kralj") location[i] = "images\\ListKralj.png";
            else if (LeftPlayerPlayedCard.getColor() == "List" && LeftPlayerPlayedCard.getName() == "Dama") location[i] = "images\\ListDama.png";
            else if (LeftPlayerPlayedCard.getColor() == "List" && LeftPlayerPlayedCard.getName() == "Decko") location[i] = "images\\ListDecko.png";
            else if (LeftPlayerPlayedCard.getColor() == "List" && LeftPlayerPlayedCard.getName() == "9") location[i] = "images\\List9.png";
            else if (LeftPlayerPlayedCard.getColor() == "List" && LeftPlayerPlayedCard.getName() == "8") location[i] = "images\\List8.png";
            else if (LeftPlayerPlayedCard.getColor() == "List" && LeftPlayerPlayedCard.getName() == "7") location[i] = "images\\List7.png";
            else if (LeftPlayerPlayedCard.getColor() == "Srce" && LeftPlayerPlayedCard.getName() == "Kec") location[i] = "images\\SrceKec.png";
            else if (LeftPlayerPlayedCard.getColor() == "Srce" && LeftPlayerPlayedCard.getName() == "10") location[i] = "images\\Srce10.png";
            else if (LeftPlayerPlayedCard.getColor() == "Srce" && LeftPlayerPlayedCard.getName() == "Kralj") location[i] = "images\\SrceKralj.png";
            else if (LeftPlayerPlayedCard.getColor() == "Srce" && LeftPlayerPlayedCard.getName() == "Dama") location[i] = "images\\SrceDama.png";
            else if (LeftPlayerPlayedCard.getColor() == "Srce" && LeftPlayerPlayedCard.getName() == "Decko") location[i] = "images\\SrceDecko.png";
            else if (LeftPlayerPlayedCard.getColor() == "Srce" && LeftPlayerPlayedCard.getName() == "9") location[i] = "images\\Srce9.png";
            else if (LeftPlayerPlayedCard.getColor() == "Srce" && LeftPlayerPlayedCard.getName() == "8") location[i] = "images\\Srce8.png";
            else if (LeftPlayerPlayedCard.getColor() == "Srce" && LeftPlayerPlayedCard.getName() == "7") location[i] = "images\\Srce7.png";
            else if (LeftPlayerPlayedCard.getColor() == "Bundeva" && LeftPlayerPlayedCard.getName() == "Kec") location[i] = "images\\BundevaKec.png";
            else if (LeftPlayerPlayedCard.getColor() == "Bundeva" && LeftPlayerPlayedCard.getName() == "10") location[i] = "images\\Bundeva10.png";
            else if (LeftPlayerPlayedCard.getColor() == "Bundeva" && LeftPlayerPlayedCard.getName() == "Kralj") location[i] = "images\\BundevaKralj.png";
            else if (LeftPlayerPlayedCard.getColor() == "Bundeva" && LeftPlayerPlayedCard.getName() == "Dama") location[i] = "images\\BundevaDama.png";
            else if (LeftPlayerPlayedCard.getColor() == "Bundeva" && LeftPlayerPlayedCard.getName() == "Decko") location[i] = "images\\BundevaDecko.png";
            else if (LeftPlayerPlayedCard.getColor() == "Bundeva" && LeftPlayerPlayedCard.getName() == "9") location[i] = "images\\Bundeva9.png";
            else if (LeftPlayerPlayedCard.getColor() == "Bundeva" && LeftPlayerPlayedCard.getName() == "8") location[i] = "images\\Bundeva8.png";
            else if (LeftPlayerPlayedCard.getColor() == "Bundeva" && LeftPlayerPlayedCard.getName() == "7") location[i] = "images\\Bundeva7.png";

            
            pb_playedCardLeft.ImageLocation = location[0];
            pb_playedCardLeft.SizeMode = PictureBoxSizeMode.StretchImage;

        }

        private void pb_handCard0_Click(object sender, EventArgs e)
        {
            if (youPlay == true && playableCard[0] == true)
            {
                lb_naPotezuSi.Visible = false;
                youPlay = false;               
                MyPlayedCard.setColor(myCard[0].getColor());
                MyPlayedCard.setID(myCard[0].getID());
                MyPlayedCard.setName(myCard[0].getName());
                MyPlayedCard.setValue(myCard[0].getValue());
                myCard[0].setPlayed(true);
                pb_handCard0.Visible = false;
                findMyPlayedCard();
                setTimerOptions();
                Player_cardDrop.Play();

            }
        }
        private void pb_handCard1_Click_1(object sender, EventArgs e)
        {
            if (youPlay == true && playableCard[1] == true)
            {
                lb_naPotezuSi.Visible = false;
                youPlay = false;           
                MyPlayedCard.setColor(myCard[1].getColor());
                MyPlayedCard.setID(myCard[1].getID());
                MyPlayedCard.setName(myCard[1].getName());
                MyPlayedCard.setValue(myCard[1].getValue());
                myCard[1].setPlayed(true);
                pb_handCard1.Visible = false;
                findMyPlayedCard();
                setTimerOptions();
                Player_cardDrop.Play();

            }
        }
        private void pb_handCard2_Click_1(object sender, EventArgs e)
        {
            if (youPlay == true && playableCard[2] == true)
            {
                lb_naPotezuSi.Visible = false;
                youPlay = false;
                MyPlayedCard.setColor(myCard[2].getColor());
                MyPlayedCard.setID(myCard[2].getID());
                MyPlayedCard.setName(myCard[2].getName());
                MyPlayedCard.setValue(myCard[2].getValue());
                myCard[2].setPlayed(true);
                pb_handCard2.Visible = false;
                findMyPlayedCard();
                setTimerOptions();
                Player_cardDrop.Play();

            }
        }      
        private void pb_handCard3_Click_1(object sender, EventArgs e)
        {
            if (youPlay == true && playableCard[3] == true)
            {
                lb_naPotezuSi.Visible = false;
                youPlay = false;
                MyPlayedCard.setColor(myCard[3].getColor());
                MyPlayedCard.setID(myCard[3].getID());
                MyPlayedCard.setName(myCard[3].getName());
                MyPlayedCard.setValue(myCard[3].getValue());
                myCard[3].setPlayed(true);
                pb_handCard3.Visible = false;
                findMyPlayedCard();
                setTimerOptions();
                Player_cardDrop.Play();

            }
        }
        private void pb_handCard4_Click_1(object sender, EventArgs e)
        {
            if (youPlay == true && playableCard[4] == true)
            {
                lb_naPotezuSi.Visible = false;
                youPlay = false;
                MyPlayedCard.setColor(myCard[4].getColor());
                MyPlayedCard.setID(myCard[4].getID());
                MyPlayedCard.setName(myCard[4].getName());
                MyPlayedCard.setValue(myCard[4].getValue());
                myCard[4].setPlayed(true);
                pb_handCard4.Visible = false;
                findMyPlayedCard();
                setTimerOptions();
                Player_cardDrop.Play();

            }
        }
        private void pb_handCard5_Click_1(object sender, EventArgs e)
        {

            if (youPlay == true && playableCard[5] == true)
            {
                lb_naPotezuSi.Visible = false;
                youPlay = false;
                MyPlayedCard.setColor(myCard[5].getColor());
                MyPlayedCard.setID(myCard[5].getID());
                MyPlayedCard.setName(myCard[5].getName());
                MyPlayedCard.setValue(myCard[5].getValue());
                myCard[5].setPlayed(true);
                pb_handCard5.Visible = false;
                findMyPlayedCard();
                setTimerOptions();
                Player_cardDrop.Play();

            }
        }
        private void pb_handCard6_Click_1(object sender, EventArgs e)
        {
            if (youPlay == true && playableCard[6] == true)
            {
                lb_naPotezuSi.Visible = false;
                youPlay = false;
                MyPlayedCard.setColor(myCard[6].getColor());
                MyPlayedCard.setID(myCard[6].getID());
                MyPlayedCard.setName(myCard[6].getName());
                MyPlayedCard.setValue(myCard[6].getValue());
                myCard[6].setPlayed(true);
                pb_handCard6.Visible = false;
                findMyPlayedCard();
                setTimerOptions();
                Player_cardDrop.Play();

            }
        }
        private void pb_handCard7_Click_1(object sender, EventArgs e)
        {
            if (youPlay == true && playableCard[7] == true)
            {
                lb_naPotezuSi.Visible = false;
                youPlay = false;
                MyPlayedCard.setColor(myCard[7].getColor());
                MyPlayedCard.setID(myCard[7].getID());
                MyPlayedCard.setName(myCard[7].getName());
                MyPlayedCard.setValue(myCard[7].getValue());
                myCard[7].setPlayed(true);
                pb_handCard7.Visible = false;
                findMyPlayedCard();
                setTimerOptions();
                Player_cardDrop.Play();

            }
        }


        private void pb_callZir_Click(object sender, EventArgs e)
        {
            Player_ClockTicking.Stop();

            modeYourTurnToCall = false;
            calledZir();
            moveCallOptions();
            mainPlayedCard.setColor("Zir");
        }
        private void pb_callList_Click(object sender, EventArgs e)
        {
            Player_ClockTicking.Stop();

            modeYourTurnToCall = false;
            calledList();
            moveCallOptions();
            mainPlayedCard.setColor("List");
        }
        private void pb_callSrce_Click(object sender, EventArgs e)
        {
            Player_ClockTicking.Stop();

            modeYourTurnToCall = false;
            calledSrce();
            moveCallOptions();
            mainPlayedCard.setColor("Srce");
        }
        private void pb_callBundeva_Click(object sender, EventArgs e)
        {
            Player_ClockTicking.Stop();

            modeYourTurnToCall = false;
            calledBundeva();
            moveCallOptions();
            mainPlayedCard.setColor("Bundeva");
        }

       

        private void pb_callDALJE_Click(object sender, EventArgs e)
        {
            Player_ClockTicking.Stop();

            pb_callBoja0.Image = null;
            modeYourTurnToCall = false;
            moveCallOptions();
            nextCaller();
        }



        public void calledZir()
        {
            Player_ReadyToPlay.Play();
            if(possibleCaller == 0)
            {
                pb_callBoja0.ImageLocation = "images\\BojaZir.png";
                pb_callBoja0.SizeMode = PictureBoxSizeMode.StretchImage;
            }
            else if (possibleCaller == 1)
            {
                pb_callBoja1.ImageLocation = "images\\BojaZir.png";
                pb_callBoja1.SizeMode = PictureBoxSizeMode.StretchImage;
            }
            else if (possibleCaller == 2)
            {
                pb_callBoja2.ImageLocation = "images\\BojaZir.png";
                pb_callBoja2.SizeMode = PictureBoxSizeMode.StretchImage;
            }
            else if (possibleCaller == 3)
            {
                pb_callBoja3.ImageLocation = "images\\BojaZir.png";
                pb_callBoja3.SizeMode = PictureBoxSizeMode.StretchImage;
            }
            calledColor = "Zir";
            share2Cards();
            for (int i = 0; i < 8; i++)
            {
                if (myCard[i].getColor() == "Zir" && myCard[i].getName() == "Decko") myCard[i].setValue(20);
                if (myCard[i].getColor() == "Zir" && myCard[i].getName() == "9") myCard[i].setValue(14);
                if (RightPlayerCard[i].getColor() == "Zir" && RightPlayerCard[i].getName() == "Decko") RightPlayerCard[i].setValue(20);
                if (RightPlayerCard[i].getColor() == "Zir" && RightPlayerCard[i].getName() == "9") RightPlayerCard[i].setValue(14);
                if (UpperPlayerCard[i].getColor() == "Zir" && UpperPlayerCard[i].getName() == "Decko") UpperPlayerCard[i].setValue(20);
                if (UpperPlayerCard[i].getColor() == "Zir" && UpperPlayerCard[i].getName() == "9") UpperPlayerCard[i].setValue(14);
                if (LeftPlayerCard[i].getColor() == "Zir" && LeftPlayerCard[i].getName() == "Decko") LeftPlayerCard[i].setValue(20);
                if (LeftPlayerCard[i].getColor() == "Zir" && LeftPlayerCard[i].getName() == "9") LeftPlayerCard[i].setValue(14);
            }
            
            card[4].setValue(20);
            card[5].setValue(14);           
            show4MiddleCards();
            PlayCards();
        }
        public void calledList()
        {
            Player_ReadyToPlay.Play();
            if (possibleCaller == 0)
            {
                pb_callBoja0.ImageLocation = "images\\BojaList.png";
                pb_callBoja0.SizeMode = PictureBoxSizeMode.StretchImage;
            }
            else if (possibleCaller == 1)
            {
                pb_callBoja1.ImageLocation = "images\\BojaList.png";
                pb_callBoja1.SizeMode = PictureBoxSizeMode.StretchImage;
            }
            else if (possibleCaller == 2)
            {
                pb_callBoja2.ImageLocation = "images\\BojaList.png";
                pb_callBoja2.SizeMode = PictureBoxSizeMode.StretchImage;
            }
            else if (possibleCaller == 3)
            {
                pb_callBoja3.ImageLocation = "images\\BojaList.png";
                pb_callBoja3.SizeMode = PictureBoxSizeMode.StretchImage;
            }
            calledColor = "List";
            share2Cards();
            for (int i = 0; i < 8; i++)
            {
                if (myCard[i].getColor() == "List" && myCard[i].getName() == "Decko") myCard[i].setValue(20);
                if (myCard[i].getColor() == "List" && myCard[i].getName() == "9") myCard[i].setValue(14);
                if (RightPlayerCard[i].getColor() == "List" && RightPlayerCard[i].getName() == "Decko") RightPlayerCard[i].setValue(20);
                if (RightPlayerCard[i].getColor() == "List" && RightPlayerCard[i].getName() == "9") RightPlayerCard[i].setValue(14);
                if (UpperPlayerCard[i].getColor() == "List" && UpperPlayerCard[i].getName() == "Decko") UpperPlayerCard[i].setValue(20);
                if (UpperPlayerCard[i].getColor() == "List" && UpperPlayerCard[i].getName() == "9") UpperPlayerCard[i].setValue(14);
                if (LeftPlayerCard[i].getColor() == "List" && LeftPlayerCard[i].getName() == "Decko") LeftPlayerCard[i].setValue(20);
                if (LeftPlayerCard[i].getColor() == "List" && LeftPlayerCard[i].getName() == "9") LeftPlayerCard[i].setValue(14);
            }
            
            card[12].setValue(20);
            card[13].setValue(14);             
            show4MiddleCards();
            PlayCards();
        }
        public void calledSrce()
        {
            Player_ReadyToPlay.Play();
            if (possibleCaller == 0)
            {
                pb_callBoja0.ImageLocation = "images\\BojaSrce.png";
                pb_callBoja0.SizeMode = PictureBoxSizeMode.StretchImage;
            }
            else if (possibleCaller == 1)
            {
                pb_callBoja1.ImageLocation = "images\\BojaSrce.png";
                pb_callBoja1.SizeMode = PictureBoxSizeMode.StretchImage;
            }
            else if (possibleCaller == 2)
            {
                pb_callBoja2.ImageLocation = "images\\BojaSrce.png";
                pb_callBoja2.SizeMode = PictureBoxSizeMode.StretchImage;
            }
            else if (possibleCaller == 3)
            {
                pb_callBoja3.ImageLocation = "images\\BojaSrce.png";
                pb_callBoja3.SizeMode = PictureBoxSizeMode.StretchImage;
            }
            calledColor = "Srce";
            share2Cards();
            for (int i = 0; i < 8; i++)
            {
                if (myCard[i].getColor() == "Srce" && myCard[i].getName() == "Decko") myCard[i].setValue(20);
                if (myCard[i].getColor() == "Srce" && myCard[i].getName() == "9") myCard[i].setValue(14);
                if (RightPlayerCard[i].getColor() == "Srce" && RightPlayerCard[i].getName() == "Decko") RightPlayerCard[i].setValue(20);
                if (RightPlayerCard[i].getColor() == "Srce" && RightPlayerCard[i].getName() == "9") RightPlayerCard[i].setValue(14);
                if (UpperPlayerCard[i].getColor() == "Srce" && UpperPlayerCard[i].getName() == "Decko") UpperPlayerCard[i].setValue(20);
                if (UpperPlayerCard[i].getColor() == "Srce" && UpperPlayerCard[i].getName() == "9") UpperPlayerCard[i].setValue(14);
                if (LeftPlayerCard[i].getColor() == "Srce" && LeftPlayerCard[i].getName() == "Decko") LeftPlayerCard[i].setValue(20);
                if (LeftPlayerCard[i].getColor() == "Srce" && LeftPlayerCard[i].getName() == "9") LeftPlayerCard[i].setValue(14);
            }
            
            card[20].setValue(20);
            card[21].setValue(14);           
            show4MiddleCards();
            PlayCards();
        }
        public void calledBundeva()
        {
            Player_ReadyToPlay.Play();
            if (possibleCaller == 0)
            {
                pb_callBoja0.ImageLocation = "images\\BojaBundeva.png";
                pb_callBoja0.SizeMode = PictureBoxSizeMode.StretchImage;
            }
            else if (possibleCaller == 1)
            {
                pb_callBoja1.ImageLocation = "images\\BojaBundeva.png";
                pb_callBoja1.SizeMode = PictureBoxSizeMode.StretchImage;
            }
            else if (possibleCaller == 2)
            {
                pb_callBoja2.ImageLocation = "images\\BojaBundeva.png";
                pb_callBoja2.SizeMode = PictureBoxSizeMode.StretchImage;
            }
            else if (possibleCaller == 3)
            {
                pb_callBoja3.ImageLocation = "images\\BojaBundeva.png";
                pb_callBoja3.SizeMode = PictureBoxSizeMode.StretchImage;
            }
            calledColor = "Bundeva";
            share2Cards();
            for (int i = 0; i < 8; i++)
            {
                if (myCard[i].getColor() == "Bundeva" && myCard[i].getName() == "Decko") myCard[i].setValue(20);
                if (myCard[i].getColor() == "Bundeva" && myCard[i].getName() == "9") myCard[i].setValue(14);
                if (RightPlayerCard[i].getColor() == "Bundeva" && RightPlayerCard[i].getName() == "Decko") RightPlayerCard[i].setValue(20);
                if (RightPlayerCard[i].getColor() == "Bundeva" && RightPlayerCard[i].getName() == "9") RightPlayerCard[i].setValue(14);
                if (UpperPlayerCard[i].getColor() == "Bundeva" && UpperPlayerCard[i].getName() == "Decko") UpperPlayerCard[i].setValue(20);
                if (UpperPlayerCard[i].getColor() == "Bundeva" && UpperPlayerCard[i].getName() == "9") UpperPlayerCard[i].setValue(14);
                if (LeftPlayerCard[i].getColor() == "Bundeva" && LeftPlayerCard[i].getName() == "Decko") LeftPlayerCard[i].setValue(20);
                if (LeftPlayerCard[i].getColor() == "Bundeva" && LeftPlayerCard[i].getName() == "9") LeftPlayerCard[i].setValue(14);
            }
            
            card[28].setValue(20);
            card[29].setValue(14);           
            show4MiddleCards();
            PlayCards();
        }

        public void setCallOptions()
        {
            pb_callZir.Visible = true;
            pb_callSrce.Visible = true;
            pb_callList.Visible = true;
            pb_callBundeva.Visible = true;
            if (Mixer == 0) pb_callDALJE.Visible = false;
            else pb_callDALJE.Visible = true;

            pb_callZir.ImageLocation = "images\\BojaZir.png";
            pb_callList.ImageLocation = "images\\BojaList.png";
            pb_callSrce.ImageLocation = "images\\BojaSrce.png";
            pb_callBundeva.ImageLocation = "images\\BojaBundeva.png";
            pb_callDALJE.ImageLocation = "images\\arrowRight.png";
            pb_callBoja0.ImageLocation = "images\\upitnik.png";

            pb_callZir.SizeMode = PictureBoxSizeMode.StretchImage;
            pb_callList.SizeMode = PictureBoxSizeMode.StretchImage;
            pb_callSrce.SizeMode = PictureBoxSizeMode.StretchImage;
            pb_callBundeva.SizeMode = PictureBoxSizeMode.StretchImage;
            pb_callDALJE.SizeMode = PictureBoxSizeMode.StretchImage;
            pb_callBoja0.SizeMode = PictureBoxSizeMode.StretchImage;
            
        }
        public void moveCallOptions()
        {
            pb_callZir.Visible = false;
            pb_callSrce.Visible = false;
            pb_callList.Visible = false;
            pb_callBundeva.Visible = false;
            pb_callDALJE.Visible = false;       
        }

        public void checkIfFilesExists()
        {
            if (!System.IO.Directory.Exists(@"docs"))
            {
                System.IO.Directory.CreateDirectory(@"docs");
            }

            if (!System.IO.File.Exists(@"docs\\statistics.gsm.txt"))
            {      
                using (System.IO.StreamWriter strWriter = new System.IO.StreamWriter(@"docs\\statistics.gsm.txt")) { }
                restartStatistics();
            }

            if (!System.IO.File.Exists(@"docs\\names.gsm.txt"))
            {
                using (System.IO.StreamWriter strWriter = new System.IO.StreamWriter(@"docs\\names.gsm.txt")) { }         
            }
        }
        public string getStatisticsByLine(int lineNum)
        {
            string wantedLine = "";
            using (System.IO.StreamReader file = new System.IO.StreamReader(@"docs\\statistics.gsm.txt"))
            {
                for(int i = 0; i < lineNum; i++)
                {
                    file.ReadLine();
                }
                wantedLine = file.ReadLine();
            }

            return wantedLine;
        }
        public void restartStatistics()
        {
            using (System.IO.StreamWriter fileWriter = new System.IO.StreamWriter(@"docs\\statistics.gsm.txt"))
            {
                for(int i = 0; i < 8; i++)
                {
                    fileWriter.WriteLine("0");
                }
                
            }
        }
        public void writeScatisticToSpecificLine(string newLineText, int lineNumber)
        {
            //ovo promjeni kada dodajes nove linije u statistiku
            //******
            int numOfStatisticsLines = 8;
            //******
            
            string[] line = new string[10];
            
            using (System.IO.StreamReader fileReader = new System.IO.StreamReader(@"docs\\statistics.gsm.txt"))
            {
               
                for(int i = 0; i < numOfStatisticsLines; i++)
                {
                    line[i] = fileReader.ReadLine();
                }
            }
            using (System.IO.StreamWriter fileWriter = new System.IO.StreamWriter(@"docs\\statistics.gsm.txt"))
            {
                for (int i = 0; i < numOfStatisticsLines; i++)
                {
                    if (i == lineNumber)
                    {
                        fileWriter.WriteLine(newLineText);
                    }
                    else
                    {
                        fileWriter.WriteLine(line[i]);
                    }
                }
            }
            

        }
        private void btn_getStatistics_Click(object sender, EventArgs e)
        {
            FormStatistics frmStat = new FormStatistics();

            frmStat.lb_formStatisticsPlayer0Name.Text = lb_yourName.Text;
            frmStat.lb_formStatisticsPlayer1Name.Text = lb_opponentRightName.Text;
            frmStat.lb_formStatisticsPlayer2Name.Text = lb_yourTeammateName.Text;
            frmStat.lb_formStatisticsPlayer3Name.Text = lb_opponentLeftName.Text;

            frmStat.lb_statisticsAllPointsOur.Text = getStatisticsByLine(2);
            frmStat.lb_statisticsAllPointsTheir.Text = getStatisticsByLine(3);

            frmStat.lb_statisticsTotalScoreOur.Text = getStatisticsByLine(0);
            frmStat.lb_statisticsTotalScoreTheir.Text = getStatisticsByLine(1);

            frmStat.lb_statisticsPlayerPoints0.Text = getStatisticsByLine(4);
            frmStat.lb_statisticsPlayerPoints1.Text = getStatisticsByLine(5);
            frmStat.lb_statisticsPlayerPoints2.Text = getStatisticsByLine(6);
            frmStat.lb_statisticsPlayerPoints3.Text = getStatisticsByLine(7);

            frmStat.ShowDialog();

        }


        private void btn_options_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Coming soon...");
            /*
            using (FormOptions frmOptions = new FormOptions())
            {
                frmOptions.ShowDialog(this);
            }
            */
        }



        public string getYourName()
        {
            return lb_yourName.Text;
        }
        public string getYourTeammateName()
        {
            return lb_yourTeammateName.Text;
        }
        public string getRightPlayerName()
        {
            return lb_opponentRightName.Text;
        }
        public string getLeftPlayerName()
        {
            return lb_opponentLeftName.Text;
        }

    }
}
